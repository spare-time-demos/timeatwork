import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProjectManageComponent } from './project/project-manage/project-manage.component';
import { ProjectViewComponent } from './project/project-view/project-view.component';
import { TaskManageComponent } from './project/task-manage/task-manage.component';
import { WorkWeekComponent } from './time-entry/weekly-work/work-week.component';
import { LoginComponent } from './auth/login/login.component';
import { AuthenticatedRouteGuard } from './auth/AuthenticatedRouteGuard';
import { MonthlyReportComponent } from './report/monthly-report/monthly-report.component';
import { UserViewComponent } from './user/user-view/user-view.component';
import { PasswordComponent } from './auth/password/password.component';
import { MonthlyReportByProjectComponent } from './report/monthly-report-byproject/monthly-report-byproject.component';
import { UserReportByWorkComponent } from './report/user-report-by-work/user-report-by-work.component';
import { AuthenticatedAdminRouteGuard } from './auth/AuthenticatedAdminRouteGuard';
import { ProjectReportByTaskComponent } from './report/project-report-by-task/project-report-by-task.component';
import { UserDetailsComponent } from './user/user-details/user-details.component';

const routes: Routes = [
  { path: '', redirectTo: WorkWeekComponent.timeEntryPath, pathMatch: 'full' },

  { path: LoginComponent.loginPath,                     component: LoginComponent },

  { path: 'project-view',                               component: ProjectViewComponent, canActivate: [AuthenticatedRouteGuard] },
  { path: 'project-manage/project',                     component: ProjectManageComponent, canActivate: [AuthenticatedRouteGuard] },
  { path: 'project-manage/project/:id',                 component: ProjectManageComponent, canActivate: [AuthenticatedRouteGuard] },
  { path: 'project-manage/project/:projectId/task/:id', component: TaskManageComponent, canActivate: [AuthenticatedRouteGuard] },
  { path: 'project-manage/project/:projectId/task',     component: TaskManageComponent, canActivate: [AuthenticatedRouteGuard] },

  { path: 'user-view',                                  component: UserViewComponent,    canActivate: [AuthenticatedAdminRouteGuard] },
  { path: 'user-details',                               component: UserDetailsComponent, canActivate: [AuthenticatedAdminRouteGuard] },
  { path: 'user-details/:id',                           component: UserDetailsComponent, canActivate: [AuthenticatedAdminRouteGuard] },

  { path: WorkWeekComponent.timeEntryPath,                     component: WorkWeekComponent, canActivate: [AuthenticatedRouteGuard] },
  { path: WorkWeekComponent.timeEntryPath + '/workweek/:from', component: WorkWeekComponent, canActivate: [AuthenticatedRouteGuard] },

  { path: MonthlyReportComponent.monthlyReportPath           ,
    component: MonthlyReportComponent, canActivate: [AuthenticatedRouteGuard] },
  { path: MonthlyReportComponent.monthlyReportPath + '/:from',
  component: MonthlyReportComponent, canActivate: [AuthenticatedRouteGuard] },

  { path: MonthlyReportByProjectComponent.monthlyReportPath           ,
    component: MonthlyReportByProjectComponent, canActivate: [AuthenticatedRouteGuard] },
  { path: MonthlyReportByProjectComponent.monthlyReportPath + '/:from',
    component: MonthlyReportByProjectComponent, canActivate: [AuthenticatedRouteGuard] },

  { path: UserReportByWorkComponent.userReportPath,
    component: UserReportByWorkComponent, canActivate: [AuthenticatedAdminRouteGuard] },
  { path: UserReportByWorkComponent.userReportPath + '/:from',
    component: UserReportByWorkComponent, canActivate: [AuthenticatedAdminRouteGuard] },

  { path: ProjectReportByTaskComponent.projectReportPath,
    component: ProjectReportByTaskComponent, canActivate: [AuthenticatedRouteGuard] },
  { path: ProjectReportByTaskComponent.projectReportPath + '/:projectId/:from',
    component: ProjectReportByTaskComponent, canActivate: [AuthenticatedRouteGuard] },

  { path: 'password',                                          component: PasswordComponent, canActivate: [AuthenticatedRouteGuard] },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
