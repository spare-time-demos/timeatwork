import { Component, OnInit, Inject } from '@angular/core';
import { LOCALE_ID } from '@angular/core';
import { environment } from '../environments/environment';
import { LoginService } from './auth/service/login.service';
import { Router, NavigationEnd } from '@angular/router';
import { LoginComponent } from './auth/login/login.component';
import { ApplicationError } from './util/application-error';
import { ErrorStackService } from './service/error-stack.service';
import { LocalDate } from './domain/local-date';


declare let gtag: (s: string, key: string, options: any) => void;

/** main entry point timeatwork angular app. */
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent  implements OnInit {
  title           = 'time@work-client';
  environmentName = environment.name;
  headerMessage   = environment.messages.header;
  workWeekMessage = environment.messages.workWeek;

  userId          = '';
  isAuthenticated = false;
  isAdmin = false;
  isLeadOrAdmin = false;
  localeId = 'en';

  readonly todayString = LocalDate.today().toString();
  applicationError: ApplicationError | undefined;

  constructor(
    private loginService: LoginService,
    private errorStackService: ErrorStackService,
    private router: Router,
    @Inject(LOCALE_ID) localeId: string
    ) {

      this.localeId = localeId;

      // google analytics / tag manager
      this.router.events.subscribe(event => {
        if(event instanceof NavigationEnd){
            gtag('config', 'UA-118343426-3',
                  {
                    page_path: event.urlAfterRedirects
                  });
         }
      }
    );
  }

  ngOnInit() {
    this.changeAuthentication();
    this.loginService.loginUserEvent.subscribe( () => { // watch changes login status.
      this.changeAuthentication();
    });

    this.errorStackService.errorChangeEvent.subscribe( () => {
      this.applicationError = this.errorStackService.currentError;
    });
  }

  onLogout() {
    this.loginService.resetSession();
    this.changeAuthentication();
    this.router.navigateByUrl(LoginComponent.loginPath);
  }

  private changeAuthentication() {
    this.isAuthenticated = this.loginService.isAuthenticated();
    this.userId = this.isAuthenticated ? this.loginService.getUserId() : null;
    this.isAdmin = this.isAuthenticated && this.loginService.isAdmin();
    this.isLeadOrAdmin = this.isAuthenticated && (this.isAdmin || this.loginService.isProjectLead())
  }

}
