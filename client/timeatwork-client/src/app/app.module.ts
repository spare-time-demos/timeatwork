import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Injectable, ErrorHandler } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { NgbModule, NgbDateAdapter } from '@ng-bootstrap/ng-bootstrap';
import { NgxChartsModule } from '@swimlane/ngx-charts';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { ProjectViewComponent } from './project/project-view/project-view.component';
import { ProjectManageComponent } from './project/project-manage/project-manage.component';
import { TaskManageComponent } from './project/task-manage/task-manage.component';
import { NgbLocalDateAdapter } from './ui-util/nbg-localdate-adapter';
import { WorkWeekComponent } from './time-entry/weekly-work/work-week.component';
import { LoginComponent } from './auth/login/login.component';
import { SessionTokenInterceptor } from './auth/SessionTokenInterceptor';
import { PasswordComponent } from './auth/password/password.component';
import { MonthlyReportComponent } from './report/monthly-report/monthly-report.component';
import { MonthlyReportByProjectComponent } from './report/monthly-report-byproject/monthly-report-byproject.component';
import { UserViewComponent } from './user/user-view/user-view.component';
import { UserDetailsComponent } from './user/user-details/user-details.component';
import { UserReportByWorkComponent } from './report/user-report-by-work/user-report-by-work.component';
import { ProjectReportByTaskComponent } from './report/project-report-by-task/project-report-by-task.component';

import { registerLocaleData } from '@angular/common';
import localeDe from '@angular/common/locales/de';

// ----------------- sentry -------------
import * as Sentry from '@sentry/browser';
import { AnalyticsService } from './service/analytics.service';

registerLocaleData(localeDe, 'de-DE');  // additional locale to en-US.

Sentry.init({
  dsn: 'https://c8895cc09c4b4ca4aced76afab2898a7@sentry.io/2504901', // sentry timeatwork-client (TODO: dev environment only so far)
  environment: 'development'
});

@Injectable()
export class SentryErrorHandler implements ErrorHandler {
  constructor() {}
  handleError(error: any) {
    const eventId = Sentry.captureException(error.originalError || error);
    console.warn(error.originalError || error);
    Sentry.showReportDialog({ eventId });
  }
}
// ----------------- sentry -------------

@NgModule({
  declarations: [
    AppComponent,
    ProjectViewComponent,
    ProjectManageComponent,
    TaskManageComponent,
    //
    WorkWeekComponent,
    MonthlyReportComponent,
    MonthlyReportByProjectComponent,
    LoginComponent,
    //
    UserViewComponent,
    UserDetailsComponent,
    PasswordComponent,
    UserReportByWorkComponent,
    ProjectReportByTaskComponent,
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    NgbModule,
    NgxChartsModule
  ],
  providers: [
    {provide: AnalyticsService, useClass: AnalyticsService },
    {provide: HTTP_INTERCEPTORS, useClass: SessionTokenInterceptor, multi: true },
    {provide: NgbDateAdapter, useClass: NgbLocalDateAdapter},
    {provide: ErrorHandler, useClass: SentryErrorHandler}                           // sentry
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
