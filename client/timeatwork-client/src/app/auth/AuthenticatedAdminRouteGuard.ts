import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { LoginService } from './service/login.service';
import { LoginComponent } from './login/login.component';

/** use on protected routes. */
@Injectable({ providedIn: 'root' })
export class AuthenticatedAdminRouteGuard implements CanActivate {
    constructor(
        private router: Router,
        private loginService: LoginService
    ) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

        const isAuthenticatedAdmin = this.loginService.isAuthenticated() && this.loginService.isAdmin;
        if (isAuthenticatedAdmin) {
            return true;
        }

        // not an admin so redirect to login page with the return url
        console.log('not authenticated as admin, redirect to login page..');
        this.router.navigate([LoginComponent.loginPath], { queryParams: { returnUrl: state.url } });
        return false;
    }
}
