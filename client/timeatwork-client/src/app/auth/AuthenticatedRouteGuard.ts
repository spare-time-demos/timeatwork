import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { LoginService } from './service/login.service';
import { LoginComponent } from './login/login.component';

/** use on protected routes. */
@Injectable({ providedIn: 'root' })
export class AuthenticatedRouteGuard implements CanActivate {
    constructor(
        private router: Router,
        private loginService: LoginService
    ) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {

        const isAuthenticated = this.loginService.isAuthenticated();
        if (isAuthenticated) {
            return true;
        }

        // not logged in so redirect to login page with the return url
        console.log('not authenticated, redirect to login page..');
        this.router.navigate([LoginComponent.loginPath], { queryParams: { returnUrl: state.url } });
        return false;
    }
}
