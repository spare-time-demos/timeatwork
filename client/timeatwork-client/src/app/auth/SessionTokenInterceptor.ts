import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { LoginService } from './service/login.service';

/** add Bearer token to all rest calls if user is already authenticated. */
@Injectable()
export class SessionTokenInterceptor implements HttpInterceptor {

    constructor(private loginService: LoginService) {}

    intercept(req: HttpRequest<any>,
              next: HttpHandler): Observable<HttpEvent<any>> {

        const token = this.loginService.getSession();

        if (token) {
            const cloned = req.clone({
                headers: req.headers.set(
                  'Authorization',
                  'Bearer ' + token)
            });

            return next.handle(cloned);

        } else {
            return next.handle(req);
        }
    }
}
