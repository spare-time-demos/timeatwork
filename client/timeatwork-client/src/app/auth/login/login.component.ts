import { Component, OnInit } from '@angular/core';
import { LoginService, AuthenticationRequest } from '../service/login.service';
import { Router } from '@angular/router';
import { WorkWeekComponent } from '../../time-entry/weekly-work/work-week.component';
import { ErrorStackService } from '../../service/error-stack.service';
import { environment } from '../../../environments/environment';

/** login page. */
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {

  public static readonly loginPath = 'login';

  readonly loginMessage = environment.messages.login;

  userId: string;

  password: string;

  constructor(
    private router: Router,
    private loginService: LoginService,
    private errorStackService: ErrorStackService) { }

  ngOnInit() {
  }

  onLogin() {
    const request: AuthenticationRequest = { userId: this.userId, password: this.password };
    this.errorStackService.setInfoMessage('login..');
    this.loginService.resetSession();
    this.loginService
        .login(request)
        .subscribe(response => {
          if (!!response.ok) {
            this.loginService.setSession(response);
            this.router.navigateByUrl(WorkWeekComponent.timeEntryPath);
            this.errorStackService.resetStack();
          } else {
            this.errorStackService.addErrorMessage(response.message);
          }

        },
        e => {
          this.errorStackService.addErrorMessage(`login error: ${e}`);
        });
  }

}
