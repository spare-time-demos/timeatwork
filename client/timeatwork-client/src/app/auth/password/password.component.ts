import { Component, OnInit } from '@angular/core';
import { ErrorStackService } from '../../service/error-stack.service';
import { LoginService, ChangePasswordRequest } from '../service/login.service';

/** change password ui. */
@Component({
  selector: 'app-password',
  templateUrl: './password.component.html',
  styleUrls: []
})
export class PasswordComponent implements OnInit {

  passwordOld: string;
  passwordNew: string;
  passwordNewConfirmation: string;

  constructor(
    private loginService: LoginService,
    private errorStackService: ErrorStackService) { }

  ngOnInit() {
  }

  onPasswordChange() {
    if (this.passwordNew === this.passwordNewConfirmation) {

      const changePassword: ChangePasswordRequest = {
        oldPassword: this.passwordOld,
        newPassword: this.passwordNew
      };

      this.loginService
          .changePassword(changePassword)
          .subscribe(resp => {
            if (resp.ok) {
              this.errorStackService.setInfoMessage(resp.message);
              this.passwordOld = null;
            } else {
              this.errorStackService.addErrorMessage(resp.message);
            }
          },
          e => {
            this.errorStackService.addErrorMessage(`could not change password: ${e}`);
          });
    } else {
      this.errorStackService.addErrorMessage('new password differs from confirmation');
    }
  }

}
