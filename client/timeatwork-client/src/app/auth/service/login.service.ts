import { Injectable, EventEmitter, Output } from '@angular/core';
import { RestServiceBase } from '../../util/rest-service-base';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

export interface AuthenticationRequest {
  userId: string;
  password: string;
}

/** login result. */
export interface AuthenticationResult {
  ok: boolean;
  message: string;
  token: string;

  userId: string;
  hasRoleProjectLead: boolean;
  hasRoleAdmin: boolean;
}


export interface ChangePasswordRequest {
  oldPassword: string;
  newPassword: string;
}


export interface ChangePasswordResult {
  ok: boolean;
  message: string;
}

/** login service.
 *  keeps login session alive (variable or local store or cookie possible)
 */
@Injectable({
  providedIn: 'root'
})
export class LoginService extends RestServiceBase {

  /** login happened. */
  @Output()
  loginUserEvent: EventEmitter<string> = new EventEmitter();

  private readonly sessionTokenKeyName = 'time@work.sessionToken';
  private readonly userIdKeyName = 'time@work.userId';
  private readonly hasRoleProjectLeadKeyName = 'time@work.hasRoleProjectLead';
  private readonly hasRoleAdminKeyName = 'time@work.hasRoleAdmin';

  constructor(private httpClient: HttpClient) {
    super();
  }

  /** user login with name+password, getting a jwt token on success. */
  login(request: AuthenticationRequest): Observable<AuthenticationResult> {
    return this.httpClient
      .post<AuthenticationResult>(this.loginUrl, request)
      .pipe(catchError(this.handleError))
      ;
  }

  /** change password for current user. */
  changePassword(request: ChangePasswordRequest): Observable<ChangePasswordResult> {
    return this.httpClient
      .post<ChangePasswordResult>(this.changePasswordUrl, request)
      .pipe(catchError(this.handleError))
      ;
  }

  resetSession() {
    sessionStorage.removeItem(this.userIdKeyName);
    sessionStorage.removeItem(this.sessionTokenKeyName);
    sessionStorage.removeItem(this.hasRoleProjectLeadKeyName);
    sessionStorage.removeItem(this.hasRoleAdminKeyName);
  }

  /** keep session token on local storage. */
  setSession(authenticationResult: AuthenticationResult) {

    sessionStorage.setItem(this.userIdKeyName, authenticationResult.userId);
    sessionStorage.setItem(this.sessionTokenKeyName, authenticationResult.token);
    sessionStorage.setItem(this.hasRoleProjectLeadKeyName, JSON.stringify(authenticationResult.hasRoleProjectLead));
    sessionStorage.setItem(this.hasRoleAdminKeyName, JSON.stringify(authenticationResult.hasRoleAdmin));

    this.loginUserEvent.emit('login');
  }

  getUserId(): string {
    return sessionStorage.getItem(this.userIdKeyName);
  }

  getSession(): string {
    return sessionStorage.getItem(this.sessionTokenKeyName);
  }

  isProjectLead(): boolean {
    return JSON.parse(sessionStorage.getItem(this.hasRoleProjectLeadKeyName));
  }

  isAdmin(): boolean {
    return JSON.parse(sessionStorage.getItem(this.hasRoleAdminKeyName));
  }

  /** user is currently logged in (has jwt token). */
  isAuthenticated(): boolean {
    return !! this.getSession();
  }

}
