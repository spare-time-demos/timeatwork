import { NgbDateStruct, NgbDate } from '@ng-bootstrap/ng-bootstrap';
import * as moment from 'moment';

/** minimal immutable local date (no time) implementation.
 *  todo: switch to joda.js library or similar.
 */
export class LocalDate {

    constructor(private year: number, private month: number, private dayOfMonth: number) {
    }

    public static today(): LocalDate {
      const d = new Date();
      return new LocalDate(d.getFullYear(), d.getMonth() + 1, d.getDate());
    }

    /** string format yyyy-mm-dd required. */
    public static from(isoDate: string | LocalDate | null | undefined): LocalDate | null {
      if (!isoDate) {
        return null;
      }

      if (isoDate instanceof LocalDate) {
        return isoDate as LocalDate;
      }
      // todo: safe parsing..
      const parts = isoDate.split('-');
      return new LocalDate(parseInt(parts[0], 10), parseInt(parts[1], 10), parseInt(parts[2], 10));
    }

    public static fromNgbDate(dateStruct: NgbDateStruct): LocalDate {
      return new LocalDate(dateStruct.year, dateStruct.month, dateStruct.day);
    }

    startOfWeek(): LocalDate {
      const t = moment(this.toString());
      t.startOf('isoWeek');
      return LocalDate.from(t.format('YYYY-MM-DD'));
    }

    startOfMonth(): LocalDate {
      const t = moment(this.toString());
      t.startOf('month');
      return LocalDate.from(t.format('YYYY-MM-DD'));
    }

    addDays(delta: number): LocalDate {
      const t = moment(this.toString());
      t.add(delta, 'days');
      return LocalDate.from(t.format('YYYY-MM-DD'));
    }

    addMonths(delta: number): LocalDate {
      const t = moment(this.toString());
      t.add(delta, 'months');
      return LocalDate.from(t.format('YYYY-MM-DD'));
    }

    isSameDate(otherDate: LocalDate | null): boolean {
      return !!otherDate && this.toString() === otherDate.toString();
    }

    /** to string yyyy-mm-dd conversion. */
    public toString(): string {
      return ('' + this.year).padStart(4, '0') +
              '-' +
             ('' + this.month).padStart(2, '0')
             + '-' +
             ('' + this.dayOfMonth).padStart(2, '0');
    }

    public getMonthName(): string {
      return moment(this.toString()).format('MMMM');
    }

    public getDayName(): string {
      return moment(this.toString()).format('dddd');
    }

    public getYear(): number {
      return this.year;
    }

    toJSON(): string {
      return this.toString();
    }

    get toNbgDate(): NgbDateStruct {
      return new NgbDate(this.year, this.month, this.dayOfMonth);
    }
}
