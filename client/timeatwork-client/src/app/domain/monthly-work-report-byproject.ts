import { LocalDate } from './local-date';
export interface DailyTaskWork {
  date: LocalDate;
  projectName: string;
  taskName: string;
  workHours: number;
}

export interface TaskWork {
  projectName: string;
  taskName: string;
  workHours: number;
  dailyTaskWorks: DailyTaskWork[];
}

export interface ProjectWorkReport {
  projectHours: number;
  projectName: string;
  taskWorks: TaskWork[];
}

/** report output. */
export interface MonthlyWorkByProjectReport {
  monthlyHours: number;
  projectWorkReports: ProjectWorkReport[];
}
