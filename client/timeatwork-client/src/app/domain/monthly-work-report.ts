export interface DailyTaskWork {
  projectName: string;
  taskName: string;
  workHours: number;
}

export interface DailyWorkReport {
  dailyHours: number;
  date: string;
  dailyTaskWorks: DailyTaskWork[];
}

/** report output. */
export interface MonthlyWorkReport {
  monthlyHours: number;
  dailyWorkReports: DailyWorkReport[];
}
