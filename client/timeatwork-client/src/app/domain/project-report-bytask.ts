
/** report output. */
export interface ProjectTaskResult {
  taskId: number;
  taskName: string;
  workHoursCount: number;
  workHoursSum: number;
}
