import { TeamMember } from './team-member';
export class Project {

  constructor() {
    this.projectLeads = [null, null];
  }

  /** pk. */
  id: number;

  /** project name. */
  name: string;

  /** is project actively used. */
  active: boolean;

  projectLeads: TeamMember[];

}
