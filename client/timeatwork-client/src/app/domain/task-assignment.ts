import { Task } from './task';
import { TeamMember } from './team-member';
import { LocalDate } from './local-date';

/** a teamMember is assigned a task to work in a given time range. */
export class TaskAssignment {
  /** pk. */
  id: number;

  task: Task;

  teamMember: TeamMember;

  from: LocalDate;

  until: LocalDate | null;
}
