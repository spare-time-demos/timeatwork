import { WorkHour } from './work-hour';

export class TaskWeek {
  taskId: number;
  taskName: string;

  workHours: WorkHour[];
}
