export class Task {
  /** pk. */
  id: number;

  /** project name. */
  name: string;
}
