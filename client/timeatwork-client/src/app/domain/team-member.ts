/** team member, may be project lead or has task assigned. */
export class TeamMember {
  /** pk. */
  id: number;

  /** user name. */
  name: string;

  /** user id. */
  userId: string;
}
