
/** report output. */
export interface TeamMemberWorkDoneResult {
  userId: string;
  name: string;
  workHoursCount: number;
  workHoursSum: number;
}
