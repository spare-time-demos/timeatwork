/** user (a team member really), used to login/logout. */
export class User {
  /** pk. */
  id: number;

  /** user name. */
  name: string;

  /** user id. */
  userId: string;

  locked: boolean;

  active: boolean;

  admin: boolean;

  lead: boolean;
}
