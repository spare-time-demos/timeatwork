import { LocalDate } from './local-date';
import { TaskWeek } from './task-week';

/** a week full of work. */
export class WorkWeek {
  from: LocalDate;
  until: LocalDate | null;

  taskWeeks: TaskWeek[];
}

/** substructure for DayChartData. */
export class ChartData {
  constructor(public name: string, public value: number) {}
}

/** input to charting. */
export class DayChartData {

  public name: string;
  public series: ChartData[];

  public static buildChartDataFromWorkWeek(workWeek: WorkWeek): DayChartData[] {

    if (!workWeek || !workWeek.from || !workWeek.until || !workWeek.taskWeeks) {
      return [];
    }

    let currentDay: LocalDate = workWeek.from.addDays(-1);
    let dayIndex = -1;
    const cds: DayChartData[] = [];
    do {
      currentDay = currentDay.addDays(1);
      dayIndex = dayIndex + 1;
      const cd = new DayChartData();
      cd.name = currentDay.getDayName();
      cd.series = workWeek.taskWeeks.map(tw => new ChartData(tw.taskName, tw.workHours[dayIndex].workHours));
      cds.push(cd);
    } while (!currentDay.isSameDate(workWeek.until));

    return cds;
  }
}
