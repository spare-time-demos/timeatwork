import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectManageComponent } from './project-manage.component';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { ProjectService, MOCK_PROJECTS } from '../service/project.service';
import { of } from 'rxjs';
import { MOCK_TEAM_MEMBERS, TeamMemberService } from '../service/teammember-service';
import { TaskService } from '../service/task.service';

describe('ProjectManageComponent', () => {
  let component: ProjectManageComponent;
  let fixture: ComponentFixture<ProjectManageComponent>;

  beforeEach(async(() => {

    // mocking:
    const projectServiceStub = jasmine.createSpyObj('ProjectService', ['getProjects']);
    projectServiceStub.getProjects.and.returnValue(of(MOCK_PROJECTS));

    const taskServiceStub = jasmine.createSpyObj('TaskService', ['deleteTask']);
    taskServiceStub.deleteTask.and.returnValue(of('stub not used in tests.'));

    const teamMemberServiceStub = jasmine.createSpyObj('TeamMemberService', ['getTeamMembers']);
    teamMemberServiceStub.getTeamMembers.and.returnValue(of(MOCK_TEAM_MEMBERS));

    TestBed.configureTestingModule({
      imports: [ FormsModule, RouterTestingModule ],
      declarations: [ ProjectManageComponent ],
      providers: [{provide: ProjectService, useValue: projectServiceStub},
                  {provide: TaskService, useValue: taskServiceStub},
                  {provide: TeamMemberService, useValue: teamMemberServiceStub}
                 ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectManageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
