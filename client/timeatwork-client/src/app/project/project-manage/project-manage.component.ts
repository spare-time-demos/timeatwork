import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { ProjectService } from '../service/project.service';
import { Project } from '../../domain/project';
import { Task } from '../../domain/task';
import { TeamMemberService } from '../service/teammember-service';
import { TeamMember } from '../../domain/team-member';
import { TaskService } from '../service/task.service';
import { ErrorStackService } from '../../service/error-stack.service';

@Component({
  selector: 'app-project-manage',
  templateUrl: './project-manage.component.html',
  styleUrls: ['./project-manage.component.less']
})
export class ProjectManageComponent implements OnInit {

  constructor(private route: ActivatedRoute,
              private projectService: ProjectService,
              private taskService: TaskService,
              private teamMemberService: TeamMemberService,
              private errorStackService: ErrorStackService) { }

  id: number;
  project: Project;
  tasks: Task[];
  teamMembers: TeamMember[];

  ngOnInit() {
    this.id = +this.route.snapshot.paramMap.get('id');

    this.teamMemberService
    .getTeamMembers()
    .subscribe(members => {
      this.teamMembers = members;
    });

    if (this.id !== 0) { // update

      this.projectService
          .getProject(this.id)
          .subscribe(p => {
           this.project = p;
           this.errorStackService.resetStack();
          });

      this.projectService
          .getProjectTasks(this.id)
          .subscribe(t => this.tasks = t);

    } else {
      this.project = new Project(); // create
    }

  }

  compareTeamMembers(tm1: TeamMember, tm2: TeamMember): boolean {
    return tm1 && tm2 ? tm1.id === tm2.id : tm1 === tm2;
  }

  onProjectSave() {
    this.errorStackService.setInfoMessage('save project..');
    if (this.id !== 0) {
      this.projectService
          .saveProject(this.project)
          .subscribe(p => {
            this.project = p;
            this.errorStackService.setInfoMessage(`project ${p.id} saved.`);
          },
          e => {
            this.errorStackService.addErrorMessage(`could not save project: ${e}`);
          });
    } else {
      this.projectService
          .createProject(this.project)
          .subscribe(p => {
            this.project = p;
            this.errorStackService.setInfoMessage(`project ${p.id} created.`);
          },
          e => {
            this.errorStackService.addErrorMessage(`could not create project: ${e}`);
          });
    }
  }

  onTaskDeleteClick(task: Task) {
    this.errorStackService.setInfoMessage(`deleting task..`);
    this.taskService
        .deleteTask(task)
        .subscribe(() => {
          this.errorStackService.setInfoMessage(`removed task ${task.id} from project.`);
          this.projectService
              .getProjectTasks(this.id)
              .subscribe(t => this.tasks = t);
        },
        e => {
          this.errorStackService.addErrorMessage(`could not remove task ${task.id}: ${e}`);
        });
  }
}
