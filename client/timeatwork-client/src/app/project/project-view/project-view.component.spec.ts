import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProjectViewComponent } from './project-view.component';
import { of } from 'rxjs';
import { MOCK_PROJECTS, ProjectService } from '../service/project.service';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('ProjectViewComponent', () => {
  let component: ProjectViewComponent;
  let fixture: ComponentFixture<ProjectViewComponent>;

  beforeEach(async(() => {

    // mocking:
    const projectServiceStub = jasmine.createSpyObj('ProjectService', ['getProjects']);
    projectServiceStub.getProjects.and.returnValue(of(MOCK_PROJECTS));

    TestBed.configureTestingModule({
      imports: [ RouterTestingModule, HttpClientTestingModule ],
      declarations: [ ProjectViewComponent ],
      providers: [
        {provide: ProjectService, useValue: projectServiceStub}
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProjectViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

});
