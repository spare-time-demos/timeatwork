import { Component, OnInit } from '@angular/core';
import { Project } from '../../domain/project';
import { ProjectService } from '../service/project.service';
import { ErrorStackService } from '../../service/error-stack.service';
import { LoginService } from '../../auth/service/login.service';

@Component({
  selector: 'app-project-view',
  templateUrl: './project-view.component.html',
  styleUrls: ['./project-view.component.less']
})
export class ProjectViewComponent implements OnInit {

  projects: Project[];

  canManageProjects = false;

  constructor(
    private projectService: ProjectService,
    private errorStackService: ErrorStackService,
    private loginService: LoginService) {
  }

  ngOnInit() {

    this.canManageProjects = !!this.loginService.isProjectLead() || !!this.loginService.isAdmin();

    if (!!this.canManageProjects) {
      this.projectService.getProjects()
                         .subscribe(p => {
                                            this.projects = p;
                                            this.errorStackService.resetStack();
                                         },
                                    e => this.errorStackService.addErrorMessage(`error loading projects: ${e}`));
    } else {
      this.errorStackService.setInfoMessage('only project leads or admins may manage projects here..');
    }
  }

}
