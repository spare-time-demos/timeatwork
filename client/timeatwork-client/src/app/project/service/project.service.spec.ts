import { TestBed, inject } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { ProjectService, MOCK_PROJECTS } from './project.service';

/** basic idea for http mock. not very useful at the moment. */
describe('ProjectService', () => {

  const mockedResponse = [
    {id: 111,
     name: 'a first active project',
     active: true,
     projectLeads: [{id: 1, name: 'a first test lead and user', userId: 'tlead1'},
                    {id: 2, name: 'another second lead', userId: 'tlead2'}]},
    {id: 2, name: 'a second active project', active: true,
     projectLeads: [{id: 1, name: 'a first test lead and user', userId: 'tlead1'}]}
  ];

  let httpTestingController: HttpTestingController;
  let projectService: ProjectService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule ]
    });

    httpTestingController = TestBed.inject(HttpTestingController);
    projectService = TestBed.inject(ProjectService);
  });

  afterEach(() => {
    httpTestingController.verify();
  });


  it('should be created', () => {
    expect(projectService).toBeTruthy();
  });

  it('can get projects via http', () => {

    projectService.getProjects().subscribe( p => {
      expect(p.length).toBe(2);
      expect(p[0].id).toEqual(111);
      expect(p[1].name).toEqual('a second active project');
    });

    const req = httpTestingController.expectOne(projectService.projectUrl);
    expect(req.request.method).toEqual('GET');
    req.flush(mockedResponse);
  });

  it('handles serialization errors', () => {

    const invalidMockedResponse = [
      {idX: 111,
       inactive: true,
       projectLeads: [{id: 1, name: 'a first test lead and user', userId: 'tlead1'},
                      {id: 2, name: 'another second lead', userId: 'tlead2'}]}
    ];

    projectService.getProjects().subscribe( p => {
      expect(p.length).toBe(1);
      expect(p[0].id).toBeUndefined();
    });

    httpTestingController.expectOne(projectService.projectUrl)
                         .flush(invalidMockedResponse);
  });


  it('handles http errors', () => {

    projectService.getProjects()
                  .subscribe( p => {fail('no valid response expected.'); },
                              (err) => {
                                expect(err).toBeTruthy();
                                expect(err).withContext('expected user error message').toEqual('could not retrieve server data.');
                              });

    httpTestingController.expectOne(projectService.projectUrl)
                         .error(new ErrorEvent('xy'), { status: 401, statusText: 'dummy error'});
  });

});
