import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Project } from '../../domain/project';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Task } from '../../domain/task';
import { RestServiceBase } from '../../util/rest-service-base';


export const MOCK_PROJECTS: Project[] = [
  { id: 11, name: 'project 1', active: true,
    projectLeads: [{id: 1, name: 'a first test lead and user', userId: 'tlead1'}] },
  { id: 12, name: 'another project', active: true,
    projectLeads: [] },
  { id: 13, name: 'yet another project', active: true,
    projectLeads: [{id: 2, name: 'a second test lead and user', userId: 'tlead2'}] },
  { id: 17, name: 'a rather inactive older project is shown here!', active: false,
    projectLeads: [{id: 1, name: 'a first test lead and user', userId: 'tlead1'}] }
];

@Injectable({
  providedIn: 'root'
})
export class ProjectService extends RestServiceBase {

  constructor(private httpClient: HttpClient) {
    super();
  }

  /** HTTP GET all projects */
  getProjects(): Observable<Project[]> {
    // return MOCK_PROJECTS;

    return this.httpClient
      .get<Project[]>(this.projectUrl)
      .pipe(catchError(this.handleError));
  }

  /** HTTP GET all projects */
  getProject(projectId: number): Observable<Project> {
    return this.httpClient
      .get<Project>(this.projectUrl + projectId)
      .pipe(catchError(this.handleError));
  }

  /** update project. */
  saveProject(project: Project): Observable<Project> {
    return this.httpClient
      .put<Project>(this.projectUrl + project.id, project)
      .pipe(catchError(this.handleError));
  }

  /** create new project. */
  createProject(project: Project): Observable<Project> {
    return this.httpClient
      .post<Project>(this.projectUrl, project)
      .pipe(catchError(this.handleError));
  }

  /** HTTP GET all tasks for project */
  getProjectTasks(projectId: number): Observable<Task[]> {
      return this.httpClient
        .get<Task[]>(this.projectUrl + projectId + '/task')
        .pipe(catchError(this.handleError));
  }

}
