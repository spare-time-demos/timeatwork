/** returned errors (body) rest backend. */
export interface ServerException {

  /** equals http status code. */
  status: number;

  /** user readable message */
  message: string;
}
