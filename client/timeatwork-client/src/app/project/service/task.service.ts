import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError } from 'rxjs/operators';
import { Task } from '../../domain/task';
import { RestServiceBase } from '../../util/rest-service-base';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { TaskAssignment } from '../../domain/task-assignment';
import { LocalDate } from '../../domain/local-date';

@Injectable({
  providedIn: 'root'
})
export class TaskService extends RestServiceBase {

  constructor(private httpClient: HttpClient) {
    super();
  }

  /** delete a task. */
  deleteTask(task: Task) {
    return this.httpClient
      .delete<number>(this.taskUrl + '/' + task.id)
      .pipe(catchError(this.handleError));
  }

  /** HTTP GET all task */
  getTask(taskId: number): Observable<Task> {
    return this.httpClient
      .get<Task>(this.taskUrl + '/' + taskId)
      .pipe(catchError(this.handleError));
  }

  /** HTTP GET all tasks assignments for task. */
  getTaskAssignments(taskId: number): Observable<TaskAssignment[]> {
    return this.httpClient
      .get<TaskAssignment[]>(this.taskUrl + '/' + taskId + '/assignment')
      .pipe(map(tas => {
              return tas.map(ta => {
                        ta.from = LocalDate.from(ta.from);       // todo: hack, use a proper date deserializer solution.
                        ta.until = LocalDate.from(ta.until);
                        return ta;
                     });
           }))
      .pipe(catchError(this.handleError));
  }

  /** update task. */
  saveTask(task: Task): Observable<Task> {
    return this.httpClient
      .put<Task>(this.taskUrl + '/' + task.id, task)
      .pipe(catchError(this.handleError));
  }

  /** create new task for given project. */
  createTask(task: Task, projectId: number): Observable<Task> {
    return this.httpClient
      .post<Task>(this.projectUrl + projectId + '/task', task)
      .pipe(catchError(this.handleError));
  }

  /** create new task assignment. */
  createAssignment(assignment: TaskAssignment): Observable<TaskAssignment> {
    return this.httpClient
      .post<TaskAssignment>(this.taskUrl + '/' + assignment.task.id + '/assignment', assignment)
      .pipe(catchError(this.handleError));
  }

  /** change from/until values in existing task assignment. */
  changeAssignment(taskId: number, assignment: TaskAssignment): Observable<TaskAssignment> {
    return this.httpClient
      .put<TaskAssignment>(this.taskUrl + '/' + taskId + '/assignment/' + assignment.id, assignment)
      .pipe(catchError(this.handleError));
  }

  /** create new task assignment. */
  deleteAssignment(taskId: number, assignmentId: number): Observable<number> {
    return this.httpClient
      .delete<number>(this.taskUrl + '/' + taskId + '/assignment/' + assignmentId)
      .pipe(catchError(this.handleError));
  }
}
