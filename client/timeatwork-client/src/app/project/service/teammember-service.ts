import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { TeamMember } from '../../domain/team-member';
import { catchError } from 'rxjs/operators';
import { RestServiceBase } from '../../util/rest-service-base';

export const MOCK_TEAM_MEMBERS: TeamMember[] = [
  {id: 1, name: 'a first test lead and user',  userId: 'tlead1'},
  {id: 2, name: 'a second test lead and user', userId: 'tlead2'},
  {id: 3, name: 'an unused member',            userId: 'tuser3'}
];

@Injectable({
  providedIn: 'root'
})
export class TeamMemberService extends RestServiceBase {

  constructor(private httpClient: HttpClient) {
    super();
  }

  /** HTTP GET all members */
  getTeamMembers(): Observable<TeamMember[]> {

    // return of(MOCK_TEAM_MEMBERS);

    return this.httpClient
      .get<TeamMember[]>(this.teamMemberUrl)
      .pipe(catchError(this.handleError));
  }
}
