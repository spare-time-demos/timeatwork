import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TaskManageComponent } from './task-manage.component';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { Task } from '../../domain/task';
import { TaskService } from '../service/task.service';
import { MOCK_TEAM_MEMBERS, TeamMemberService } from '../service/teammember-service';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

describe('TaskManageComponent', () => {
  let component: TaskManageComponent;
  let fixture: ComponentFixture<TaskManageComponent>;

  // mocking:
  const taskServiceStub = jasmine.createSpyObj('TaskService', ['getTask']);
  taskServiceStub.getTask.and.returnValue(of(new Task() ));

  const teamMemberServiceStub = jasmine.createSpyObj('TeamMemberService', ['getTeamMembers']);
  teamMemberServiceStub.getTeamMembers.and.returnValue(of(MOCK_TEAM_MEMBERS));

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ FormsModule, RouterTestingModule, NgbModule ],
      declarations: [ TaskManageComponent ],
      providers: [
        {provide: TaskService, useValue: taskServiceStub},
        {provide: TeamMemberService, useValue: teamMemberServiceStub}
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TaskManageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
