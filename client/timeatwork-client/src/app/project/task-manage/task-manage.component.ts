import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Task } from '../../domain/task';
import { TaskAssignment } from '../../domain/task-assignment';
import { TaskService } from '../service/task.service';
import { TeamMemberService } from '../service/teammember-service';
import { TeamMember } from '../../domain/team-member';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { LocalDate } from '../../domain/local-date';
import { ErrorStackService } from '../../service/error-stack.service';
import { AnalyticsService } from '../../service/analytics.service';

@Component({
  selector: 'app-task-manage',
  templateUrl: './task-manage.component.html',
  styleUrls: ['./task-manage.component.less']
})
export class TaskManageComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private taskService: TaskService,
    private teamMemberService: TeamMemberService,
    private errorStackService: ErrorStackService,
    private analyticsService: AnalyticsService) { }

  id: number;
  projectId: number;
  task: Task;

  taskAssignments: TaskAssignment[] = [];

  teamMembers: TeamMember[] = [];
  newAssignment: TaskAssignment;

  compareTeamMembers(tm1: TeamMember, tm2: TeamMember): boolean {
    return tm1 && tm2 ? tm1.id === tm2.id : tm1 === tm2;
  }

  ngOnInit() {
    this.errorStackService.resetStack();
    this.id = +this.route.snapshot.paramMap.get('id');
    this.projectId = +this.route.snapshot.paramMap.get('projectId');

    this.teamMemberService
      .getTeamMembers()
      .subscribe(members => {
        this.teamMembers = members;
      });

    if (this.id) {
      this.taskService
        .getTask(this.id)
        .subscribe(t => {
          this.task = t;
          this.newAssignment = this.buildEmptyAssignment(this.task);
        });

      this.taskService
        .getTaskAssignments(this.id)
        .subscribe(ta => {
          this.taskAssignments = ta;
        });
    } else {
      this.id = 0;
      this.task = new Task();
      this.taskAssignments = [];
      this.newAssignment = this.buildEmptyAssignment(this.task);
    }
  }

  onTaskSave() {
    if (this.id !== 0) {
      this.taskService
        .saveTask(this.task)
        .subscribe(t => {
          this.task = t;
          this.errorStackService.setInfoMessage(`saved task ${t.id}.`);
        },
          e => {
            this.errorStackService.addErrorMessage(`could not save project: ${e}`);
          });
    } else {
      this.taskService
        .createTask(this.task, this.projectId)
        .subscribe(t => {
          this.task = t;
          this.router.navigate([`project-manage/project/${this.projectId}/task/${t.id}`]);
        },
          e => {
            this.errorStackService.addErrorMessage(`could not create task: ${e}`);
          });
    }
  }

  /** create a new user assignment for given task (Button "add"). */
  onAssignmentCreate() {

    this.taskService
      .createAssignment(this.newAssignment)
      .subscribe(ta => {

        this.analyticsService
            .eventEmitter('assignment', 'action', 'create', 'taskId', ta.id);

        this.taskService
          .getTaskAssignments(this.id)
          .subscribe(tas => {
            this.taskAssignments = tas;
            this.errorStackService.setInfoMessage(`created new assignment ${ta.id}`);
            this.newAssignment = this.buildEmptyAssignment(this.task)
          });
      },
        e => {
          this.errorStackService.addErrorMessage(`could not create assignment: ${e}`);
        });
  }

  onAssignmentDeleteClick(assignment: TaskAssignment) {
    this.taskService
      .deleteAssignment(this.id, assignment.id)
      .subscribe(() => {

        this.analyticsService
            .eventEmitter('assignment', 'action', 'delete', 'taskId', assignment.task.id);

        this.taskService
          .getTaskAssignments(this.id)
          .subscribe(ta => {
            this.taskAssignments = ta;
            this.errorStackService.setInfoMessage(`removed assignment ${assignment.teamMember.name} from task.`);
          } );
      },
        e => {
          this.errorStackService.addErrorMessage(`could not remove assignment ${assignment.id}: ${e}`);
        });
  }

  onAssignmentDateFromChange(assignment: TaskAssignment, newFrom: NgbDateStruct | null) {

    assignment.from = LocalDate.fromNgbDate(newFrom);

    this.taskService
      .changeAssignment(this.id, assignment)
      .subscribe(ta => {
        this.taskAssignments = [];
        this.taskService
          .getTaskAssignments(this.id)
          .subscribe(tas => {
            this.taskAssignments = tas;
            this.errorStackService.setInfoMessage(`changed assignment ${assignment.teamMember.name} from: ${assignment.from}`);
          } );
      },
        e => {
          this.errorStackService.addErrorMessage(`could not change assignment ${assignment.id}: ${e}`);
        });
  }

  onAssignmentDateUntilChange(assignment: TaskAssignment, newUntil: NgbDateStruct | null) {

    assignment.until = !newUntil ? null : LocalDate.fromNgbDate(newUntil);

    this.taskService
      .changeAssignment(this.id, assignment)
      .subscribe(ta => {
        this.errorStackService.setInfoMessage(`changed assignment ${assignment.teamMember.name} until: ${ta.until}`);
        this.taskService
          .getTaskAssignments(this.id)
          .subscribe(tas => this.taskAssignments = tas);
        },
        e => {
          this.errorStackService.addErrorMessage(`could not change assignment ${assignment.id}: ${e}`);
        });
  }

  buildEmptyAssignment(task: Task): TaskAssignment {
    if(!task) {
      throw new Error('no task for empty assignment')
    }
    const ta = new TaskAssignment();
    ta.task = task;
    ta.teamMember = null;
    ta.from = LocalDate.today();
    return ta;
  }
}
