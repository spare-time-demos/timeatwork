import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { LocalDate } from '../../domain/local-date';
import { MonthlyWorkByProjectReport, ProjectWorkReport } from '../../domain/monthly-work-report-byproject';
import { ReportService } from '../service/report.service';
import { ErrorStackService } from '../../service/error-stack.service';

/** monthly report ui */
@Component({
  selector: 'app-monthly-report-byproject',
  templateUrl: './monthly-report-byproject.component.html',
  styleUrls: ['./monthly-report-byproject.component.less']
})
export class MonthlyReportByProjectComponent implements OnInit {

  static readonly monthlyReportPath = 'report/monthly/byProject';

  from: LocalDate;
  fromTitle: string;
  monthlyWorkByProjectReport: MonthlyWorkByProjectReport;
  projectWorkReports: ProjectWorkReport[];

  constructor(
    public  router: Router,
    private route: ActivatedRoute,
    private reportService: ReportService,
    private errorStackService: ErrorStackService) {

    this.router.routeReuseStrategy.shouldReuseRoute = () => false;  // need to reload on url change (from)!
  }

  ngOnInit() {

    this.errorStackService.resetStack();
    const paramFrom = this.route.snapshot.paramMap.get('from');

    if (!paramFrom) {
      this.router
          .navigate([`${MonthlyReportByProjectComponent.monthlyReportPath}/${LocalDate.today().startOfMonth().toString()}`]);
    } else {
      this.from = LocalDate.from(paramFrom);
      this.fromTitle = this.from.getYear() + ' ' + this.from.getMonthName();

      if (this.from.isSameDate(this.from.startOfMonth())) {
        this.reportService
            .getMonthlyReportByProject(this.from)
            .subscribe(report => {
              this.monthlyWorkByProjectReport = report;
              this.projectWorkReports = report.projectWorkReports;
            });
      } else {
        this.router
            .navigate([`${MonthlyReportByProjectComponent.monthlyReportPath}/${this.from.startOfMonth().toString()}`]);
      }
    }
  }


  onMonthDisplayChange(deltaMonths: number) {
    const newFrom = this.from.addMonths(deltaMonths);
    this.router.navigate([`${MonthlyReportByProjectComponent.monthlyReportPath}/${newFrom.toString()}`]);
  }

    /** start pdf download */
    onDownloadPdf() {
      this.reportService
      .getMonthlyReportByProjectAsPdf(this.from)
      .subscribe(pdfBlob => {
         this.reportService.saveToFileSystem(pdfBlob, 'application/pdf', 'timeatwork.report.' + this.from.getMonthName()  + '.pdf');
        },
        e => {
          this.errorStackService.addErrorMessage(`error retrieving pdf file: ${e}`)
      });
    }
}
