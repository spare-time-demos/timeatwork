import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MonthlyReportComponent } from './monthly-report.component';
import { RouterTestingModule } from '@angular/router/testing';
import { ReportService } from '../service/report.service';
import { MonthlyWorkReport } from '../../domain/monthly-work-report';


describe('MonthlyReportComponent', () => {
  let component: MonthlyReportComponent;
  let fixture: ComponentFixture<MonthlyReportComponent>;

  // mocking:
  const reportServiceStub = jasmine.createSpyObj('ReportService', ['getMonthlyReport']);
  reportServiceStub.getMonthlyReport.and.returnValue(null);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes([
          {path: MonthlyReportComponent.monthlyReportPath + '/:from', component: MonthlyReportComponent}
        ])
      ],
      declarations: [ MonthlyReportComponent ],
      providers: [
        {provide: ReportService, useValue: reportServiceStub}
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MonthlyReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
