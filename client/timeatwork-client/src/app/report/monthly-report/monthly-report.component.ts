import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { ReportService } from '../service/report.service';
import { MonthlyWorkReport } from 'src/app/domain/monthly-work-report';
import { LocalDate } from 'src/app/domain/local-date';
import { DailyWorkReport } from '../../domain/monthly-work-report';
import { ErrorStackService } from '../../service/error-stack.service';


/** monthly report ui */
@Component({
  selector: 'app-monthly-report',
  templateUrl: './monthly-report.component.html',
  styleUrls: ['./monthly-report.component.less']
})
export class MonthlyReportComponent implements OnInit {

  static readonly monthlyReportPath = 'report/monthly/byDay';

  from: LocalDate;
  fromTitle: string;
  monthlyWorkReport: MonthlyWorkReport;
  dailyWorkReports: DailyWorkReport[];

  constructor(
    public  router: Router,
    private route: ActivatedRoute,
    private reportService: ReportService,
    private errorStackService: ErrorStackService) {

    this.router.routeReuseStrategy.shouldReuseRoute = () => false;  // need to reload on url change (from)!
  }

  ngOnInit() {

    this.errorStackService.resetStack();
    const paramFrom = this.route.snapshot.paramMap.get('from');

    if (!paramFrom) {
      this.router
          .navigate([`${MonthlyReportComponent.monthlyReportPath}/${LocalDate.today().startOfMonth().toString()}`]);
    } else {
      this.from = LocalDate.from(paramFrom);
      this.fromTitle = this.from.getYear() + ' ' + this.from.getMonthName();

      if (this.from.isSameDate(this.from.startOfMonth())) {
        this.reportService
            .getMonthlyReportByDay(this.from)
            .subscribe(report => {
              this.monthlyWorkReport = report;
              this.dailyWorkReports = report.dailyWorkReports;
            });
      } else {
        this.router
            .navigate([`${MonthlyReportComponent.monthlyReportPath}/${this.from.startOfMonth().toString()}`]);
      }
    }
  }


  onMonthDisplayChange(deltaMonths: number) {
    const newFrom = this.from.addMonths(deltaMonths);
    this.router.navigate([`${MonthlyReportComponent.monthlyReportPath}/${newFrom.toString()}`]);
  }

}
