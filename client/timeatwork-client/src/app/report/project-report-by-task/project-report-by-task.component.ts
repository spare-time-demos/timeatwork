import { Component, OnInit } from '@angular/core';
import { LocalDate } from '../../domain/local-date';
import { Router, ActivatedRoute } from '@angular/router';
import { ReportService } from '../service/report.service';
import { ErrorStackService } from '../../service/error-stack.service';
import { ProjectTaskResult } from '../../domain/project-report-bytask';
import { Project } from '../../domain/project';
import { ProjectService } from '../../project/service/project.service';
import { AnalyticsService } from '../../service/analytics.service';

/** report-ui: project report for project-lead. */
@Component({
  selector: 'app-project-report-by-task',
  templateUrl: './project-report-by-task.component.html',
  styleUrls: ['./project-report-by-task.component.less']
})
export class ProjectReportByTaskComponent implements OnInit {

  static readonly projectReportPath = 'report/project/byTask';

  from: LocalDate;
  projectName: string;
  projectId = 0;
  projects: Project[];

  projectTaskResult: ProjectTaskResult[];

  constructor(
    public  router: Router,
    private route: ActivatedRoute,
    private reportService: ReportService,
    private projectService: ProjectService,
    private errorStackService: ErrorStackService,
    private analyticsService: AnalyticsService) {

    this.router.routeReuseStrategy.shouldReuseRoute = () => false;  // need to reload on url change (from)!
  }

  ngOnInit() {

    this.errorStackService.resetStack();
    const paramFrom = this.route.snapshot.paramMap.get('from');
    const qpProjectId = this.route.snapshot.paramMap.get('projectId');

    this.errorStackService.resetStack();

    if (!paramFrom) {
      this.router
          .navigate([`${ProjectReportByTaskComponent.projectReportPath}/0/${LocalDate.today().startOfMonth().toString()}`]);
    } else {
      this.from = LocalDate.from(paramFrom);

      if (!!qpProjectId) {
        this.projectId = Number.parseInt(qpProjectId, 10);
      }

      if (!this.projects) {
        this.projectService
            .getProjects()
            .subscribe(p => {
                this.projects = p;
                if (!!this.projectId && !!this.projects) {
                  const selectedProject = this.projects.find(sp => sp.id === this.projectId);
                  if (!!selectedProject) {
                    this.projectName = selectedProject.name;
                  }
                }
              },
              e => this.errorStackService.addErrorMessage(`error loading projects: ${e}`)
            );
      }


      if (this.from.isSameDate(this.from.startOfMonth()) && this.projectId !== 0 ) {
        this.reportService
            .getProjectReportByTask(this.projectId, this.from)
            .subscribe(report => {
              this.projectTaskResult = report;
            },
            e => this.errorStackService.addErrorMessage(`error loading report data: ${e}`)
            );
      } else {
        this.router
            .navigate([`${ProjectReportByTaskComponent.projectReportPath}/${this.projectId}/${this.from.startOfMonth().toString()}`]);
      }
    }
  }


  onMonthDisplayChange(deltaMonths: number) {
    const newFrom = this.from.addMonths(deltaMonths);
    this.router.navigate([`${ProjectReportByTaskComponent.projectReportPath}/${this.projectId}/${newFrom.toString()}`]);
  }

  onProjectChange(project: Project) {
    this.projectId = project.id;
    this.projectName = project.name;

    this.analyticsService
        .eventEmitter('report', 'criteria', 'change', 'projectId', project.id);

    this.router.navigate([`${ProjectReportByTaskComponent.projectReportPath}/${this.projectId}/${this.from.toString()}`]);
  }

  /** start download */
  onDownloadExcel() {
    this.reportService
    .getProjectReportByTaskAsExcel(this.projectId, this.from)
    .subscribe(excelBlob => {
       this.reportService.saveToFileSystem(excelBlob);
      },
      e => {
        this.errorStackService.addErrorMessage(`error retrieving excel file: ${e}`)
    });
  }
}
