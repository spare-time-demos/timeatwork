import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';

import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { RestServiceBase } from '../../util/rest-service-base';
import { LocalDate } from '../../domain/local-date';
import { MonthlyWorkReport } from '../../domain/monthly-work-report';
import { MonthlyWorkByProjectReport } from '../../domain/monthly-work-report-byproject';
import { TeamMemberWorkDoneResult } from '../../domain/user-report-bywork';
import { ProjectTaskResult } from '../../domain/project-report-bytask';
import { saveAs } from 'file-saver';

@Injectable({
  providedIn: 'root'
})
export class ReportService extends RestServiceBase {

  constructor(private httpClient: HttpClient) {
    super();
  }

  /** HTTP GET get report data.
   * @param from day of month to view.
   */
  public getMonthlyReportByDay(from: LocalDate): Observable<MonthlyWorkReport> {

    return this.httpClient
      .get<MonthlyWorkReport>(this.monthlyReportUrl + '/' + from.toString() + '/byDay')
      .pipe(catchError(this.handleError));
  }

  /** HTTP GET get report data.
   * @param from day of month to view.
   */
  public getMonthlyReportByProject(from: LocalDate): Observable<MonthlyWorkByProjectReport> {

    return this.httpClient
      .get<MonthlyWorkByProjectReport>(this.monthlyReportUrl + '/' + from.toString() + '/byProject')
      .pipe(catchError(this.handleError));
  }


  /** HTTP GET get report pdf.
   * @param from day of month to view.
   */
  public getMonthlyReportByProjectAsPdf(from: LocalDate):  Observable<HttpResponse<any>> {

    let myHeaders = new HttpHeaders();
    myHeaders = myHeaders.append('Accept', '*/*');

    return this.httpClient
      .get(this.monthlyReportUrl + '/' + from.toString() + '/byProject/pdf',
           { headers: myHeaders, observe: 'response', responseType: 'blob'  })
      .pipe(catchError(this.handleError));
  }

  /** HTTP GET get report data user (low work first)
   * @param from day of month to view.
   */
  public getUserReportByWork(from: LocalDate): Observable<TeamMemberWorkDoneResult[]> {

    return this.httpClient
      .get<TeamMemberWorkDoneResult[]>(this.userReportUrl + '/' + from.toString() + '/byWork')
      .pipe(catchError(this.handleError));
  }

  /** HTTP GET get project report data
   * @param projectId project id to report.
   * @param from day of month to view.
   */
  public getProjectReportByTask(projectId: number, from: LocalDate): Observable<ProjectTaskResult[]> {

    return this.httpClient
      .get<ProjectTaskResult[]>(this.projectReportUrl + '/' + projectId.toString() + '/' + from.toString() + '/byTask')
      .pipe(catchError(this.handleError));
  }

  /** HTTP GET get project report data in Excel Report Format
   * @param projectId project id to report.
   * @param from day of month to view.
   */
  public getProjectReportByTaskAsExcel(projectId: number, from: LocalDate) : Observable<HttpResponse<any>> {

    let myHeaders = new HttpHeaders();
    myHeaders = myHeaders.append('Accept', '*/*');

    return this.httpClient
      .get(this.projectReportUrl + '/' + projectId.toString() + '/' + from.toString() + '/byTask/excel',
           { headers: myHeaders, observe: 'response', responseType: 'blob'  })
      .pipe(catchError(this.handleError));
  }

  public saveToFileSystem(
    response: HttpResponse<any>,
    contentType = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    fileNameSuggestion = 'report.xlsx') {

    const contentDispositionHeader: string = response.headers.get('Content-Disposition');
    let filename = fileNameSuggestion;

    if(!!contentDispositionHeader) {  // TODO: adapt headers allow from server, so far only null
      const parts: string[] = contentDispositionHeader.split(';');
      filename = parts[1].split('=')[1];
    }
    const blob = new Blob([response.body], { type: contentType});
    saveAs(blob, filename);
  }

}
