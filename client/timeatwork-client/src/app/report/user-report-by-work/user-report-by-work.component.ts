import { Component, OnInit } from '@angular/core';
import { LocalDate } from '../../domain/local-date';
import { TeamMemberWorkDoneResult } from '../../domain/user-report-bywork';
import { Router, ActivatedRoute } from '@angular/router';
import { ReportService } from '../service/report.service';
import { ErrorStackService } from '../../service/error-stack.service';

@Component({
  selector: 'app-user-report-by-work',
  templateUrl: './user-report-by-work.component.html',
  styleUrls: ['./user-report-by-work.component.less']
})
export class UserReportByWorkComponent implements OnInit {

  static readonly userReportPath = 'report/user/byWork';

  from: LocalDate;
  fromTitle: string;
  teamMemberWorkDoneResult: TeamMemberWorkDoneResult[];

  constructor(
    public  router: Router,
    private route: ActivatedRoute,
    private reportService: ReportService,
    private errorStackService: ErrorStackService) {

    this.router.routeReuseStrategy.shouldReuseRoute = () => false;  // need to reload on url change (from)!
  }

  ngOnInit() {

    this.errorStackService.resetStack();
    const paramFrom = this.route.snapshot.paramMap.get('from');

    if (!paramFrom) {
      this.router
          .navigate([`${UserReportByWorkComponent.userReportPath}/${LocalDate.today().startOfMonth().toString()}`]);
    } else {
      this.from = LocalDate.from(paramFrom);
      this.fromTitle = this.from.getYear() + ' ' + this.from.getMonthName();

      if (this.from.isSameDate(this.from.startOfMonth())) {
        this.reportService
            .getUserReportByWork(this.from)
            .subscribe(report => {
              this.teamMemberWorkDoneResult = report;
            });
      } else {
        this.router
            .navigate([`${UserReportByWorkComponent.userReportPath}/${this.from.startOfMonth().toString()}`]);
      }
    }
  }


  onMonthDisplayChange(deltaMonths: number) {
    const newFrom = this.from.addMonths(deltaMonths);
    this.router.navigate([`${UserReportByWorkComponent.userReportPath}/${newFrom.toString()}`]);
  }

}
