import { Injectable } from '@angular/core';

declare let gtag: (s: string, eventName: string, options: any) => void;

/** send manually custom google analytics events.
 * based on: https://medium.com/@PurpleGreenLemon/how-to-properly-add-google-analytics-tracking-to-your-angular-web-app-bc7750713c9e
 */
@Injectable({
  providedIn: 'root'
})
export class AnalyticsService {

  constructor() { }

  /** send event to ga.
   * @param eventCategory see https://developers.google.com/analytics/devguides/collection/gtagjs/events for well known events.
   */
  public eventEmitter(
    eventName: string,
    eventCategory: string,
    eventAction: string,
    eventLabel: string = null,
    eventValue: number = null ){
         gtag('event', eventName, {
              eventCategory,
              eventLabel,
              eventAction,
              eventValue
             })
  }

}
