import { Injectable, Output, EventEmitter} from '@angular/core';
import { ApplicationError, ErrorType } from '../util/application-error';

/** will hold current error situation for display. */
@Injectable({
  providedIn: 'root'
})
export class ErrorStackService {

  currentError: ApplicationError | null = null;

  /** an error or an error reset occurred. */
  @Output()
  errorChangeEvent: EventEmitter<ApplicationError | null> = new EventEmitter();

  constructor() { }

  public addError(error: ApplicationError): void {
    this.currentError = error;
    this.errorChangeEvent.emit(error);
  }

  public addErrorMessage(message: string): void {
    this.currentError = new ApplicationError(message, ErrorType.Error);
    this.errorChangeEvent.emit(this.currentError);
  }

  public setInfoMessage(message: string): void {
    this.currentError = new ApplicationError(message, ErrorType.Info);
    this.errorChangeEvent.emit(this.currentError);
  }

  public resetStack() {
    if(!!this.currentError) {
      this.currentError = null;
      this.errorChangeEvent.emit(null);
    }
  }

}
