import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { RestServiceBase } from '../../util/rest-service-base';
import { LocalDate } from '../../domain/local-date';
import { WorkWeek } from '../../domain/work-week';
import { map } from 'rxjs/operators';

/** work week rest client calls. */
@Injectable({
  providedIn: 'root'
})
export class WorkWeekService extends RestServiceBase {

  constructor(private httpClient: HttpClient) {
    super();
  }

  /** HTTP GET work done for given week.
   * @param from day starting week to load.
   */
  getWorkWeek(from: LocalDate): Observable<WorkWeek> {

    return this.httpClient
      .get<WorkWeek>(this.workWeekUrl + '/' + from.toString())
      .pipe(map(d => {
          const ww = new WorkWeek();
          ww.from = LocalDate.from(d.from);
          ww.until  = LocalDate.from(d.until);
          ww.taskWeeks = d.taskWeeks;
          return ww;
        }))
      .pipe(catchError(this.handleError));
  }

  /** save updated working hours for given workweek.
   * @param workWeek week to update.
   * @returns number of work hours stored on this call.
   */
  saveWorkWeek(workWeek: WorkWeek): Observable<number> {

      const from = workWeek.from;

      return this.httpClient
        .put<number>(this.workWeekUrl + '/' + from.toString(), workWeek)
        .pipe(catchError(this.handleError));
  }

}
