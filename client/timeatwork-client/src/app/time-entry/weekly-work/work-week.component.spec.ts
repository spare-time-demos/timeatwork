import { of } from 'rxjs';

import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';

import { NgxChartsModule } from '@swimlane/ngx-charts';

import { WorkWeekComponent } from './work-week.component';
import { WorkWeek } from '../../domain/work-week';
import { WorkWeekService } from '../service/work-week-service';

describe('WorkWeekComponent', () => {

  let component: WorkWeekComponent;
  let fixture: ComponentFixture<WorkWeekComponent>;

  // mocking:
  const workWeekServiceStub = jasmine.createSpyObj('WorkWeekService', ['getWorkWeek']);
  workWeekServiceStub.getWorkWeek.and.returnValue(of(new WorkWeek() ));

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        BrowserAnimationsModule,
        NgxChartsModule,
        RouterTestingModule.withRoutes([
            {path: WorkWeekComponent.timeEntryPath + '/workweek/:from', component: WorkWeekComponent}
        ])
      ],
      declarations: [
        WorkWeekComponent
      ],
      providers: [
        {provide: WorkWeekService, useValue: workWeekServiceStub}
      ]
    })
    .compileComponents();

  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkWeekComponent);
    component = fixture.componentInstance;

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

});
