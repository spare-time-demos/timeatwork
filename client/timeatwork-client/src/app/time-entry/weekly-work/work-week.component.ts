import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

import { WorkWeek, DayChartData } from '../../domain/work-week';
import { WorkWeekService } from '../service/work-week-service';
import { LocalDate } from '../../domain/local-date';
import { ActivatedRoute, Router } from '@angular/router';
import { TaskWeek } from '../../domain/task-week';
import { ErrorStackService } from '../../service/error-stack.service';
import { AnalyticsService } from '../../service/analytics.service';
import { environment } from '../../../environments/environment';

/** user-ui: time entry. */
@Component({
  selector: 'app-work-week',
  templateUrl: './work-week.component.html'
})
export class WorkWeekComponent implements OnInit {

  static readonly timeEntryPath = 'time-entry';
  static readonly timeEntryWorkWeekPath = WorkWeekComponent.timeEntryPath + '/workweek';

  workWeekMessage = environment.messages.workWeek;

  from: LocalDate;
  workWeek: WorkWeek;

  private ngForm: NgForm;

  constructor(
    public router: Router,
    private route: ActivatedRoute,
    private workWeekService: WorkWeekService,
    private errorStackService: ErrorStackService,
    private analyticsService: AnalyticsService) {

    this.router.routeReuseStrategy.shouldReuseRoute = () => false;  // need to reload on url change (from)!
  }

  /** vertical sum. */
  sumOfDay(dayOffset: number): number {
    if (!this.workWeek) {
      return 0;
    }
    return this.workWeek.taskWeeks
      .map(tw => tw.workHours[dayOffset].workHours as number)
      .reduce((c: number, n: number) => c + n, 0);
  }

  /** horizontal sum. */
  sumOfTask(tw: TaskWeek): number {
    return tw.workHours
      .map(wh => wh.workHours)
      .reduce((c, n) => c + n, 0);
  }

  getChartData(workWeek: WorkWeek): DayChartData[] {
    return !!workWeek ? DayChartData.buildChartDataFromWorkWeek(workWeek) : [];
  }

  /** load given weeks data. */
  ngOnInit() {

    const paramFrom = this.route.snapshot.paramMap.get('from');

    if (!paramFrom) {
      this.router.navigate([`${WorkWeekComponent.timeEntryWorkWeekPath}/${LocalDate.today().startOfWeek().toString()}`]);
    } else {
      this.from = LocalDate.from(paramFrom);
      if (this.from.isSameDate(this.from.startOfWeek())) {
        this.workWeekService
          .getWorkWeek(this.from)
          .subscribe(ww => {
            this.workWeek = ww;
            this.errorStackService.resetStack();
          });
      } else {
        this.router.navigate([`${WorkWeekComponent.timeEntryWorkWeekPath}/${this.from.startOfWeek().toString()}`]);
      }
    }
  }

  /** save manual data */
  onWeekSave(ngForm: NgForm) {

    this.ngForm = ngForm;
    this.errorStackService.setInfoMessage(`save week: ${this.from}..`);


    this.workWeekService
        .saveWorkWeek(this.workWeek)
        .subscribe(updatedHours => {

        this.errorStackService.setInfoMessage(`saved week: ${this.from} with ${updatedHours} hours.`);

        this.analyticsService
            .eventEmitter('week-saved', 'action', 'save', 'hours', updatedHours);

        this.workWeekService
          .getWorkWeek(this.from)
          .subscribe(ww => {
            this.workWeek = ww;
            this.ngForm.form.markAsPristine();
          });
      },
        e => {
          this.errorStackService.addErrorMessage(`could not save week: ${this.from}: ${e}`);
        });

  }

  /** load next or previews week. */
  onDateDisplayChange(deltaDays: number) {
    this.errorStackService.resetStack();
    const newFrom = this.from.addDays(deltaDays);
    this.router.navigate([`${WorkWeekComponent.timeEntryWorkWeekPath}/${newFrom.toString()}`]);
  }

}
