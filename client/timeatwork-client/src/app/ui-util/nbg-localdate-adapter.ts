import {Injectable} from '@angular/core';
import { NgbDateAdapter, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { LocalDate } from '../domain/local-date';

/**
 * [`NgbDateAdapter`](#/components/datepicker/api#NgbDateAdapter) implementation that uses
 * LocalDate dates as a user date model.
 */
@Injectable()
export class NgbLocalDateAdapter extends NgbDateAdapter<LocalDate> {
  /**
   * Converts a native `LocalDate` to a `NgbDateStruct`.
   */
  fromModel(date: LocalDate): NgbDateStruct {
    return (date instanceof LocalDate)
        ? date.toNbgDate
        : null;
  }

  /**
   * Converts a `NgbDateStruct` to a `LocalDate`.
   */
  toModel(date: NgbDateStruct): LocalDate {
    return !!date && Number.isInteger(date.year) && Number.isInteger(date.month) && Number.isInteger(date.day)
        ? LocalDate.fromNgbDate(date)
        : null;
  }
}
