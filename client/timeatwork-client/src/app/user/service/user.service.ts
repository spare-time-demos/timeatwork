import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { RestServiceBase } from '../../util/rest-service-base';
import { Observable } from 'rxjs';
import { User } from '../../domain/user';
import { catchError } from 'rxjs/operators';

export const MOCK_USERS: User[] = [
  { id: 1, name: 'admin 1', active: true,  userId: 'a1', locked: false, admin: true,  lead: false },
  { id: 2, name: 'lead 2',  active: false, userId: 'l2', locked: false, admin: false, lead: true  },
  { id: 3, name: 'user 3',  active: true,  userId: 'u3', locked: true,  admin: false, lead: false },
];

export interface ChangeResetResult {
  ok: boolean;
  message: string;
}

@Injectable({
  providedIn: 'root'
})
export class UserService  extends RestServiceBase {

  constructor(private httpClient: HttpClient) {
    super();
  }

  /** HTTP GET all users. */
  getUsers(): Observable<User[]> {

    return this.httpClient
      .get<User[]>(this.userUrl)
      .pipe(catchError(this.handleError));
  }


  /** HTTP GET specific user. */
  getUser(id: number): Observable<User> {

    return this.httpClient
      .get<User>(this.userUrl + '/' + id)
      .pipe(catchError(this.handleError));
  }

  /** update user. */
  saveUser(user: User): Observable<User> {
    return this.httpClient
      .put<User>(this.userUrl + '/' + user.id, user)
      .pipe(catchError(this.handleError));
  }

  /** create new user. */
  createUser(user: User): Observable<User> {
    return this.httpClient
      .post<User>(this.userUrl, user)
      .pipe(catchError(this.handleError));
  }

  /** reset password. */
  resetPassword(user: User): Observable<ChangeResetResult> {
    return this.httpClient
      .put<ChangeResetResult>(this.userUrl + '/' + user.id + '/password', user)
      .pipe(catchError(this.handleError));
  }

}
