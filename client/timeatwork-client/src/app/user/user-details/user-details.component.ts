import { Component, OnInit } from '@angular/core';
import { User } from '../../domain/user';
import { ErrorStackService } from '../../service/error-stack.service';
import { UserService } from '../service/user.service';
import { ActivatedRoute } from '@angular/router';

/** admin-ui: edit new or existing user (details view). */
@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: []
})
export class UserDetailsComponent implements OnInit {

  id: number;
  user: User;

  constructor(
    private route: ActivatedRoute,
    private errorStackService: ErrorStackService,
    private userService: UserService
  ) { }

  ngOnInit() {
    this.errorStackService.resetStack();
    this.id = +this.route.snapshot.paramMap.get('id');

    if (this.id !== 0) { // update

      this.userService
          .getUser(this.id)
          .subscribe(u => {
           this.user = u;
          });

    } else {
      this.user = new User(); // create
    }
  }

  /** save changes on given user. */
  public onUserSave(user: User) {

    if (!!user.id) {
      this.userService
          .saveUser(user)
          .subscribe(u => {
            this.user = u;
            this.errorStackService.setInfoMessage(`user ${u.userId} saved.`);
          },
          e => {
            this.errorStackService.addErrorMessage(`could not save user: ${e}`);
          });
    } else {
      this.userService
          .createUser(user)
          .subscribe(u => {
            this.user = u;
            this.errorStackService.setInfoMessage(`new user ${u.userId} created.`);
          },
          e => {
            this.errorStackService.addErrorMessage(`could not create user: ${e}`);
          });
    }
  }

  public onPasswordReset(user: User) {

    this.userService
    .resetPassword(user)
    .subscribe(u => {
      this.errorStackService.setInfoMessage(`user "${user.userId}": password reset ${u.message}.`);
    },
    e => {
      this.errorStackService.addErrorMessage(`could not reset password: ${e}`);
    });
  }
}
