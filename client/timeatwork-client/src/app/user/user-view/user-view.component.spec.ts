import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { UserViewComponent } from './user-view.component';
import { UserService, MOCK_USERS } from '../service/user.service';
import { of } from 'rxjs';
import { Component, Input } from '@angular/core';
import { User } from '../../domain/user';

@Component({selector: 'app-user-details', template: ''})
class UserDetailsStubComponent {
  @Input()
  user: User;
}

describe('UserViewComponent', () => {
  let component: UserViewComponent;
  let fixture: ComponentFixture<UserViewComponent>;

  beforeEach(async(() => {

    const userServiceStub = jasmine.createSpyObj('UserService', ['getUsers']);
    userServiceStub.getUsers.and.returnValue(of(MOCK_USERS));

    TestBed.configureTestingModule({
      imports: [ RouterTestingModule, HttpClientTestingModule ],
      declarations: [ UserViewComponent, UserDetailsStubComponent],
      providers: [
        {provide: UserService, useValue: userServiceStub}
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
