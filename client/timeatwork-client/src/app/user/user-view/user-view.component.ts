import { Component, OnInit } from '@angular/core';
import { User } from '../../domain/user';
import { UserService } from '../service/user.service';
import { ErrorStackService } from '../../service/error-stack.service';
import { LoginService } from '../../auth/service/login.service';

/** admin-ui: view users. */
@Component({
  selector: 'app-user-view',
  templateUrl: './user-view.component.html',
  styleUrls: []
})
export class UserViewComponent implements OnInit {

  canManageUser = false;

  users: User[];

  selectedUser: User;

  constructor(
    private userService: UserService,
    private errorStackService: ErrorStackService,
    private loginService: LoginService) {}

  /** load user list. */
  ngOnInit() {
    this.canManageUser = !!this.loginService.isAdmin();

    if (!!this.canManageUser) {
      this.userService.getUsers()
                         .subscribe(u => {
                                            this.users = u;
                                            this.errorStackService.resetStack();
                                         },
                                    e => this.errorStackService.addErrorMessage(`error loading users: ${e}`));
    } else {
      this.errorStackService.setInfoMessage('only admins may manage users here..');
    }
  }

  /** select existing user. */
  public onUserSelect(user: User): void {
    this.selectedUser = user;
  }

  /** add new user.. */
  public onUserNew(): void {
    this.selectedUser = new User();
    this.selectedUser.id = 0;
  }

}
