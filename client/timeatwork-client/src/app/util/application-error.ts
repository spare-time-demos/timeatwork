export enum ErrorType {
  Info = 'Info',
  Error = 'Error'
}

/** custom error messages for display.. */
export class ApplicationError extends Error {

  constructor(public message: string, public type: ErrorType = ErrorType.Error) {
    super(message);
  }

  public get isError(): boolean {
    return this.type === ErrorType.Error;
  }

  public get isInfo(): boolean {
    return this.type === ErrorType.Info;
  }

}
