import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';

export class DateUtil {

  static fromJson(dateJsonString: string | Date): Date | null {
    if (!dateJsonString) {
      return null;
    }
    return new Date(dateJsonString);
  }

  static fromNgbDate(ds: NgbDateStruct | undefined): Date | null {
    if (!ds) {
      return null;
    }
    return new Date(ds.year, ds.month - 1, ds.day);
  }
}
