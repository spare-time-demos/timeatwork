import { HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { ServerException } from '../project/service/server-exception';
import { environment } from '../../environments/environment';

export class RestServiceBase {

  static readonly baseUrl = environment.restServicesBaseUrl;

  readonly projectUrl  = RestServiceBase.baseUrl + 'project/';
  readonly taskUrl     = RestServiceBase.baseUrl + 'task';
  readonly workWeekUrl = RestServiceBase.baseUrl + 'workweek';
  readonly teamMemberUrl = RestServiceBase.baseUrl + 'teammember';
  readonly monthlyReportUrl = RestServiceBase.baseUrl + 'report/monthly';
  readonly projectReportUrl = RestServiceBase.baseUrl + 'report/project';
  readonly userReportUrl = RestServiceBase.baseUrl + 'report/user';
  readonly userUrl     = RestServiceBase.baseUrl + 'admin/user';
  readonly loginUrl  = RestServiceBase.baseUrl.replace('rest/', '') + 'authentication';
  readonly changePasswordUrl   = RestServiceBase.baseUrl + 'password';

  /** basic error handing for rest calls. */
  protected handleError(error: HttpErrorResponse): Observable<never> {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);

      const serverException: ServerException = error.error;
      if (!!serverException.message) {
        return throwError(`server exception: ${serverException.message}`);
      } else {
        return throwError(`backend error occurred: ${error.status} ${error.message}.`);
      }
    }

    // return an observable with a user-facing error message
    return throwError('could not retrieve server data.');
  }

}
