// development.
export const environment = {
  production: false,

  /** run rest service from separate tomcat port.. */
  restServicesBaseUrl: 'http://localhost:8080/rest/',

  name: '(dev)',

  // optional messages to include on pages..
  messages: {
    login: 'this application is in <em>demo</em> mode,<br /> <br /> <ul><li>use <em>user/password</em> to log into it.</ul>',

    workWeek: '',

    header: 'please ignore <em>this</em> sample header message'
  }
};

import 'zone.js/dist/zone-error';  // Included with Angular CLI.
