// all other production like environments, not development.
export const environment = {
  production: true,

  /** run rest service from same port port.. */
  restServicesBaseUrl: '/rest/',

  name: '(prod)',

  // optional messages to include on pages..
  messages: {
    login: 'this application is in <em>demo</em> mode',

    workWeek: '',

    header: ''
  }
};
