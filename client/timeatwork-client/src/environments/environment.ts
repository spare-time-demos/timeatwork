export const environment = {
  production: false,

  restServicesBaseUrl: 'http://localhost:8080/rest/',

  name: '(default)',

  // optional messages to include on pages..
  messages: {
    login: '',

    workWeek: '',

    header: ''
  }
};
