---
layout: index
title: 'home'
components:
  - type: pageContent
  - type: recentPosts
    limit: 3
    category: ':any'
    noWrapper: true
    template:
      - 'includes/postPreview_large'
next: 'category1'
---

hello to the project site of time@work.

