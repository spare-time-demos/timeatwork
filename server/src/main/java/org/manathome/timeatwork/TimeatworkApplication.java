package org.manathome.timeatwork;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * time@work application.
 * 
 * @author man-at-home
 * @since 2019
 */
@SpringBootApplication
public class TimeatworkApplication {

  /** main entry point. */
  public static void main(String[] args) {
    SpringApplication.run(TimeatworkApplication.class, args);
  }

}
