package org.manathome.timeatwork.batch;

import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;

/** spring batch configuration (job: usersync). */
@EnableBatchProcessing
public class BatchConfiguration {

}