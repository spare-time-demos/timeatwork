package org.manathome.timeatwork.batch.usersync;

import org.manathome.timeatwork.util.Logger;
import org.springframework.batch.item.ItemProcessor;

public class UserProcessor implements ItemProcessor<String, String> {

  private static final Logger logger = Logger.getLogger(UserProcessor.class);

  @Override
  public String process(final String user) throws Exception {
    logger.trace("processing: " + user);
    return user;
  }

}
