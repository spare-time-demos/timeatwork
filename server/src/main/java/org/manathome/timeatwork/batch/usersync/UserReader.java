package org.manathome.timeatwork.batch.usersync;

import org.manathome.timeatwork.util.Logger;
import org.springframework.batch.item.ItemReader;

/** read user from company ad/ldap.. */
public class UserReader implements ItemReader<String> {

  private static final Logger logger = Logger.getLogger(UserReader.class);
  
  private static final int USER_READ_BATCH_SIZE = 20;

  private long userReadIndex;

  /** read user. 
   */
  @Override
  public String read() {

    ++userReadIndex;
    
    if (userReadIndex < USER_READ_BATCH_SIZE) {
      logger.trace("read " + userReadIndex);
      return "fake user line " + userReadIndex;
    } else {
      userReadIndex = 0;
      logger.debug("end of batch, reset" + userReadIndex);
      return null;
    }
  }

}
