package org.manathome.timeatwork.batch.usersync;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * spike: spring bagtch job that should be able to sync ad/ldap registered
 * employees to time@work users.
 */
@Configuration
@EnableBatchProcessing
public class UserSyncJob {
  
  @Bean
  public UserReader userReader() {
    return new UserReader();
  }

  @Bean
  public UserProcessor userProcessor() {
    return new UserProcessor();
  }

  @Bean
  public UserSyncWriter userSyncWriter() {
    return new UserSyncWriter();
  }

  private Step userSyncStep(final StepBuilderFactory stepBuilderFactory) {
    return stepBuilderFactory.get("userSyncStep")
        .<String, String>chunk(1)
        .reader(userReader())
        .processor(userProcessor())
        .writer(userSyncWriter())
        .allowStartIfComplete(true)
        .build();
  }

  /** spring ci. */
  @Bean
  public Job jobUserSync(
      @Autowired final StepBuilderFactory stepBuilderFactory, 
      @Autowired final JobBuilderFactory jobBuilderFactory) {
    return jobBuilderFactory.get("jobUserSync")
        .incrementer(new RunIdIncrementer())
        .start(userSyncStep(stepBuilderFactory))
        .build();
  }

}
