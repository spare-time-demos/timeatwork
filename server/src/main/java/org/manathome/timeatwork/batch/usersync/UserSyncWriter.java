package org.manathome.timeatwork.batch.usersync;

import java.util.List;

import org.manathome.timeatwork.util.Logger;
import org.springframework.batch.item.ItemWriter;

public class UserSyncWriter implements ItemWriter<String> {

  private static final Logger logger = Logger.getLogger(UserSyncWriter.class);

  @Override
  public void write(List<? extends String> items) throws Exception {
    logger.debug("sync" + items.size() + " users to user database.");
  }

}
