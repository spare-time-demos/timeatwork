package org.manathome.timeatwork.configuration;

import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Configuration;

/** enable caching (eh-cache). */
@Configuration
@EnableCaching
public class CacheConfiguration {

}
