package org.manathome.timeatwork.configuration;

import org.ehcache.event.CacheEvent;
import org.ehcache.event.CacheEventListener;
import org.manathome.timeatwork.util.Logger;

/** logging cache events with eh-cache specific cache listener. */
public class CacheLogger implements CacheEventListener<Object, Object> {
  
  private static final Logger log = Logger.getLogger(CacheLogger.class);

  /** callback: cache event happened. */
  @Override
  public void onEvent(CacheEvent<?, ?> cacheEvent) {
    
    log.trace("Cache Event: Key: " 
            + "{" + cacheEvent.getKey() 
            + "} | EventType: { " + cacheEvent.getType() 
            + "} | old value: { " + cacheEvent.getOldValue() 
            + "} | new value: { " + cacheEvent.getNewValue() 
            + "}");
    
  }
  
}
