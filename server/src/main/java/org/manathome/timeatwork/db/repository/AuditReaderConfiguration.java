package org.manathome.timeatwork.db.repository;

import javax.persistence.EntityManagerFactory;

import org.hibernate.envers.AuditReader;
import org.hibernate.envers.AuditReaderFactory;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/** register hibernate envers audit reader for querying audit information. 
 * @see <a href="https://hibernate.org/orm/envers">hibernate envers</a>
 */
@Configuration
public class AuditReaderConfiguration {

  private final EntityManagerFactory entityManagerFactory;

  AuditReaderConfiguration(final EntityManagerFactory entityManagerFactory) {
    this.entityManagerFactory = entityManagerFactory;
  }

  @Bean
  AuditReader auditReader() {
    return AuditReaderFactory.get(entityManagerFactory.createEntityManager());
  }
}