package org.manathome.timeatwork.db.repository;

import java.time.LocalDate;
import java.util.List;

import org.manathome.timeatwork.db.repository.result.ProjectTaskResult;
import org.manathome.timeatwork.domain.Task;
import org.manathome.timeatwork.domain.WorkDone;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

@Component
public class ProjectReportQuery {

  @Autowired
  private NamedParameterJdbcTemplate jdbcTemplate;

  /** sql query project with sums on task hours. */
  public List<ProjectTaskResult> readProjectByTask(
      Long projectId,
      LocalDate from, 
      LocalDate until) {
    
    final var parameters = new MapSqlParameterSource()
        .addValue("projectId", projectId)
        .addValue("from", from)
        .addValue("until", until);

    final String sql 
               = " select tk.id as task_id, max(tk.name) as task_name, " 
               + " count(wd.work_hours) as work_hours_count, sum(wd.work_hours) as work_hours_sum "
               + " from " + Task.TABLE_NAME + " tk "
               + " left join " + WorkDone.TABLE_NAME + " wd "
               + "       on  tk.id = wd.task_id"
               + " where wd.date_at >= :from and wd.date_at <= :until"
               + "   and tk.project_id = :projectId"
               + " group by tk.id"
               + " order by sum(wd.work_hours) desc ";

    return jdbcTemplate.query(
        sql, 
        parameters,
        new BeanPropertyRowMapper<ProjectTaskResult>(ProjectTaskResult.class, true));

  }

}
