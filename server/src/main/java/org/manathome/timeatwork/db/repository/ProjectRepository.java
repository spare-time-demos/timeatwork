package org.manathome.timeatwork.db.repository;

import java.util.stream.Stream;

import org.manathome.timeatwork.domain.Project;
import org.manathome.timeatwork.domain.TeamMember;
import org.springframework.data.repository.CrudRepository;

/**
 * database repository for work items.
 * 
 * @author man@home
 */
public interface ProjectRepository extends CrudRepository<Project, Long> {

  Stream<Project> findByProjectLeads(TeamMember lead);
}
