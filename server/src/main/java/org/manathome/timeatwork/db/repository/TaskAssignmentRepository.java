package org.manathome.timeatwork.db.repository;

import java.time.LocalDate;
import java.util.stream.Stream;

import org.manathome.timeatwork.domain.TaskAssignment;
import org.manathome.timeatwork.domain.TeamMember;
import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface TaskAssignmentRepository extends CrudRepository<TaskAssignment, Long> {

  Stream<TaskAssignment> findByTaskIdOrderById(Long taskId);

  @Query("SELECT ta FROM TaskAssignment ta WHERE ta.teamMember = :teamMember AND "
      + "ta.from <= :until AND (ta.until IS NULL OR ta.until >= :from)")
  @EntityGraph(attributePaths = { "task" })
  Stream<TaskAssignment> findAssignedTasksForMember(@Param("teamMember") TeamMember member,
      @Param("from") LocalDate from, @Param("until") LocalDate until);
}
