package org.manathome.timeatwork.db.repository;

import java.util.stream.Stream;

import org.manathome.timeatwork.domain.Task;
import org.springframework.data.repository.CrudRepository;

/**
 * database repository for work items.
 * 
 * @author man@home
 */
public interface TaskRepository extends CrudRepository<Task, Long> {

  Stream<Task> findByProjectIdOrderById(Long projectId);
}
