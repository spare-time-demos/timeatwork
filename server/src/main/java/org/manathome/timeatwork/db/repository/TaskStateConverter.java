package org.manathome.timeatwork.db.repository;

import java.util.stream.Stream;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import org.manathome.timeatwork.domain.TaskState;

@Converter(autoApply = true)
public class TaskStateConverter implements AttributeConverter<TaskState, String> {

  @Override
  public String convertToDatabaseColumn(TaskState taskState) {
    if (taskState == null) {
      return null;
    }
    return taskState.getCode();
  }

  @Override
  public TaskState convertToEntityAttribute(String taskState) {
    if (taskState == null) {
      return null;
    }

    return Stream.of(TaskState.values()).filter(c -> c.getCode().equals(taskState)).findFirst()
        .orElseThrow(IllegalArgumentException::new);
  }
}