package org.manathome.timeatwork.db.repository;

import java.util.Optional;
import org.manathome.timeatwork.domain.TeamMember;
import org.manathome.timeatwork.domain.values.Name;
import org.springframework.data.repository.CrudRepository;

public interface TeamMemberRepository extends CrudRepository<TeamMember, Long>  {
  
  
  Optional<TeamMember> findByUserId(Name userId);
  
}
