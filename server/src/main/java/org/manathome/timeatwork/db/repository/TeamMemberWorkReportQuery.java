package org.manathome.timeatwork.db.repository;

import java.time.LocalDate;
import java.util.List;

import org.manathome.timeatwork.db.repository.result.TeamMemberWorkDoneResult;
import org.manathome.timeatwork.domain.TeamMember;
import org.manathome.timeatwork.domain.WorkDone;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

@Component
public class TeamMemberWorkReportQuery {

  @Autowired
  private NamedParameterJdbcTemplate jdbcTemplate;

  /** sql query least working users. */
  public List<TeamMemberWorkDoneResult> readUsersOrderByWorkDoneAscending(
      LocalDate from, 
      LocalDate until) {
    
    final var parameters = new MapSqlParameterSource()
        .addValue("from", from)
        .addValue("until", until);

    final String sql 
               = " select tm.user_id, max(tm.name) as name, " 
               + " count(wd.work_hours) as work_hours_count, sum(wd.work_hours) as work_hours_sum "
               + " from " + TeamMember.TABLE_NAME + " tm "
               + " left join " + WorkDone.TABLE_NAME + " wd "
               + "       on  tm.id = wd.team_member_id"
               + " where wd.date_at >= :from and wd.date_at <= :until"
               + " group by tm.user_id"
               + " order by sum(wd.work_hours) asc ";

    return jdbcTemplate.query(
        sql, 
        parameters,
        new BeanPropertyRowMapper<TeamMemberWorkDoneResult>(TeamMemberWorkDoneResult.class, true));

  }

}
