package org.manathome.timeatwork.db.repository;

import java.time.LocalDate;
import java.util.stream.Stream;

import org.manathome.timeatwork.domain.TeamMember;
import org.manathome.timeatwork.domain.WorkDone;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface WorkDoneRepository extends CrudRepository<WorkDone, Long> {

  @Query("SELECT w FROM WorkDone w WHERE " 
       + "w.teamMember = :teamMember AND " 
       + "w.date <= :until AND "
       + "w.date >= :from ")
  Stream<WorkDone> findWorkDoneByMember(
      @Param("teamMember") TeamMember teamMember, 
      @Param("from") LocalDate from,
      @Param("until") LocalDate until);
}