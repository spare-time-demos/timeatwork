package org.manathome.timeatwork.db.repository.result;

public class ProjectTaskResult {

  private long taskId;
  private String taskName;
  
  private int workHoursSum;
  private int workHoursCount;
  
  
  public long getTaskId() {
    return taskId;
  }

  public void setTaskId(long taskId) {
    this.taskId = taskId;
  }

  public String getTaskName() {
    return taskName;
  }

  public void setTaskName(String taskName) {
    this.taskName = taskName;
  }

  public int getWorkHoursSum() {
    return workHoursSum;
  }

  public void setWorkHoursSum(int workHoursSum) {
    this.workHoursSum = workHoursSum;
  }

  public int getWorkHoursCount() {
    return workHoursCount;
  }

  public void setWorkHoursCount(int workHoursCount) {
    this.workHoursCount = workHoursCount;
  }
  
}
