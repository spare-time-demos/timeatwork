package org.manathome.timeatwork.db.repository.result;

/** report result row. */
public class TeamMemberWorkDoneResult {
    
  private String userId;

  private String name;
  
  private int workHoursSum;
  
  private int workHoursCount;

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getWorkHoursSum() {
    return workHoursSum;
  }

  public void setWorkHoursSum(int workHoursSum) {
    this.workHoursSum = workHoursSum;
  }

  public int getWorkHoursCount() {
    return workHoursCount;
  }

  public void setWorkHoursCount(int workHoursCount) {
    this.workHoursCount = workHoursCount;
  }
  
}
