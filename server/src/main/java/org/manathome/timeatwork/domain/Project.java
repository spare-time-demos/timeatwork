package org.manathome.timeatwork.domain;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Convert;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.envers.Audited;
import org.hibernate.envers.RelationTargetAuditMode;
import org.manathome.timeatwork.domain.values.Name;
import org.manathome.timeatwork.domain.values.NameConverter;
import org.manathome.timeatwork.util.assertions.Require;

/** tracked project. 
 * this entity is audited with envers.*/
@Entity
@Table(name = "HX_PROJECT")
@Audited(targetAuditMode = RelationTargetAuditMode.NOT_AUDITED)
public class Project {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "name", length = 100, nullable = false, unique = true)
  @Convert(converter = NameConverter.class)
  private Name name;

  @Column(name = "active", nullable = false)
  private boolean active = true;

  @ManyToMany
  @JoinTable(name = "HX_PROJECT_LEAD", joinColumns = @JoinColumn(name = "project_id"), 
             inverseJoinColumns = @JoinColumn(name = "team_member_id"))
  private Set<TeamMember> projectLeads;

  /** builder. */
  public static Project buildForTest(long id, String projectName) {
    final var p = new Project(Name.of(projectName));
    p.setId(Long.valueOf(id));
    return p;
  }

  public Project() {
    this.projectLeads = new HashSet<>();
  }

  public Project(final Name name) {
    this();
    this.name = Require.notNull(name, "name");
  }

  public Long getId() {
    return id;
  }

  private void setId(final Long id) {
    Require.isTrue(this.id == null, "id " + this.id + " cannot be changed on project");
    this.id = id;
  }

  public @NotNull Name getName() {
    return Require.notNull(name);
  }

  public void setName(@NotNull Name name) {
    this.name = Require.notNull(name, "name");
  }

  public Collection<TeamMember> getProjectLeads() {
    return Collections.unmodifiableSet(this.projectLeads);
  }

  public void addProjectLead(final TeamMember projectLead) {
    this.projectLeads.add(Require.notNull(projectLead, "projectLead"));
  }

  public void removeProjectLead(final TeamMember projectLead) {
    this.projectLeads.remove(projectLead);
  }

  public boolean isActive() {
    return this.active;
  }

  public void setActive(boolean active) {
    this.active = active;
  }

  /** is project lead by currentUser. */
  public boolean hasLead(@NotNull final TeamMember currentUser) {
    return currentUser != null
        && this.projectLeads != null 
        && this.getProjectLeads()
               .stream()
               .anyMatch(tm -> Require.notNull(currentUser.getId(), "currentUser.id")
                                      .equals(tm.getId()));
  }

  @Override
  public String toString() {
    return "Project[" + id + ", " + name + ", active: " + active + "]";
  }

}
