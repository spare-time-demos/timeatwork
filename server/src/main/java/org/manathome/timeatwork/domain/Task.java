package org.manathome.timeatwork.domain;

import io.swagger.v3.oas.annotations.media.Schema;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.manathome.timeatwork.domain.values.Name;
import org.manathome.timeatwork.domain.values.NameConverter;
import org.manathome.timeatwork.util.assertions.Require;

/** a project task to be worked on. */
@Entity
@Table(name = Task.TABLE_NAME, 
       uniqueConstraints = {
           @UniqueConstraint(name = "UQ_TASK_NAME_PER_PROJECT", 
                      columnNames = { "project_id", "name" })})
public class Task {
  
  public static final String TABLE_NAME = "HX_TASK";
  public static final String TASK_NAME = "task";

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "name", length = 100, nullable = false, unique = true)
  @Convert(converter = NameConverter.class)
  private Name name;

  @NotNull
  @Column(name = "state", nullable = false)
  private TaskState state;

  @NotNull
  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "project_id")
  private Project project;

  /** builder. */
  public static Task buildForTest(String name) {
    final var p = Project.buildForTest(-1, "dummy project");
    return new Task(p, Name.of(name));
  }

  /** builder. */
  public static Task buildForTest(long id, String name) {
    final var t = buildForTest(name);
    t.setId(id);
    return t;
  }

  /** jpa required ctor. */
  protected Task() {
  }

  /** ctor. */
  public Task(final Project project, final Name name) {
    this.project = Require.notNull(project, "project");
    this.name = Require.notNull(name);
    this.state = TaskState.PLANNED;
  }

  public Long getId() {
    return id;
  }

  private void setId(final Long id) {
    this.id = id;
  }

  @Schema(description = "unique task name within project", minLength = 1, maxLength = 100, 
          type = "string", required = true, example = "first task: do something")
  public Name getName() {
    return name;
  }

  public void setName(final Name name) {
    this.name = Require.notNull(name);
  }

  public Project getProject() {
    return project;
  }

  public TaskState getState() {
    return state;
  }

  public void setState(final TaskState state) {
    this.state = Require.notNull(state);
  }

  @Override
  public String toString() {
    return "Task[" + id + ", " + name + ", state: " + state + "]";
  }

}
