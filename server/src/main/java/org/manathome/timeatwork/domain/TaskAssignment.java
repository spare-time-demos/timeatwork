package org.manathome.timeatwork.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.v3.oas.annotations.media.Schema;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.manathome.timeatwork.util.assertions.Require;

/** planned assignment of a team member to a given project task. */
@Entity
@Table(name = "HX_TASK_ASSIGNMENT")
public class TaskAssignment {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @ManyToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "team_member_id", nullable = false)
  private TeamMember teamMember;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "task_id", nullable = false)
  private Task task;

  @Column(name = "date_from", nullable = false)
  private LocalDate from;

  @Column(name = "date_until", nullable = true)
  private LocalDate until;

  /** builder. */
  public static TaskAssignment buildForTest(long teamMemberId, long taskId) {
    final var tm = TeamMember.buildForTest(teamMemberId, "dummy user", "dummy");
    final var t = Task.buildForTest(taskId, "dummy task");
    return new TaskAssignment(tm, t, LocalDate.now().minusDays(10));
  }

  /** jpa .ctor. */
  public TaskAssignment() {
    
  }

  /** create an assignment. */
  public TaskAssignment(final TeamMember member, final Task task, LocalDate from) {
    this.teamMember = Require.notNull(member);
    this.task = Require.notNull(task);
    this.from = Require.notNull(from);
  }

  @Schema(description = "internal unique assignment id", example = "-1")
  public Long getId() {
    return id;
  }

  @Schema(description = "assigned team member")
  public @NotNull TeamMember getTeamMember() {
    return teamMember;
  }

  @JsonIgnore
  public Task getTask() {
    return task;
  }

  public void setTask(final Task task) {
    this.task = task;
  }

  @Schema(description = "assignment start date", example = "2019-01-02")
  public @NotNull LocalDate getFrom() {
    return from;
  }

  @Schema(description = "optional assignment end date", example = "2021-12-31", required = false)
  public LocalDate getUntil() {
    return until;
  }

  /**
   * assignment time range.
   * 
   * @param from  required from date
   * @param until optional until date
   */
  public void setAssignmentRange(final LocalDate from, final LocalDate until) {

    Require.notNull(from);
    if (until != null && from != null) {
      Require.isTrue(until.isAfter(from));
    }
    this.from = from;
    this.until = until;
  }

  @Override
  public String toString() {
    return "Assignment[id=" + id + ", teamMember=" + teamMember 
           + ", task=" + task + ", from=" + from + "]";
  }

  /** may this work be added to current week by given user. */
  public boolean allowesWork(final WorkDone workDone) {

    return workDone != null 
        && workDone.isValid()
        && workDone.getTeamMember().getId().longValue() == this.getTeamMember().getId().longValue()
        && workDone.getTask().getId().longValue() == this.getTask().getId().longValue()
        && !workDone.getDate().isBefore(this.from) 
        && (this.until == null || !workDone.getDate().isAfter(this.until));
  }

}
