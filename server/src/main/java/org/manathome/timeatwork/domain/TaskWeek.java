package org.manathome.timeatwork.domain;

import io.swagger.v3.oas.annotations.media.Schema;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collection;

import org.manathome.timeatwork.domain.values.Name;
import org.manathome.timeatwork.domain.values.WorkHours;
import org.manathome.timeatwork.domain.values.WorkHoursAssigned;
import org.manathome.timeatwork.util.assertions.Require;

/** 
 * public faced data. 
 * @category dto. 
 */
@Schema(name = "TaskWeek", description = "one week of working hours for a given task.")
public class TaskWeek {

  private static final int DAYS_IN_WEEK = 7;

  private Long taskId;
  private Name taskName;

  private final WorkHoursAssigned[] workHours = new WorkHoursAssigned[DAYS_IN_WEEK];

  private TaskWeek() {
  }

  /** .ctor. */
  public TaskWeek(
      final LocalDate from, 
      final TaskAssignment taskAssignment, 
      final Collection<WorkDone> workDone) {
    this();

    Require.notNull(workDone, "workDone");
    Require.isTrue(taskAssignment != null || !workDone.isEmpty(), 
                   "assignment or workDone required.");

    final var task = taskAssignment == null 
            ? Require.notNull(workDone, "workDone")
                    .stream()
                    .findFirst()
                    .orElseThrow()
                    .getTask()
            : taskAssignment.getTask();

    taskId = task.getId();
    taskName = task.getName();

    for (int dayIndex = 0; dayIndex < DAYS_IN_WEEK; dayIndex++) {

      final var day = from.plusDays(dayIndex);

      // task is not assigned OR task assignment does not contain given day
      final boolean isReadOnly = taskAssignment == null 
          || task == null 
          || task.getState() == TaskState.DONE
          || taskAssignment.getFrom().isAfter(day)
          || (taskAssignment.getUntil() != null && taskAssignment.getUntil().isBefore(day));

      final var calcWorkHours = workDone.stream()
          .filter(wd -> wd.getDate().isEqual(day) 
                     && wd.getTask().getId().longValue() == taskId.longValue())
          .map(WorkDone::getWorkHours)
          .findFirst()
          .orElse(WorkHours.ofHours(0));

      this.workHours[dayIndex] = new WorkHoursAssigned(calcWorkHours, isReadOnly);
    }
  }

  @Schema(description = "task name (prose).")
  public Name getTaskName() {
    return this.taskName;
  }

  @Schema(description = "task unique id")
  public Long getTaskId() {
    return this.taskId;
  }

  @Schema(description = "7 days array of working hours")
  public WorkHoursAssigned[] getWorkHours() {
    return workHours;
  }

  /** sum hours. */
  public WorkHours sumWorkHours() {
    return WorkHours
        .ofHours(Arrays
            .stream(workHours)
            .mapToInt(wd -> wd.getWorkHours())
            .sum()); //NOSONAR
  }

  @Override
  public String toString() {
    return "TW[task=" + taskName + ", workHours=" + sumWorkHours() + "]";
  }

  /** dump values to logs etc. */
  public String toDump() {
    return "TW[task:" + taskId + "," + taskName + ": Mo:" 
        + workHours[0].getWorkHours() + ", Tu:"
        + workHours[1].getWorkHours() + ", We:" 
        + workHours[2].getWorkHours() + ", Th:" 
        + workHours[3].getWorkHours()
        + ", Fr:" + workHours[4].getWorkHours() 
        + ", Sa:" + workHours[5].getWorkHours() + ", So:"
        + workHours[6].getWorkHours() + "]\n";
  }

  /** check require. */
  public void validate() {
    Require.notNull(this.taskId, "taskId");
    Require.notNull(getWorkHours(), "workhours");
    Require.isTrue(getWorkHours().length == DAYS_IN_WEEK, "workhours length not " + DAYS_IN_WEEK);
    for (final var wh: this.workHours) {
      Require.notNull(wh, "worhHour");
    }
  }

}
