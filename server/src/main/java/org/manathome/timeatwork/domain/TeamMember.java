package org.manathome.timeatwork.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.manathome.timeatwork.domain.values.Name;
import org.manathome.timeatwork.domain.values.NameConverter;
import org.manathome.timeatwork.util.assertions.Require;

/** employee or worker that leads or works on a project. */
@Entity
@Table(name = TeamMember.TABLE_NAME)
public class TeamMember implements Serializable {
  
  /**
   * required for caching.
   */
  private static final long serialVersionUID = -3338312632276995655L;

  public static final String TABLE_NAME = "HX_TEAM_MEMBER";

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "name", length = 100, nullable = false, unique = true)
  @Convert(converter = NameConverter.class)
  private Name name;

  @Column(name = "user_id", length = 25, nullable = false, unique = true)
  @Convert(converter = NameConverter.class)
  private Name userId;
  
  @JsonIgnore
  @Column(name = "password", length = 25, nullable = true)
  private String password;
  
  @Column(name = "is_locked")
  private int lockedFlag;

  @Column(name = "is_active")
  private int activeFlag;

  @Column(name = "is_admin")
  private int adminRoleFlag;

  @Column(name = "is_lead")
  private int leadRoleFlag;

  /** builder. */
  public static TeamMember buildForTest(long id, String name, String userId) {
    return new TeamMember(Long.valueOf(id), Name.of(name), Name.of(userId));
  }

  /** jpa required ctor. */
  protected TeamMember() {
  }

  /** ctor. */
  public TeamMember(final Long id, final Name userId, final Name name) {
    this(userId, name);
    this.id = id;
  }

  /** ctor. */
  public TeamMember(final Name userId, final Name name) {
    this.name = Require.notNull(name, "name");
    this.userId = Require.notNull(userId, "userId");
  }

  public Long getId() {
    return id;
  }

  public Name getUserId() {
    return userId;
  }
  
  public String getUserIdAsString() {
    return userId.toString();
  }  
  
  public void setUserId(final Name userId) {
    this.userId = userId;
  }

  public Name getName() {
    return name;
  }

  public void setName(final Name name) {
    this.name = Require.notNull(name);
  }
  
  @JsonIgnore
  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }  


  public boolean isLocked() {
    return lockedFlag == 1;
  }

  public void setLocked(boolean locked) {
    this.lockedFlag = locked ? 1 : 0;
  }

  public boolean isActive() {
    return activeFlag == 1;
  }

  public void setActive(boolean active) {
    this.activeFlag = active ? 1 : 0;
  }

  public boolean hasAdminRole() {
    return adminRoleFlag == 1;
  }

  public void setAdminRole(boolean isAdmin) {
    this.adminRoleFlag = isAdmin ? 1 : 0;
  }

  public boolean hasLeadRole() {
    return leadRoleFlag == 1;
  }

  public void setLeadRole(boolean isLead) {
    this.leadRoleFlag = isLead ? 1 : 0;
  }  
  
  @Override
  public String toString() {
    return "TM[" + id + ": " + userId + ", " + name + "]";
  }

  /** current team member may log in into app. */
  public boolean userMayLogIn() {
    return isActive() 
        && ! isLocked() 
        && this.getUserId() != null 
        && this.getPassword() != null;
  }

}
