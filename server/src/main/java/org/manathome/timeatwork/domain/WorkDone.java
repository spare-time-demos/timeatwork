package org.manathome.timeatwork.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.v3.oas.annotations.media.Schema;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import org.manathome.timeatwork.domain.values.WorkHours;
import org.manathome.timeatwork.domain.values.WorkHoursConverter;
import org.manathome.timeatwork.util.assertions.Require;


/** work done on a task by a team member. */
@Entity
@Table(name = WorkDone.TABLE_NAME, uniqueConstraints = {
    @UniqueConstraint(name = "UQ_WORK_DONE_ONE_PER_DAY", 
                      columnNames = { "team_member_id", "task_id", "date_at" }) })
@Schema(description = "work done on a task by a team member.")
public class WorkDone {
  
  public static final String TABLE_NAME = "HX_WORK_DONE";

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "team_member_id", nullable = false)
  private TeamMember teamMember;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "task_id", nullable = false)
  private Task task;

  @Column(name = "date_at", nullable = false)
  private LocalDate date;

  @Column(name = "work_hours", nullable = false)
  @Convert(converter = WorkHoursConverter.class)
  private WorkHours workHours;

  protected WorkDone() {

  }

  /** ctor. */
  public WorkDone(final TeamMember teamMember, final Task task, final LocalDate date) {
    this.teamMember = Require.notNull(teamMember);
    this.task = Require.notNull(task);
    this.date = Require.notNull(date);
    this.workHours = WorkHours.ZERO;
  }

  /** ctor. */
  public WorkDone(
      final TeamMember teamMember, 
      final Task task, 
      final LocalDate date, 
      final WorkHours workHours) {
    this(teamMember, task, date);
    this.workHours = workHours;
  }

  public TeamMember getTeamMember() {
    return teamMember;
  }

  public Task getTask() {
    return task;
  }

  @JsonIgnore
  public Long getTaskId() {
    return Require.notNull(this.task).getId();
  }

  public LocalDate getDate() {
    return this.date;
  }

  public String getDateFormatted() {
    return this.date.toString();
  }

  public WorkHours getWorkHours() {
    return workHours;
  }

  public void setWorkHours(final WorkHours workHours) {
    this.workHours = Require.notNull(workHours);
  }

  @Override
  public String toString() {
    return "WorkDone[at " + date + ", for " + task + ": " + workHours + ", " + teamMember + "]";
  }

  public String getCompareString() {
    return "tm:" + this.getTeamMember().getId() + "-t" + this.getTaskId() + "-d" + this.getDate();
  }

  public boolean isValid() {
    return this.getTeamMember() != null && this.getTask() != null && this.getDate() != null
        && this.getWorkHours() != null && this.getWorkHours().asMinutes() >= 0;
  }

}
