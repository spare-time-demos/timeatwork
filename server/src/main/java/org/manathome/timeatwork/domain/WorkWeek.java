package org.manathome.timeatwork.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import io.swagger.v3.oas.annotations.media.Schema;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.stream.Collectors;

import org.manathome.timeatwork.domain.values.WorkHours;
import org.manathome.timeatwork.util.Logger;
import org.manathome.timeatwork.util.assertions.Require;


/** week, holds all work and working assignments.. 
 * @category dto. 
 */
@Schema(description = "week, holds all work and working assignments.")
public class WorkWeek {

  private static final Logger logger = Logger.getLogger(WorkWeek.class);

  private LocalDate from;

  private LocalDate until;

  private TaskWeek[] taskWeeks;

  public WorkWeek() {
    logger.trace("WorkWeeg.ctor(noarg");
  }

  /** ctor. */
  public WorkWeek(final LocalDate from, 
      final LocalDate until, 
      final Collection<TaskAssignment> taskAssignments,
      final Collection<WorkDone> workDone) {

    setFrom(Require.notNull(from, "from"));
    this.until = Require.notNull(until, "until");

    buildTaskWeeks(
        Require.notNull(taskAssignments, "assignments"), 
        Require.notNull(workDone, "workDone"));
  }

  private void buildTaskWeeks(
      Collection<TaskAssignment> taskAssignments, 
      Collection<WorkDone> workDone) {

    final var allTaskIds = new HashSet<Long>(); // all distinct tasks from assignments and workDone.
    taskAssignments.forEach(ta -> allTaskIds.add(ta.getTask().getId()));
    workDone.forEach(wd -> allTaskIds.add(wd.getTask().getId()));

    logger.trace("WorkWeek.buildTaskWeeks: " + allTaskIds.size() + " tasks from " + from);

    this.taskWeeks = allTaskIds.stream()
        .map(taskId -> new TaskWeek(this.from,
            taskAssignments
                .stream()
                .filter(ta -> ta.getTask().getId().longValue() == taskId.longValue()).findFirst()
                .orElse(null),
            workDone.stream().filter(wd -> wd.getTask().getId().longValue() == taskId.longValue())
                .collect(Collectors.toList())))
        .toArray(TaskWeek[]::new);
  }

  @Schema(description = "start of week", example = "2019-01-02")
  public LocalDate getFrom() {
    return from;
  }

  private void setFrom(final LocalDate from) {
    this.from = from;
  }

  @Schema(description = "last day of week", example = "2019-01-08")
  public LocalDate getUntil() {
    return until;
  }

  @Schema(description = "list of tasks for this week")
  public TaskWeek[] getTaskWeeks() {
    return taskWeeks;
  }

  /** sum all working hours in all tasks. */
  @JsonIgnore
  public WorkHours sumWorkHours() {
    return WorkHours
        .ofMinutes(Arrays.stream(taskWeeks)
        .mapToLong(tw -> tw.sumWorkHours().asMinutes())
        .sum());
  }

  @Override
  public String toString() {
    return "WW[from=" + from + ", sumWorkHours()=" + sumWorkHours() + "]";
  }
  
  
  /** checks, throw exceptions. */
  public void validate() {
    Require.notNull(this.getFrom(), "from");
    Require.notNull(this.getTaskWeeks(), "taskWeek");
    if (this.getUntil() != null) {
      Require.isTrue(this.getFrom().isBefore(this.getUntil()), 
                     "invalid date range " + from + " - " + until);
    }
    Arrays.stream(this.getTaskWeeks()).forEach(tw -> tw.validate());
  }

}
