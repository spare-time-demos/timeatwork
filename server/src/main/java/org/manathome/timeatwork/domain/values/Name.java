package org.manathome.timeatwork.domain.values;

import com.fasterxml.jackson.annotation.JsonValue;

import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.manathome.timeatwork.util.assertions.Require;

/**
 * an identifying name.
 * 
 * @category value
 */
@Schema(name = "name", description = "unique descriptive name", required = true, maxLength = 100)
public class Name implements Serializable {

  private static final long serialVersionUID = -7399262699687048362L;
  
  private final String nameValue;

  protected Name() {
    nameValue = null;
  }

  public Name(final @NotNull Name name) {
    this.nameValue = Require.notNull(name, "name required").toString();
  }

  public Name(final @NotNull @NotBlank String name) {
    this.nameValue = Require.notNullOrEmptyWhitespace(name, "non empty or null name required");
  }

  @SuppressWarnings("PMD:ShortMethodName")
  public static Name of(final @NotNull @NotBlank String name) {
    return new Name(name);
  }

  @Override
  public int hashCode() {
    return this.nameValue.hashCode();
  }

  @Override
  public boolean equals(Object obj) {
    return obj != null && getClass() == obj.getClass() && toString().equals(obj.toString());
  }

  @Override
  @JsonValue
  public String toString() {
    return nameValue;
  }

}
