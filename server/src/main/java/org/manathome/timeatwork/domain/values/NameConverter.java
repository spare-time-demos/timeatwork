package org.manathome.timeatwork.domain.values;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter
public class NameConverter implements AttributeConverter<Name, String>  {

  @Override
  public String convertToDatabaseColumn(final Name name) {
    return name == null ? null : name.toString();
  }

  @Override
  public Name convertToEntityAttribute(final String dbStringValue) {
    return dbStringValue == null ? null : Name.of(dbStringValue);
  }

}
