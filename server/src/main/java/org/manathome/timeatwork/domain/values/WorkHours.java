package org.manathome.timeatwork.domain.values;

import com.fasterxml.jackson.annotation.JsonValue;

import java.time.Duration;
import org.manathome.timeatwork.util.assertions.Require;

/**
 * work already done by a team member.
 * stored in fraction of hours in database, output rest in minutes.
 * 
 * @category value
 * 
 * @see WorkHoursConverter for database format
 * @see #toMinutes() for json format
 */
public class WorkHours {

  /** 0 hours. */
  public static final WorkHours ZERO = new WorkHours(0);

  private final Duration duration;

  @SuppressWarnings("PMD:ShortMethodName")
  public static WorkHours ofHours(final int hours) {
    return new WorkHours(hours);
  }

  public static WorkHours ofMinutes(final Long minutes) {
    return new WorkHours(minutes);
  }

  /** ctor. */
  protected WorkHours(int hours) {
    this.duration = Require.notNull(Duration.ofHours(Require.zeroOrPositive(hours)));
  }

  /** ctor. */
  protected WorkHours(final Long minutes) {
    this.duration = Require.notNull(Duration.ofMinutes(Require.notNull(minutes).longValue()));
  }

  /**
   * create new added work hours. does *not* change current instance.
   */
  public WorkHours add(final int addedHours) {
    return this.add(WorkHours.ofHours(addedHours));
  }

  /**
   * create new added work hours. does *not* change current instance.
   */
  public WorkHours add(final WorkHours addedHours) {
    Require.notNull(addedHours);
    return WorkHours.ofHours(this.asHours() + addedHours.asHours());
  }

  public int asHours() {
    return (int) duration.toHours();
  }

  public long asMinutes() {
    return (long) duration.toMinutes();
  }

  public Duration asDuration() {
    return duration;
  }

  /** json rest output in minutes. */
  @JsonValue
  public String toJsonFormatInMinutes() {
    return "" + this.asMinutes();
  }

  @Override
  public String toString() {
    return asHours() + "h";
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((duration == null) ? 0 : duration.hashCode());
    return result;
  }

  @Override
  public boolean equals(final Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    WorkHours other = (WorkHours) obj;

    return duration.equals(other.duration);
  }

}
