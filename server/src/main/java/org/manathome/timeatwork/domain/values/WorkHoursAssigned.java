package org.manathome.timeatwork.domain.values;

import io.swagger.v3.oas.annotations.media.Schema;

import org.manathome.timeatwork.util.assertions.Require;

/** helper class: work done on ui (one entry field for hours a day). */
public class WorkHoursAssigned {

  private boolean isReadOnly = false;

  private WorkHours workHours = WorkHours.ZERO;

  private WorkHoursAssigned() {
  }

  /** .ctor. */
  public WorkHoursAssigned(WorkHours workHours, boolean isReadOnly) {
    this();
    this.isReadOnly = isReadOnly;
    this.workHours = Require.notNull(workHours, "workHours");
  }

  @Schema(description = "current user is able to  change values for this day and task.")
  public boolean isReadOnly() {
    return isReadOnly;
  }

  @Schema(description = "working hours done for given task on this day")
  public int getWorkHours() {
    return workHours == null ? WorkHours.ZERO.asHours() : workHours.asHours();
  }

  @Override
  public String toString() {
    return "WorkHoursAssigned [isReadOnly=" + isReadOnly + ", workHours=" + workHours + "]";
  }

  public WorkHoursAssigned changeTo(int hours) {
    return new WorkHoursAssigned(new WorkHours(hours), this.isReadOnly);
  }

  public WorkHoursAssigned changeTo(WorkHours hours) {
    return new WorkHoursAssigned(hours, this.isReadOnly);
  }

  public WorkHoursAssigned addTo(int addedHours) {
    return new WorkHoursAssigned(this.workHours.add(addedHours), this.isReadOnly);
  }

}
