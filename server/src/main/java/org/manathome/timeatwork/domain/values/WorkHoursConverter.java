package org.manathome.timeatwork.domain.values;

import java.math.BigDecimal;
import java.math.RoundingMode;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

/** converting WorkHours into datbase column format (fraction of hours). */
@Converter
public class WorkHoursConverter implements AttributeConverter<WorkHours, BigDecimal>  {

  private static BigDecimal MINUTES_IN_HOUR = new BigDecimal(60);
  
  @Override
  public BigDecimal convertToDatabaseColumn(final WorkHours workHours) {
    
    if (workHours == null) {
      return null;
    }
    
    return new BigDecimal(workHours.asDuration().toMinutes())
                  .divide(MINUTES_IN_HOUR, 2, RoundingMode.UP);
  }

  @Override
  public WorkHours convertToEntityAttribute(final BigDecimal hoursAsDecimal) {
    
    if (hoursAsDecimal == null) {
      return null;
    }

    return new WorkHours(hoursAsDecimal.intValue());  // todo: lost minutes..
  }

}
