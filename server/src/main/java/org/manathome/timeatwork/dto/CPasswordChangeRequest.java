package org.manathome.timeatwork.dto;

import io.swagger.v3.oas.annotations.media.Schema;

@Schema(description = "password change") 
public class CPasswordChangeRequest {

  private String oldPassword;

  private String newPassword;

  @Schema(description = "currently used password for verification", 
      example = "oldPassword123456", 
      required = true)  
  public String getOldPassword() {
    return oldPassword;
  }

  public void setOldPassword(String oldPassword) {
    this.oldPassword = oldPassword;
  }

  @Schema(description = "new password, successor to old password", 
      example = "moreSecure", 
      required = true)  
  public String getNewPassword() {
    return newPassword;
  }

  public void setNewPassword(String newPassword) {
    this.newPassword = newPassword;
  }

}
