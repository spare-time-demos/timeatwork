package org.manathome.timeatwork.dto;

import io.swagger.v3.oas.annotations.media.Schema;

public class CPasswordChangeResult {


  @Schema(description = "true if change was successful.")  
  public final boolean ok;
  
  @Schema(description = "a human readable message describing failed attempts.")  
  public final String message;

  
  public boolean isOk() {
    return ok;
  }

  public String getMessage() {
    return message;
  }
  
  public CPasswordChangeResult(boolean ok, String message) {
    this.ok = ok;
    this.message = message;
  }
}
