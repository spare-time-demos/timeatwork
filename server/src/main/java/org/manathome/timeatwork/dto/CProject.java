package org.manathome.timeatwork.dto;

import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;
import java.util.Arrays;

import org.manathome.timeatwork.domain.Project;
import org.manathome.timeatwork.domain.values.Name;
import org.manathome.timeatwork.util.assertions.Require;

/** 
 * public faced data of a given project in time@work. 
 * @category dto. 
 */
@Schema(name = "Project", description = "tracked project.")
public class CProject implements Serializable {

  private static final long serialVersionUID = 1L;

  /** pk. */
  private Long id;

  /** project name. */
  private String name;

  /** is project actively used. */
  private boolean active;

  private CTeamMember[] projectLeads;

  @Schema(description = "unique project id.", example = "0815")
  public Long getId() {
    return id;
  }

  @Schema(description = "unique project name", minLength = 1, maxLength = 100, 
          type = "string", example = "my unique project name")  
  public String getName() {
    return name;
  }

  @Schema(description = "is project active for administration by project lead.", 
      example = "true", required = true)  
  public boolean isActive() {
    return active;
  }

  @Schema(name = "projectLeads", description = "project leads (administrator)", required = false)  
  public CTeamMember[] getProjectLeads() {
    return projectLeads;
  }

  /** required for json. */
  protected CProject() {

  }

  /** builder. */
  public CProject(final Project project) {

    this.id = Require.notNull(project).getId();
    this.name = project.getName().toString();
    this.active = project.isActive();
    this.projectLeads = project.getProjectLeads().stream()
        .map(CTeamMember::new)
        .toArray(CTeamMember[]::new);
  }
  
  
  /** converter. */
  public Project asProject() {
    final var p = new Project(Name.of(getName()));
    p.setActive(isActive());
    Arrays.stream(Require.notNull(getProjectLeads()))
           .filter(ctm -> ctm != null)
           .map(CTeamMember::asTeamMember)
           .forEach(p::addProjectLead);           
    return p;
  }

}
