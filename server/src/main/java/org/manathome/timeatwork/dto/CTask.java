package org.manathome.timeatwork.dto;

import java.io.Serializable;

import org.manathome.timeatwork.domain.Task;
import org.manathome.timeatwork.util.assertions.Require;

/** 
 * public faced data. 
 * @category dto. 
 * 
 * @see Task
 */
public class CTask implements Serializable {
  
  private static final long serialVersionUID = 1583016716978780207L;

  /** pk. */
  private Long id;

  /** project name. */
  private String name;
  
  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  protected CTask() {    
  }
  
  /** fill from task. */
  public CTask(Task task) {
    this.id = Require.notNull(task).getId();
    this.name = task.getName().toString();
  }

  @Override
  public String toString() {
    return "CTask [id=" + id + ", name=" + name + "]";
  }

}
