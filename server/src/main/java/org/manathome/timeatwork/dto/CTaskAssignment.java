package org.manathome.timeatwork.dto;

import io.swagger.v3.oas.annotations.media.Schema;

import java.time.LocalDate;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import org.manathome.timeatwork.domain.TaskAssignment;
import org.manathome.timeatwork.util.assertions.Require;

/** 
 * public faced data. 
 * 
 * @category dto. 
 * 
 * @see TaskAssignment
 */
@Schema(name = "TaskAssignment", 
        description = "planned assignment of a team member to a given project task.")
public class CTaskAssignment {

  private Long id;

  private CTask task;

  private CTeamMember teamMember;

  private LocalDate from;

  private LocalDate until;

  public Long getId() {
    return id;
  }

  public @Nonnull CTask getTask() {
    return task;
  }

  public @Nonnull CTeamMember getTeamMember() {
    return teamMember;
  }

  public @Nonnull LocalDate getFrom() {
    return from;
  }

  public @Nullable LocalDate getUntil() {
    return until;
  }

  /** builder. */
  protected CTaskAssignment() {
    
  }

  /** builder. */
  public CTaskAssignment(final TaskAssignment assignment) {
    
    id = Require.notNull(assignment).getId();
    task = new CTask(assignment.getTask());
    teamMember = new CTeamMember(assignment.getTeamMember());
    from = assignment.getFrom();
    until = assignment.getUntil();
  }
  
}
