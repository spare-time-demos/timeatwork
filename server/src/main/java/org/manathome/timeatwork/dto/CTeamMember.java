package org.manathome.timeatwork.dto;

import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;

import org.manathome.timeatwork.domain.TeamMember;
import org.manathome.timeatwork.domain.values.Name;
import org.manathome.timeatwork.util.assertions.Require;

/** 
 * public faced data. 
 * @category dto. 
 * 
 * @see TeamMember
 */
@Schema(name = "TeamMember", 
        description = "a team member, could be project lead or an member with assigned tasks.")
public class CTeamMember implements Serializable {

  private static final long serialVersionUID = -679235760849574385L;

  /** pk. */
  private Long id;

  /** user name. */
  private String name;

  /** user id. */
  private String userId;

  @Schema(description = "unique internal member id", example = "4712", required = false)
  public Long getId() {
    return id;
  }

  /** Surname or other name of employee, used for display on screen. */
  @Schema(description = "surname or other name of employee, used for display on screen.", 
          example = "Tom Teammember", type = "string", maxLength = 100, required = true) 
  public String getName() {
    return name;
  }

  @Schema(description = "unique user id, used during login of this member.", 
          example = "tlead1", type = "string", maxLength = 25, required = true)  
  public String getUserId() {
    return userId;
  }

  
  /** required for json. */
  protected CTeamMember() {
    
  }
  
  /** fill from teamMember. */
  public CTeamMember(final TeamMember teamMember) {
    this.id = Require.notNull(teamMember).getId();
    this.name = teamMember.getName().toString();
    this.userId = teamMember.getUserId().toString();
  }
  
  public TeamMember asTeamMember() {
    return new TeamMember(id, Name.of(getUserId()), Name.of(getName()));
  }
}
