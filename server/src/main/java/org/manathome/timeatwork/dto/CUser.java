package org.manathome.timeatwork.dto;

import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;

import org.manathome.timeatwork.domain.TeamMember;
import org.manathome.timeatwork.domain.values.Name;
import org.manathome.timeatwork.util.assertions.Require;

/** public faced data for user. 
 * @category dto. 
 * 
 * @see TeamMember
 */
@Schema(name = "User", 
        description = "a time@work user (is always also a team member).")
public class CUser implements Serializable {

  private static final long serialVersionUID = -679235760849574385L;

  /** pk. */
  private Long id;

  /** user name. */
  private String name;

  /** user id. */
  private String userId;
  
  private boolean isActive;

  private boolean isLocked;
  
  private boolean isAdmin;
  
  private boolean isLead;
    
  public void setId(Long id) {
    this.id = id;
  }

  public void setName(String name) {
    this.name = name;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public void setActive(boolean isActive) {
    this.isActive = isActive;
  }

  public void setLocked(boolean isLocked) {
    this.isLocked = isLocked;
  }

  public void setAdmin(boolean isAdmin) {
    this.isAdmin = isAdmin;
  }

  public void setLead(boolean isLead) {
    this.isLead = isLead;
  }

  @Schema(description = "unique internal member id", example = "4712", required = false)
  public Long getId() {
    return id;
  }

  /** Surname or other name of employee, used for display on screen. */
  @Schema(description = "surname or other name of employee, used for display on screen.", 
          example = "Tom Teammember", type = "string", maxLength = 100, required = true) 
  public String getName() {
    return name;
  }

  @Schema(description = "unique user id, used during login of this member.", 
          example = "tlead1", type = "string", maxLength = 25, required = true)  
  public String getUserId() {
    return userId;
  }

  
  /** required for json. */
  protected CUser() {
    
  }
  
  /** test builder. */
  public static CUser buildTestuser(String userId, String name) {
    final var u = new CUser();
    u.userId = userId;
    u.name = name;
    u.isActive = true;
    u.isLocked = false;
    u.isLead = true;
    return u;
  }
  
  /** fill from teamMember. */
  public CUser(final TeamMember teamMember) {
    this.id = Require.notNull(teamMember, "teamMember as CUser").getId();
    this.name = teamMember.getName().toString();
    this.userId = teamMember.getUserId().toString();
    this.isActive = teamMember.isActive();
    this.isLocked = teamMember.isLocked();
    this.isAdmin = teamMember.hasAdminRole();
    this.isLead = teamMember.hasLeadRole();
  }
  
  /** convert team member. */
  public TeamMember asTeamMember() {
    
    final var tm = new TeamMember(
        this.getId(), 
        Name.of(this.getUserId()), 
        Name.of(this.getName())
        );
    
    tm.setActive(this.isActive());
    tm.setLocked(this.isLocked());
    
    tm.setAdminRole(this.isAdmin());
    tm.setLeadRole(this.isLead());
    
    return tm;
  }

  @Schema(description = "user is active, (otherwise no login, nor a new assignment is possible)", 
          example = "true", 
          required = true)
  public boolean isActive() {
    return isActive;
  }

  @Schema(description = "user is locked, no login possible", example = "false", required = true)
  public boolean isLocked() {
    return isLocked;
  }

  @Schema(description = "user has admin role (is an admin user)", 
          example = "false", 
          required = true)
  public boolean isAdmin() {
    return isAdmin;
  }

  @Schema(description = "user has project lead role", example = "true", required = true)
  public boolean isLead() {
    return isLead;
  }

}
