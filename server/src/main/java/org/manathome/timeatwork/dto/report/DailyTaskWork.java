package org.manathome.timeatwork.dto.report;

import static java.util.stream.Collectors.groupingBy;

import io.swagger.v3.oas.annotations.media.Schema;

import java.time.LocalDate;
import java.util.List;

import org.manathome.timeatwork.domain.WorkDone;
import org.manathome.timeatwork.util.Logger;
import org.manathome.timeatwork.util.assertions.Require;

@Schema(description = "daily taskwork by user (reporting).")
public class DailyTaskWork {
  
  private static final Logger logger = Logger.getLogger(DailyTaskWork.class);

  private LocalDate date;
  
  private String projectName;
  private String taskName;
  
  private int workHours;

  /** ctor for exactly one day. */
  public static DailyTaskWork from(List<WorkDone> workDone) {
    
    Require.notNull(workDone, "workDone");
    Require.isTrue(workDone.size() > 0, "no work done given.");
    
    logger.trace("     daily task work " + workDone.get(0).getDate() + ":" + workDone.size());
    
    DailyTaskWork dtw = new DailyTaskWork();
    dtw.setDate(workDone.get(0).getDate());
    dtw.setProjectName(workDone.get(0).getTask().getProject().getName().toString());
    dtw.setTaskName(workDone.get(0).getTask().getName().toString());
    dtw.setWorkHours(workDone.stream().mapToInt(w -> w.getWorkHours().asHours()).sum());
    return dtw;
  }
  
  /** ctor for multiple days. */
  public static DailyTaskWork[] fromMultipleDays(List<WorkDone> workDone) {
    
    Require.notNull(workDone, "workDone");
    Require.isTrue(workDone.size() > 0, "no work done given.");
    
    logger.trace("     all task work: " + workDone.size());
    
    DailyTaskWork[] dtws = workDone.stream()    
            .collect(groupingBy(WorkDone::getDateFormatted))
            .entrySet()
            .stream()
            .map(e -> DailyTaskWork.from(e.getValue()))
            .toArray(DailyTaskWork[]::new);
            
    return dtws;
  }

  public LocalDate getDate() {
    return date;
  }

  public void setDate(LocalDate date) {
    this.date = date;
  }

  public String getProjectName() {
    return projectName;
  }

  public void setProjectName(String projectName) {
    this.projectName = projectName;
  }

  public String getTaskName() {
    return taskName;
  }

  public void setTaskName(String taskName) {
    this.taskName = taskName;
  }

  public int getWorkHours() {
    return workHours;
  }

  public void setWorkHours(int workHours) {
    this.workHours = workHours;
  }  
}