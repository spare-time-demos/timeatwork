package org.manathome.timeatwork.dto.report;

import static java.util.stream.Collectors.groupingBy;

import io.swagger.v3.oas.annotations.media.Schema;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import org.manathome.timeatwork.domain.WorkDone;
import org.manathome.timeatwork.util.Logger;
import org.manathome.timeatwork.util.assertions.Require;

@Schema(description = "daily work in month by user (reporting).")
public class DailyWorkReport {

  private static final Logger logger = Logger.getLogger(DailyWorkReport.class);

  private LocalDate date;

  private int dailyHours;
  
  private DailyTaskWork[] dailyTaskWorks;

  /** ctor. */
  public static DailyWorkReport from(final LocalDate date, final List<WorkDone> workDone) {
    final var dwr = new DailyWorkReport();
    dwr.setDate(Require.notNull(date,"date"));
    
    DailyTaskWork[] dtw = workDone.stream()
        .collect(groupingBy(WorkDone::getTaskId))
        .entrySet()
        .stream()
        .map(e -> DailyTaskWork.from(e.getValue()))
        .toArray(DailyTaskWork[]::new);

    
    dwr.setDailyTaskWorks(dtw);
    dwr.setDailyHours(Arrays.stream(dwr.getDailyTaskWorks()).mapToInt(d -> d.getWorkHours()).sum());
    
    logger.trace("   daily work " + date + " tasks found: " + dwr.getDailyTaskWorks().length);
    return dwr;
  }

  public LocalDate getDate() {
    return date;
  }

  public void setDate(LocalDate date) {
    this.date = date;
  }

  public int getDailyHours() {
    return dailyHours;
  }

  public void setDailyHours(int dailyHours) {
    this.dailyHours = dailyHours;
  }

  public DailyTaskWork[] getDailyTaskWorks() {
    return dailyTaskWorks;
  }

  public void setDailyTaskWorks(DailyTaskWork[] dailyTaskWorks) {
    this.dailyTaskWorks = dailyTaskWorks;
  }
}