package org.manathome.timeatwork.dto.report;

import static java.util.stream.Collectors.groupingBy;

import io.swagger.v3.oas.annotations.media.Schema;

import java.util.Arrays;
import java.util.List;

import org.manathome.timeatwork.domain.WorkDone;
import org.manathome.timeatwork.util.Logger;
import org.manathome.timeatwork.util.assertions.Require;

@Schema(description = "all work by user in given month, gropued by day, project, task (reporting).")
public class MonthlyWorkByDayReport {
  
  private static final Logger logger = Logger.getLogger(MonthlyWorkByDayReport.class);

  private int monthlyHours;

  private DailyWorkReport[] dailyWorkReports;

  /** ctor. */
  public static MonthlyWorkByDayReport from(final List<WorkDone> workDone) {
    
    final var mwr = new MonthlyWorkByDayReport();

    if (Require.notNull(workDone).size() > 0) {
      DailyWorkReport[] dwr = workDone.stream()
                                    .collect(groupingBy(WorkDone::getDate))
                                    .entrySet()
                                    .stream()
                                    .map(e -> DailyWorkReport.from(e.getKey(), e.getValue()))
                                    .toArray(DailyWorkReport[]::new);
    
      logger.trace(" days found in month: " + dwr.length);
      
      mwr.setDailyWorkReports(dwr);
      mwr.setMonthlyHours(Arrays.stream(mwr.getDailyWorkReports())
                               .mapToInt(dr -> dr.getDailyHours())
                               .sum());
    }
    return mwr;
  }

  public int getMonthlyHours() {
    return monthlyHours;
  }

  public void setMonthlyHours(int monthlyHours) {
    this.monthlyHours = monthlyHours;
  }

  public DailyWorkReport[] getDailyWorkReports() {
    return dailyWorkReports;
  }

  public void setDailyWorkReports(DailyWorkReport[] dailyWorkReports) {
    this.dailyWorkReports = dailyWorkReports;
  }
}


