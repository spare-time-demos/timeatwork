package org.manathome.timeatwork.dto.report;

import static java.util.stream.Collectors.groupingBy;

import io.swagger.v3.oas.annotations.media.Schema;

import java.util.List;

import org.manathome.timeatwork.domain.WorkDone;
import org.manathome.timeatwork.util.Logger;
import org.manathome.timeatwork.util.assertions.Require;

@Schema(description = "all work by user in given month, gropued by day, project, task (reporting).")
public class MonthlyWorkByProjectReport {
  
  private static final Logger logger = Logger.getLogger(MonthlyWorkByProjectReport.class);

  private int monthlyHours;

  private ProjectWorkReport[] projectWorkReports;

  /** ctor. */
  public static MonthlyWorkByProjectReport from(final List<WorkDone> workDone) {
    
    final var mwr = new MonthlyWorkByProjectReport();

    if (Require.notNull(workDone).size() > 0) {
      
      ProjectWorkReport[] pwr = workDone.stream()
              .collect(groupingBy(w -> w.getTask().getProject().getId()))
              .entrySet()
              .stream()
              .map(entrySet -> ProjectWorkReport.from(
                  entrySet.getValue().get(0).getTask().getProject(), 
                  entrySet.getValue())
                  )
              .toArray(ProjectWorkReport[]::new);
      
      logger.trace(" projects found in month: " + pwr.length);
      
      mwr.setProjectWorkReports(pwr);
      mwr.setMonthlyHours(workDone.stream()
                               .mapToInt(wd -> wd.getWorkHours().asHours())
                               .sum());
    }
    return mwr;
  }

  public int getMonthlyHours() {
    return monthlyHours;
  }

  public void setMonthlyHours(int monthlyHours) {
    this.monthlyHours = monthlyHours;
  }

  public ProjectWorkReport[] getProjectWorkReports() {
    return projectWorkReports;
  }

  public void setProjectWorkReports(ProjectWorkReport[] projectWorkReports) {
    this.projectWorkReports = projectWorkReports;
  }
}


