package org.manathome.timeatwork.dto.report;

import static java.util.stream.Collectors.groupingBy;

import io.swagger.v3.oas.annotations.media.Schema;

import java.util.Arrays;
import java.util.List;

import org.manathome.timeatwork.domain.Project;
import org.manathome.timeatwork.domain.WorkDone;
import org.manathome.timeatwork.dto.CProject;
import org.manathome.timeatwork.util.Logger;
import org.manathome.timeatwork.util.assertions.Require;

@Schema(description = "project work in month by user (reporting).")
public class ProjectWorkReport {

  private static final Logger logger = Logger.getLogger(ProjectWorkReport.class);

  private CProject project;
  
  private String projectName;

  private int projectHours;
  
  private TaskWork[] taskWorks;

  /** ctor. */
  public static ProjectWorkReport from(final Project project, final List<WorkDone> workDone) {
    
    final var pwr = new ProjectWorkReport();
    
    pwr.setProject(new CProject(Require.notNull(project)));
    pwr.setProjectName(project.getName().toString());
    
    TaskWork[] tw = workDone.stream()
        .collect(groupingBy(WorkDone::getTaskId))
        .entrySet()
        .stream()
        .map(e -> TaskWork.from(e.getValue()))
        .toArray(TaskWork[]::new);

    
    pwr.setTaskWorks(tw);
    pwr.setProjectHours(Arrays.stream(pwr.getTaskWorks()).mapToInt(d -> d.getWorkHours()).sum());
    
    logger.trace("   task work " + project.getName() 
               + " tasks found:" + pwr.getTaskWorks().length);
    return pwr;
  }

  public CProject getProject() {
    return project;
  }

  public void setProject(CProject project) {
    this.project = project;
  }

  public String getProjectName() {
    return projectName;
  }

  public void setProjectName(String projectName) {
    this.projectName = projectName;
  }

  public int getProjectHours() {
    return projectHours;
  }

  public void setProjectHours(int projectHours) {
    this.projectHours = projectHours;
  }

  public TaskWork[] getTaskWorks() {
    return taskWorks;
  }

  public void setTaskWorks(TaskWork[] taskWorks) {
    this.taskWorks = taskWorks;
  }

}