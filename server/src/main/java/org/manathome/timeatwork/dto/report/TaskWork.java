package org.manathome.timeatwork.dto.report;

import io.swagger.v3.oas.annotations.media.Schema;

import java.time.LocalDate;
import java.util.List;

import org.manathome.timeatwork.domain.WorkDone;
import org.manathome.timeatwork.util.Logger;
import org.manathome.timeatwork.util.assertions.Require;

@Schema(description = "projects taskwork by user (reporting).")
public class TaskWork {
  
  private static final Logger logger = Logger.getLogger(TaskWork.class);

  private LocalDate date;
  
  private String projectName;
  private String taskName;
  
  private int workHours;
  private DailyTaskWork[] dailyTaskWorks;

  /** ctor. */
  public static TaskWork from(List<WorkDone> workDone) {
    
    Require.notNull(workDone, "workDone");
    Require.isTrue(workDone.size() > 0, "no work done given.");
    
    logger.trace("     task work " + workDone.get(0).getTask().getName());
    
    TaskWork tw = new TaskWork();
    tw.setProjectName(workDone.get(0).getTask().getProject().getName().toString());
    tw.setTaskName(workDone.get(0).getTask().getName().toString());
    tw.setWorkHours(workDone.stream().mapToInt(w -> w.getWorkHours().asHours()).sum());
    tw.setDailyTaskWorks(DailyTaskWork.fromMultipleDays(workDone));
    return tw;
  }

  public LocalDate getDate() {
    return date;
  }

  protected void setDate(LocalDate date) {
    this.date = date;
  }

  public String getProjectName() {
    return projectName;
  }

  protected void setProjectName(String projectName) {
    this.projectName = projectName;
  }

  public String getTaskName() {
    return taskName;
  }

  protected void setTaskName(String taskName) {
    this.taskName = taskName;
  }

  public int getWorkHours() {
    return workHours;
  }

  protected void setWorkHours(int workHours) {
    this.workHours = workHours;
  }

  public DailyTaskWork[] getDailyTaskWorks() {
    return dailyTaskWorks;
  }

  protected void setDailyTaskWorks(DailyTaskWork[] dailyTaskWorks) {
    this.dailyTaskWorks = dailyTaskWorks;
  }
}