package org.manathome.timeatwork.rest;

import static java.time.temporal.TemporalAdjusters.firstDayOfMonth;
import static java.time.temporal.TemporalAdjusters.lastDayOfMonth;

import io.micrometer.core.annotation.Timed;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;

import java.time.LocalDate;
import java.util.List;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.validation.constraints.NotNull;

import org.manathome.timeatwork.db.repository.TeamMemberWorkReportQuery;
import org.manathome.timeatwork.db.repository.result.TeamMemberWorkDoneResult;
import org.manathome.timeatwork.security.UserRoleName;
import org.manathome.timeatwork.util.Logger;
import org.manathome.timeatwork.util.assertions.Require;

import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;


/** http api for monthly reporting. */
@RestController
@CrossOrigin
@SecurityRequirement(name = ResourceHelper.AUTHENTICATION_HEADER)
@Tag(name = OpenapiTags.TAG_UI_ADMIN)
@Secured({UserRoleName.Secured.ROLE_ADMIN})
@Timed
public class AdminReportResource {
  
  private static final Logger logger = Logger.getLogger(AdminReportResource.class);

  public static final String PATH_REPORT_USER = ResourceHelper.PATH_BASE_REST 
      + "report/user/";
  
  public static final String FROM_ISO_DATE_STRING = "from-iso-date-string";
  
  @Inject
  private  TeamMemberWorkReportQuery teamMemberworkReportQuery;
  
 
  /** rest. */
  @Operation(summary = "monthly work done by users grouped by user, date format " 
                     + FROM_ISO_DATE_STRING + ": YYYY-MM-DD.")
  @GetMapping(path = PATH_REPORT_USER + "{" + FROM_ISO_DATE_STRING + "}/byWork", 
              produces = ResourceHelper.CONTENT_TYPE_JSON)
  @Transactional
  public List<TeamMemberWorkDoneResult> reportMonthlyByDay(
      @Parameter(description = "date, start of report, format YYYY-MM-DD", example = "2019-03-01") 
      @NotNull 
      @PathVariable(name = FROM_ISO_DATE_STRING) 
      final String fromIsoDateString) {
    
    logger.trace(PATH_REPORT_USER + fromIsoDateString + "/byWork");
    
    final var from = LocalDate.parse(Require.notNull(fromIsoDateString, "from"))
                              .with(firstDayOfMonth());
    final var until = from.with(lastDayOfMonth());

    final var teamMemberRows = teamMemberworkReportQuery
        .readUsersOrderByWorkDoneAscending(from, until);

    logger.trace("found users: " + teamMemberRows.size());
    return teamMemberRows;
  }
  
}
