package org.manathome.timeatwork.rest;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;

import java.security.Principal;
import java.util.Date;

import javax.validation.constraints.NotNull;

import org.manathome.timeatwork.security.UserRoleName;
import org.manathome.timeatwork.util.Logger;

import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/** technical admin endpoints. */
@RestController
@CrossOrigin
@SecurityRequirement(name = ResourceHelper.AUTHENTICATION_HEADER)
@Tag(name = OpenapiTags.TAG_ADMIN)
@Secured({UserRoleName.Secured.ROLE_ADMIN})
public class AdminResource {
  
  private static final Logger logger = Logger.getLogger(AdminResource.class);

  public static final String PATH_ADMIN      = ResourceHelper.PATH_BASE_REST + "admin/";
  public static final String PATH_ADMIN_INFO = PATH_ADMIN + "info";

  public static final String ADMIN_HEADER = "time@work info;";
  
  /**
   * dummy output.
   */
  @Operation(summary = "(dummy) application info endpoint.")
  @GetMapping(PATH_ADMIN_INFO)
  public @NotNull String info(final Principal principal) {
    logger.debug(PATH_ADMIN_INFO + " call as user " + principal);
    
    return 
        ADMIN_HEADER + " \n" 
        + " at " + new Date() 
        + " as " + (principal == null ? "unknown" : principal.getName());
  }

}
