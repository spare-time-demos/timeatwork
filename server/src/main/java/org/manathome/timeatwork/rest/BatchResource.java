package org.manathome.timeatwork.rest;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;

import javax.validation.constraints.NotNull;

import org.manathome.timeatwork.security.UserRoleName;
import org.manathome.timeatwork.util.Logger;
import org.manathome.timeatwork.util.TimeAtWorkInternalException;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/** spring batch job starter. */
@RestController
@CrossOrigin
@SecurityRequirement(name = ResourceHelper.AUTHENTICATION_HEADER)
@Tag(name = OpenapiTags.TAG_ADMIN)
@Secured({UserRoleName.Secured.ROLE_ADMIN})
public class BatchResource {
  
  private static final Logger logger = Logger.getLogger(BatchResource.class);

  public static final String PATH_BATCH =   AdminResource.PATH_ADMIN;

  @Autowired
  private JobLauncher jobLauncher;

  @Autowired
  private Job job;

  /** rest. 
   * 
   * @exception TimeAtWorkInternalTestException unexpected...
   * */
  @Operation(summary = "start background job: usersync")
  @GetMapping(PATH_BATCH + "job/usersync")
  public @NotNull String startuserSync() {

    final JobParameters jobParameters = new JobParametersBuilder()
        .addLong("requestTime", System.currentTimeMillis())
        .addString("requestType", "rest")
        .toJobParameters();

    try {
      final var exec = jobLauncher.run(job, jobParameters);
      return "started: " 
          + exec.getJobInstance().getJobName() 
          + " - " 
          + exec.getJobInstance().getId();
    } catch (Exception ex) {
      logger.error("error sync user job" + ex, ex);
      throw new TimeAtWorkInternalException("error sync user job: " + ex.getMessage(), ex);
    }
  }

}
