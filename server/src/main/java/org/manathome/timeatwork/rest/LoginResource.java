package org.manathome.timeatwork.rest;

import io.micrometer.core.annotation.Timed;
import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;

import java.security.Principal;

import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;
import javax.validation.constraints.NotNull;

import org.manathome.timeatwork.dto.CPasswordChangeRequest;
import org.manathome.timeatwork.dto.CPasswordChangeResult;
import org.manathome.timeatwork.security.AppUser;
import org.manathome.timeatwork.security.AppUserDetailsService;
import org.manathome.timeatwork.security.AuthenticationRequest;
import org.manathome.timeatwork.security.AuthenticationResult;
import org.manathome.timeatwork.security.JwtTokenProcessor;
import org.manathome.timeatwork.security.UserRoleName;
import org.manathome.timeatwork.service.AuditService;
import org.manathome.timeatwork.service.LocalizedValidation;
import org.manathome.timeatwork.util.InvalidDataGivenException;
import org.manathome.timeatwork.util.Logger;
import org.manathome.timeatwork.util.MetricName;
import org.manathome.timeatwork.util.assertions.Require;
import org.manathome.timeatwork.util.generated.LocalizedMessages;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/** Login (get an valid jwt token with user name + password. 
 * https://medium.com/@hantsy/protect-rest-apis-with-spring-security-and-jwt-5fbc90305cc5
 */
@RestController
@CrossOrigin
@Tag(name = OpenapiTags.TAG_SECURITY)
@Timed
@Transactional
public class LoginResource {
  
  private static final Logger logger = Logger.getLogger(LoginResource.class);

  public static final String URL_CHANGE_PASSWORD = ResourceHelper.PATH_BASE_REST +  "password";
  public static final String URL_AUTHENTICATION = "/authentication";
  
  public static final String TOKEN_HEADER_AUTHORIZATION = "Authorization";
  public static final String TOKEN_PREFIX_AUTHORIZATION = "Bearer ";
  
  public static final String SUCCESS_MESSAGE = LocalizedMessages.login_success_message;
  public static final String FAILED_LOGIN_MESSAGE = LocalizedMessages.login_failed_message;

  @Autowired
  public JwtTokenProcessor jwtTokenProcessor; 

  @Autowired
  public AuthenticationManager authenticationManager;
  
  @Autowired
  private AppUserDetailsService appUserService;  
  
  @Autowired
  public AuditService auditService;
  
  @Autowired
  LocalizedValidation validation;
  
  private final Counter loginOkCounter;

  private final Counter loginFailedCounter;
  
  /** .ctor */
  public LoginResource(MeterRegistry registry) {
    
    loginOkCounter = registry.counter(
        MetricName.login, 
        MetricName.MetricCommonLabel.result, "ok");
    
    loginFailedCounter = registry.counter(
        MetricName.login, 
        MetricName.MetricCommonLabel.result, "failed");
    
  }

  /** login with userName and password, getting an jwt token on successful.
   */
  @Operation(summary = "login with username and password, getting an jwt token on successful.")  
  @PostMapping(URL_AUTHENTICATION)
  public AuthenticationResult login(
      final @RequestBody AuthenticationRequest authenticationRequest,
      final HttpServletResponse response) {
    
    logger.info("authentication attempt with " + URL_AUTHENTICATION 
        + " as " + authenticationRequest); 

    try {
      if (authenticationRequest == null) {
        throw new BadCredentialsException(
            "no login data provided in call to " + URL_AUTHENTICATION);
      }
            
      final var authenticationToken = new UsernamePasswordAuthenticationToken(
          authenticationRequest.getUserId(), 
          authenticationRequest.getPassword());
   
      final var auth = authenticationManager.authenticate(authenticationToken);
      Require.isTrue(auth.isAuthenticated(), "authenticated postcondition");
      
      final String tokenString = jwtTokenProcessor
          .buildTokenForUser((UserDetails) auth.getPrincipal());
      
      response.addHeader(TOKEN_HEADER_AUTHORIZATION, TOKEN_PREFIX_AUTHORIZATION + tokenString);
      
      final boolean hasRoleProjectLead = auth.getAuthorities()
          .stream()
          .anyMatch(role -> UserRoleName.isSameRole(role.getAuthority(), UserRoleName.PROJECTLEAD));

      final boolean hasRoleAdmin = auth.getAuthorities()
          .stream()
          .anyMatch(role -> UserRoleName.isSameRole(role.getAuthority(), UserRoleName.ADMIN));
      
      // LOGIN SUCCESS
      // -------------
      
      loginOkCounter.increment();      
      auditService.logUserEvent("authentication attempt sucessfull as " + authenticationRequest);
      
      return new AuthenticationResult(
          true, 
          validation.getMessage(
              SUCCESS_MESSAGE, 
              new Object[] { auth.getName() }),
          tokenString,
          auth.getName(),
          hasRoleProjectLead,
          hasRoleAdmin
          );
      
    } catch (final BadCredentialsException bce) {
      
      // LOGIN FAILED
      // -------------
      
      loginFailedCounter.increment();
      
      auditService.logUserEvent(
          "authentication attempt failed as " 
          + authenticationRequest + ": " + FAILED_LOGIN_MESSAGE);
      
      return new AuthenticationResult(
          false, 
          validation.getMessage(FAILED_LOGIN_MESSAGE),
          null,
          null,
          false,
          false);
    
    } catch (final Exception ex) {

      loginFailedCounter.increment();

      auditService.logUserEvent(
          "authentication attempt failed as " 
          + authenticationRequest + ": "  + ex.getMessage());
      
      return new AuthenticationResult(
          false, 
          validation.getMessage(FAILED_LOGIN_MESSAGE),
          null,
          null,
          false,
          false);      
    }
    
  }
  
  /** rest. */
  @Operation(summary = "change password for an existing user.")
  @PostMapping(path = URL_CHANGE_PASSWORD,
      consumes = ResourceHelper.CONTENT_TYPE_JSON, 
      produces = ResourceHelper.CONTENT_TYPE_JSON)
  @Transactional 
  public CPasswordChangeResult changePassword(
      @NotNull final Principal principal,
      @NotNull final @RequestBody CPasswordChangeRequest password) {

    logger.trace(URL_CHANGE_PASSWORD);
    
    Require.notNull(password, "request");
    
    validation.validateNotNullOrEmptyWhitespace(
        password.getOldPassword(), "changePassword.noOldPassword");
    validation.validateNotNullOrEmptyWhitespace(
        password.getNewPassword(), "changePassword.noNewPassword");

    final var currentUser = Require.notNull(appUserService.getCurrentTeamMember(), 
                                            "no currently user logged in");
    
    Require.isTrue(currentUser.isActive(), "active user required");
    Require.isTrue(currentUser.userMayLogIn(), "user must be able to log in.");
    
    // validate old password
    if (!appUserService.validatePassword(
        new AppUser(currentUser), 
        password.getOldPassword())) {
      throw new InvalidDataGivenException("wrong password given");
    }
    
    // change to new password
    String encryptedNewPassword; 
    try {
      
      encryptedNewPassword = appUserService.encryptPassword(password.getNewPassword());
            
      currentUser.setLocked(false);
      currentUser.setPassword(encryptedNewPassword);
      
      // validate new password..
      Require.isTrue(appUserService.validatePassword(
          new AppUser(currentUser), 
          password.getNewPassword()), 
          "new pw must be working");
      
    } catch (Exception ex) {
      throw new InvalidDataGivenException(ex.getMessage());      
    }
    
    final var stm = appUserService.saveUser(currentUser);
    
    Require.isTrue(
        stm.getPassword().contentEquals(encryptedNewPassword), 
        "encryted pw changed on save!");
    
    Require.isTrue(currentUser.userMayLogIn(), "active unlocked user postcondition");
      
    auditService.logUserEvent(
        "password changed " 
        + currentUser 
        + ", "
        + "new length " + encryptedNewPassword.length());
    
    return new CPasswordChangeResult(true, "password changed successfully");
  }
  
  /** map this exception to 401 also. */
  @ExceptionHandler({ HttpMessageNotReadableException.class })
  @ResponseStatus(value = HttpStatus.UNAUTHORIZED, reason = "login data not readable")
  public void handleException(HttpMessageNotReadableException ex) {
    
    loginFailedCounter.increment();
    auditService.logUserEvent("login data not readable: " + ex.getMessage());
    
  }
}
