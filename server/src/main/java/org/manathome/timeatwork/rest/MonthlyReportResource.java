package org.manathome.timeatwork.rest;

import static java.time.temporal.TemporalAdjusters.firstDayOfMonth;
import static java.time.temporal.TemporalAdjusters.lastDayOfMonth;

import io.micrometer.core.annotation.Timed;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;

import java.io.IOException;
import java.time.LocalDate;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.validation.constraints.NotNull;

import org.apache.poi.util.IOUtils;
import org.manathome.timeatwork.db.repository.WorkDoneRepository;
import org.manathome.timeatwork.dto.report.MonthlyWorkByDayReport;
import org.manathome.timeatwork.dto.report.MonthlyWorkByProjectReport;
import org.manathome.timeatwork.security.AppUserDetailsService;
import org.manathome.timeatwork.security.UserRoleName;
import org.manathome.timeatwork.service.PdfGeneratorService;
import org.manathome.timeatwork.util.Logger;
import org.manathome.timeatwork.util.TimeAtWorkInternalException;
import org.manathome.timeatwork.util.assertions.Require;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;



/** http api for monthly reporting. */
@RestController
@CrossOrigin
@SecurityRequirement(name = ResourceHelper.AUTHENTICATION_HEADER)
@Tag(name = OpenapiTags.TAG_UI_USER)
@Secured({UserRoleName.Secured.ROLE_USER})
@Timed
public class MonthlyReportResource {
  
  
  private static final Logger logger = Logger.getLogger(MonthlyReportResource.class);

  
  public static final String PATH_REPORT_MONTHLY = ResourceHelper.PATH_BASE_REST 
      + "report/monthly/";
  public static final String FROM_ISO_DATE_STRING = "from-iso-date-string";
  
  @Inject
  private WorkDoneRepository workDoneRepository;

  @Inject
  private AppUserDetailsService userService;
  
  @Inject
  private PdfGeneratorService pdfGeneratorService;
 
  /** rest. */
  @Operation(summary = "monthly reporting grouped by day and task, date format: " 
                     + PATH_REPORT_MONTHLY + "YYYY-MM-DD.")
  @GetMapping(path = PATH_REPORT_MONTHLY + "{" + FROM_ISO_DATE_STRING + "}/byDay", 
              produces = ResourceHelper.CONTENT_TYPE_JSON)
  @Transactional
  public MonthlyWorkByDayReport reportMonthlyByDay(
      @Parameter(description = "date, start of month, format YYYY-MM-DD", example = "2019-03-01") 
      @NotNull 
      @PathVariable(name = FROM_ISO_DATE_STRING) 
      final String fromIsoDateString) {
    
    logger.trace(PATH_REPORT_MONTHLY + "/" + fromIsoDateString + "/byDay");
    
    final var from = LocalDate.parse(Require.notNull(fromIsoDateString, "from"))
                              .with(firstDayOfMonth());
    final var until = from.with(lastDayOfMonth());

    final var user = userService.getCurrentTeamMember();
    
    final var workDone = workDoneRepository
        .findWorkDoneByMember(user, from, until)
        .collect(Collectors.toList());

    logger.trace("found work: " + workDone.size());
    final var mwr = MonthlyWorkByDayReport.from(workDone);
    return mwr;
  }
  

  private MonthlyWorkByProjectReport loadReportData(final LocalDate from) {
    final var until = from.with(lastDayOfMonth());

    final var user = userService.getCurrentTeamMember();
    
    final var workDone = workDoneRepository
        .findWorkDoneByMember(user, from, until)
        .collect(Collectors.toList());

    logger.trace("found work: " + workDone.size());
    final var mwr = MonthlyWorkByProjectReport.from(workDone);
    return mwr;
  }  
  
  /** rest data for report. */
  @Operation(summary = "monthly reporting grouped by project, task, date format: " 
                     + PATH_REPORT_MONTHLY + "YYYY-MM-DD.")
  @GetMapping(path = PATH_REPORT_MONTHLY + "{" + FROM_ISO_DATE_STRING + "}/byProject", 
              produces = ResourceHelper.CONTENT_TYPE_JSON)
  @Transactional
  public MonthlyWorkByProjectReport reportMonthlyByProject(
      @Parameter(description = "date, start of month, format YYYY-MM-DD", example = "2019-03-01") 
      @NotNull 
      @PathVariable(name = FROM_ISO_DATE_STRING) 
      final String fromIsoDateString) {
    
    logger.trace(PATH_REPORT_MONTHLY + "/" + fromIsoDateString + "/byProject");
    
    final var from = LocalDate.parse(Require.notNull(fromIsoDateString, "from"))
                              .with(firstDayOfMonth());
    final var mwr = loadReportData(from);
    return mwr;
  }



  /** pdf report. */
  @Operation(summary = "monthly reporting grouped by project, task, date format: " 
                     + PATH_REPORT_MONTHLY + "YYYY-MM-DD as pdf.")
  @GetMapping(path = PATH_REPORT_MONTHLY + "{" + FROM_ISO_DATE_STRING + "}/byProject/pdf", 
              produces = ResourceHelper.CONTENT_TYPE_PDF)
  @Transactional
  public void reportMonthlyByProjectAsPdfDownload(
      javax.servlet.http.HttpServletResponse response,
      
      @Parameter(description = "date, start of month, format YYYY-MM-DD", example = "2019-03-01") 
      @NotNull 
      @PathVariable(name = FROM_ISO_DATE_STRING) 
      final String fromIsoDateString) {
    
    logger.trace(PATH_REPORT_MONTHLY + "/" + fromIsoDateString + "/byProject");
    
    final var from = LocalDate.parse(Require.notNull(fromIsoDateString, "from"))
                              .with(firstDayOfMonth());
    final var mwr = loadReportData(from);
    
    try (final var pdfStream = pdfGeneratorService.generateProjectPdfFile(mwr)) {

      response.setContentType(ResourceHelper.CONTENT_TYPE_PDF);
      response.setHeader("Content-Disposition", 
                         "attachment; filename=MonthlyReportProjectByTask-" 
                        + from.toString() + ".pdf");

      IOUtils.copy(pdfStream, response.getOutputStream());
    } catch (final IOException ioe) {
      throw new TimeAtWorkInternalException("error generationg pdf " + ioe.getMessage(), ioe);
    }
    logger.trace("returning pdf..");
  }

}
