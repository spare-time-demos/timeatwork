package org.manathome.timeatwork.rest;

@SuppressWarnings("PMD.ClassNamingConventions")
public final class OpenapiTags {
  
  private OpenapiTags() {
    
  }
  
  /** default openapi schema url base path. */
  public static final String PATH_API = "/v3/";

  public static final String TAG_UI_USER = "ui-user";

  public static final String TAG_UI_ADMIN = "ui-admin";

  public static final String TAG_ADMIN = "admin";

  public static final String TAG_SECURITY = "security";
}
