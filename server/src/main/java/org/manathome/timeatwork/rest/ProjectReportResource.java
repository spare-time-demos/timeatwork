package org.manathome.timeatwork.rest;

import static java.time.temporal.TemporalAdjusters.firstDayOfMonth;
import static java.time.temporal.TemporalAdjusters.lastDayOfMonth;

import io.micrometer.core.annotation.Timed;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;

import java.time.LocalDate;
import java.util.List;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.validation.constraints.NotNull;

import org.apache.poi.util.IOUtils;
import org.manathome.timeatwork.db.repository.ProjectReportQuery;
import org.manathome.timeatwork.db.repository.ProjectRepository;
import org.manathome.timeatwork.db.repository.result.ProjectTaskResult;
import org.manathome.timeatwork.domain.Project;
import org.manathome.timeatwork.security.AppUserDetailsService;
import org.manathome.timeatwork.security.UserRoleName;
import org.manathome.timeatwork.service.ExcelGeneratorService;
import org.manathome.timeatwork.util.InvalidDataGivenException;
import org.manathome.timeatwork.util.Logger;
import org.manathome.timeatwork.util.NotFoundException;
import org.manathome.timeatwork.util.assertions.Require;

import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

/** http api for project reporting. */
@RestController
@CrossOrigin
@SecurityRequirement(name = ResourceHelper.AUTHENTICATION_HEADER)
@Tag(name = OpenapiTags.TAG_UI_ADMIN)
@Timed
@Secured({ UserRoleName.Secured.ROLE_PROJECTLEAD, UserRoleName.Secured.ROLE_ADMIN })
public class ProjectReportResource {

  private static final Logger logger = Logger.getLogger(ProjectReportResource.class);

  public static final String PATH_REPORT_PROJECT = ResourceHelper.PATH_BASE_REST 
                                                 + "report/project/";
  public static final String FROM_ISO_DATE_STRING = "from-iso-date-string";
  public static final String PROJECT_ID = "projectId";

  @Inject
  private ProjectReportQuery projectReportQuery;

  @Inject
  private AppUserDetailsService userService;

  @Inject
  private ProjectRepository projectRepository;

  @Inject
  private ExcelGeneratorService excelGeneratorService;

  /** rest. */
  @Operation(summary = "monthly project reporting grouped task, date format: " 
                      + PATH_REPORT_PROJECT + "YYYY-MM-DD.")
  @GetMapping(path = PATH_REPORT_PROJECT + "{" + PROJECT_ID + "}/" 
                  + "{" + FROM_ISO_DATE_STRING + "}/"
      + "byTask", produces = ResourceHelper.CONTENT_TYPE_JSON)
  @Transactional
  public List<ProjectTaskResult> reportProjectByTask(
      @Parameter(description = "project id", example = "1") 
      @NotNull 
      @PathVariable(name = PROJECT_ID) 
      final Long projectId,

      @Parameter(description = "date, start of month, format YYYY-MM-DD", example = "2020-02-01") 
      @NotNull 
      @PathVariable(name = FROM_ISO_DATE_STRING) 
      final String fromIsoDateString) {

    logger.trace(PATH_REPORT_PROJECT + projectId + "/" + fromIsoDateString + "/byTask");

    final var projectTasksRows = readProject(projectId, fromIsoDateString);
    return projectTasksRows;
  }

  private List<ProjectTaskResult> readProject(
      final Long projectId, 
      final String fromIsoDateString) {
    
    final var from = LocalDate.parse(Require.notNull(fromIsoDateString, "from"))
        .with(firstDayOfMonth());
    final var until = from.with(lastDayOfMonth());

    final var currentUser = userService.getCurrentTeamMember();

    final Project project = projectRepository.findById(projectId)
        .orElseThrow(() -> new NotFoundException("project", "" + projectId));

    if (!currentUser.hasAdminRole() && !project.hasLead(currentUser)) {
      throw new InvalidDataGivenException(
          "project " + projectId 
          + " not accessible for " + currentUser.getUserId());
    }

    final var projectTasksRows = projectReportQuery.readProjectByTask(project.getId(), from, until);

    logger.trace("found tasks: " + projectTasksRows.size());
    return projectTasksRows;
  }

  /** rest. */
  @Operation(summary = "monthly project reporting grouped task, date format: " + PATH_REPORT_PROJECT
      + "YYYY-MM-DD. as excel download")
  @GetMapping(path = PATH_REPORT_PROJECT 
      + "{" + PROJECT_ID + "}/" 
      + "{" + FROM_ISO_DATE_STRING + "}/"
      + "byTask/excel", produces = ResourceHelper.CONTENT_TYPE_EXCEL)
  public void reportProjectByTaskDownload(
      
      javax.servlet.http.HttpServletResponse response,
      
      @Parameter(description = "project id", example = "1") 
      @NotNull 
      @PathVariable(name = PROJECT_ID) 
      final Long projectId,

      @Parameter(description = "date, start of month, format YYYY-MM-DD", example = "2020-02-01") 
      @NotNull 
      @PathVariable(name = FROM_ISO_DATE_STRING)      
      final String fromIsoDateString) throws java.io.IOException {

    logger.trace(PATH_REPORT_PROJECT + projectId + "/" + fromIsoDateString 
                                     + "/byTask/excel EXCEL DOWNLOAD");

    final var projectTasksRows = readProject(projectId, fromIsoDateString);
    
    try (var excelStream = excelGeneratorService.generateProjectExcelFile(projectTasksRows)) {

      response.setContentType(ResourceHelper.CONTENT_TYPE_EXCEL);
      response.setHeader("Content-Disposition", "attachment; filename=ProjectByTask.xlsx");

      IOUtils.copy(excelStream, response.getOutputStream());
    }
    logger.trace("returning excel..");

  }

}
