package org.manathome.timeatwork.rest;

import io.swagger.v3.oas.annotations.ExternalDocumentation;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeIn;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.info.License;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import io.swagger.v3.oas.annotations.tags.Tag;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.validation.constraints.NotNull;

import org.manathome.timeatwork.db.repository.ProjectRepository;
import org.manathome.timeatwork.db.repository.TaskRepository;
import org.manathome.timeatwork.domain.Project;
import org.manathome.timeatwork.domain.Task;
import org.manathome.timeatwork.domain.TaskState;
import org.manathome.timeatwork.domain.values.Name;
import org.manathome.timeatwork.dto.CProject;
import org.manathome.timeatwork.dto.CTask;
import org.manathome.timeatwork.security.AppUserDetailsService;
import org.manathome.timeatwork.security.UserRoleName;
import org.manathome.timeatwork.service.LocalizedValidation;
import org.manathome.timeatwork.util.Logger;
import org.manathome.timeatwork.util.NotFoundException;
import org.manathome.timeatwork.util.assertions.Require;
import org.manathome.timeatwork.util.generated.LocalizedMessages;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

/** http api for project administration. */
@OpenAPIDefinition(info = @Info(title = "time@work rest api", version = "0.1", 
                   description = "internal rest api, used by time@work angular client", 
                   license = @License(name = "Apache 2.0", url = "https://www.apache.org/licenses/LICENSE-2.0.txt"), 
                   contact = @Contact(url = "https://gitlab.com/spare-time-demos/timeatwork", 
                                      name = "man@home", 
                                      email = "man.at.home@do-not-use-this-mail.com")), tags = {
    @Tag(name = OpenapiTags.TAG_UI_ADMIN, description = "ui: admin and team lead api calls"),
    @Tag(name = OpenapiTags.TAG_UI_USER,  description = "ui: user api calls"),
    @Tag(name = OpenapiTags.TAG_SECURITY, description = "login"),
    @Tag(name = OpenapiTags.TAG_ADMIN,    description = "admin api, internal use") }, 
         externalDocs = @ExternalDocumentation(url = "https://gitlab.com/spare-time-demos/timeatwork/tree/master/docs/src/main/docs/features", 
         description = "specs"))
@SecurityScheme(
    name = ResourceHelper.AUTHENTICATION_HEADER, 
    type = SecuritySchemeType.APIKEY, 
    in = SecuritySchemeIn.HEADER, 
    description = "auth with jwt bearer token in '" 
                + ResourceHelper.AUTHENTICATION_HEADER 
                + "' header of proctected requests.", 
    paramName = ResourceHelper.AUTHENTICATION_HEADER)

@RestController
@CrossOrigin
@Secured({UserRoleName.Secured.ROLE_PROJECTLEAD, UserRoleName.Secured.ROLE_ADMIN})
@SecurityRequirement(name = ResourceHelper.AUTHENTICATION_HEADER)
@Tag(name = OpenapiTags.TAG_UI_ADMIN)
@SuppressWarnings("PMD:ExcessiveImports")
public class ProjectResource {

  private static final Logger logger = Logger.getLogger(ProjectResource.class);

  public static final String PATH_PROJECT =   ResourceHelper.PATH_BASE_REST + "project/";

  public static final String PARAM_PROJECT_ID = "{projectId}";

  @Inject
  private ProjectRepository projectRepository;

  @Inject
  private TaskRepository taskRepository;
  
  @Inject
  private AppUserDetailsService userService;
  
  @Inject
  private LocalizedValidation validation;

  /** rest. */
  @Operation(summary = "retrieving all visible projects for current user.")
  @GetMapping(path = PATH_PROJECT, produces = ResourceHelper.CONTENT_TYPE_JSON)
  public @NotNull List<CProject> getProjects() {
    
    final var currentUser = userService.getCurrentTeamMember();

    logger.trace(PATH_PROJECT);
    return StreamSupport.stream(projectRepository.findAll().spliterator(), false)
        .sorted(Comparator.comparingLong(Project::getId))
        .filter(p -> currentUser.hasAdminRole() || p.hasLead(currentUser))
        .map(CProject::new)
        .collect(Collectors.toList());
  }

  /** rest. */
  @Operation(summary = "retrieving a specific visible projects for current user.")
  @GetMapping(path = PATH_PROJECT + PARAM_PROJECT_ID, produces = ResourceHelper.CONTENT_TYPE_JSON)
  public CProject getProject(
      @NotNull 
      @Parameter(description = "unique project id to find", required = true, example = "0185") 
      @PathVariable final Long projectId) {

    logger.trace(PATH_PROJECT + projectId);
    
    final var currentUser = userService.getCurrentTeamMember();
    
    return projectRepository
        .findById(Require.notNull(projectId, PARAM_PROJECT_ID))
        .filter(p -> currentUser.hasAdminRole() || p.hasLead(currentUser))
        .map(CProject::new)
        .orElse(null);
  }

  /** rest. */
  @Operation(summary = "change an existing project.")
  @PutMapping(path = PATH_PROJECT + PARAM_PROJECT_ID, 
      consumes = ResourceHelper.CONTENT_TYPE_JSON, 
      produces = ResourceHelper.CONTENT_TYPE_JSON)
  @Transactional 
  public @NotNull CProject changeProject(
      @NotNull 
      @RequestBody 
      CProject changedProject,
      
      @NotNull 
      @Parameter(description = "unique project id to change", example = "0815") 
      @PathVariable 
      Long projectId) {

    logger.trace(PATH_PROJECT + projectId + " for " + changedProject);

    Require.notNull(changedProject, "changedProject");
    Require.notNull(projectId, PARAM_PROJECT_ID);
    Require.isTrue(projectId.equals(changedProject.getId()), "projectId are equal");
    
    validation.validateNotNullOrEmptyWhitespace(changedProject.getName(),
                                                LocalizedMessages.project_name_required);
    
    final var currentUser = userService.getCurrentTeamMember();
    
    return projectRepository.findById(projectId).map(project -> {
      
      logger.debug("change " + project + " to " + changedProject);

      validation.validateIsTrue(currentUser.hasAdminRole() || project.hasLead(currentUser), 
          "project.lead.not.you");

      project.setName(Name.of(changedProject.getName()));
      project.setActive(changedProject.isActive());

      // remove old leads
      project.getProjectLeads()
             .stream()
             .filter(pl -> Arrays.stream(changedProject.getProjectLeads())
                             .filter(Objects::nonNull)
                             .allMatch(cpl -> !cpl.getId().equals(pl.getId())))
          .collect(Collectors.toList())
          .stream().forEach(pl -> {
            logger.trace("remove lead" + pl + " from project " + project);
            project.removeProjectLead(pl);
          });

      // add new leads
      Arrays.stream(changedProject.getProjectLeads())
          .filter(plNew -> plNew != null
              && project.getProjectLeads().stream()
              .allMatch(plOld -> !plNew.getId().equals(plOld.getId())))
          .collect(Collectors.toList()).stream().forEach(plNew -> {
            logger.trace("add lead" + plNew + " from project " + project);
            project.addProjectLead(plNew.asTeamMember());
          });

      validation.validateIsTrue(currentUser.hasAdminRole() || project.hasLead(currentUser), 
          "project.lead.not.remove.yourself");
      
      return new CProject(projectRepository.save(project));
      
    })
    .orElseGet(() -> new CProject(projectRepository.save(changedProject.asProject())));
  }

  /** rest. */
  @Operation(summary = "create a new project.", tags = { "ui-admin" })
  @PostMapping(path = PATH_PROJECT, 
               consumes = ResourceHelper.CONTENT_TYPE_JSON, 
               produces = ResourceHelper.CONTENT_TYPE_JSON)
  @ResponseStatus(HttpStatus.CREATED)
  public @NotNull CProject createProject(final @NotNull @RequestBody CProject newProject) {

    logger.trace(PATH_PROJECT + " (post) for " + newProject);
    
    Require.notNull(newProject, "newProject");
    Require.isTrue(newProject.getId() == null || newProject.getId().equals(Long.valueOf(0)), 
                   "id should be null");

    validation.validateNotNullOrEmptyWhitespace(newProject.getName(), 
                                                LocalizedMessages.project_name_required);

    final var currentUser = userService.getCurrentTeamMember();
    final var project = newProject.asProject();

    validation.validateIsTrue(currentUser.hasAdminRole() || project.hasLead(currentUser), 
        LocalizedMessages.project_lead_not_you);
    
    return new CProject(Require.notNull(projectRepository.save(project)));
  }

  /** rest. */
  @Operation(summary = "retrieving tasks for a specific project.")
  @GetMapping(path = PATH_PROJECT + PARAM_PROJECT_ID + "/task", 
              produces = ResourceHelper.CONTENT_TYPE_JSON)
  @Transactional
  public @NotNull List<CTask> getProjectTasks(
      @Parameter(description = "project id to find tasks for", example = "0815") 
      @PathVariable 
      final Long projectId) {

    logger.trace(PATH_PROJECT + projectId + "/task");

    Require.notNull(projectId, PARAM_PROJECT_ID);
    
    final var currentUser = userService.getCurrentTeamMember();
    final Project project = projectRepository
        .findById(projectId)
        .orElseThrow(() -> new NotFoundException("project", "" + projectId));
    
    validation.validateIsTrue(currentUser.hasAdminRole() || project.hasLead(currentUser), 
        "project.lead.not.you");

    try (var tasks = taskRepository.findByProjectIdOrderById(project.getId())) {
      return tasks
          .map(CTask::new)
          .collect(Collectors.toList());
    }
  }

  /** rest. */
  @Operation(summary = "create a new task for an existing project")
  @PostMapping(path = PATH_PROJECT + PARAM_PROJECT_ID
      + "/task", consumes = ResourceHelper.CONTENT_TYPE_JSON, 
      produces = ResourceHelper.CONTENT_TYPE_JSON)
  @ResponseStatus(HttpStatus.CREATED)  
  @ApiResponse(responseCode = "201", description = "task created")
  @ApiResponse(responseCode = "403", description = "not authorized as project lead or admin") 
  @ApiResponse(responseCode = "404", description = "project not found", 
               content = @Content(schema = @Schema(implementation = ResponseStatusException.class)))
  @Transactional
  public @NotNull CTask createTaskForProject(
      @NotNull
      @Parameter(description = "new task data")
      @RequestBody 
      final CTask newTask,
      
      @NotNull 
      @Parameter(description = "project id for task to change", example = "0815") 
      @PathVariable 
      final Long projectId) {

    Require.notNull(newTask, "newTask");
    Require.isTrue(newTask.getId() == null, "id should be null for new task.");
    Require.notNullOrZero(projectId, PARAM_PROJECT_ID);

    validation.validateNotNullOrEmptyWhitespace(newTask.getName(), 
                                                LocalizedMessages.task_name_required);

    final var currentUser = userService.getCurrentTeamMember();
    final var project = projectRepository
        .findById(projectId)
        .orElseThrow(() -> new NotFoundException("project", Objects.toString(projectId)));

    validation.validateIsTrue(currentUser.hasAdminRole() || project.hasLead(currentUser), 
        "project.lead.not.you");    
    
    final var taskToStore = new Task(project, Name.of(newTask.getName()));

    taskToStore.setState(TaskState.ACTIVE);

    return new CTask(taskRepository.save(taskToStore));
  }

}
