package org.manathome.timeatwork.rest;

/** some constants. */
public final class ResourceHelper {
  
  public static final String PATH_BASE_REST = "/rest/";

  public static final String CONTENT_TYPE_JSON = "application/json";

  public static final String CONTENT_TYPE_PDF = "application/pdf";
  
  public static final String CONTENT_TYPE_EXCEL = 
                      "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
  
  public static final String AUTHENTICATION_HEADER = "Authentication";

  private ResourceHelper() {
    
  }
  
}
