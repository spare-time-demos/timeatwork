package org.manathome.timeatwork.rest;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;

import java.util.List;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.validation.constraints.NotNull;

import org.manathome.timeatwork.db.repository.TaskAssignmentRepository;
import org.manathome.timeatwork.db.repository.TaskRepository;
import org.manathome.timeatwork.db.repository.TeamMemberRepository;
import org.manathome.timeatwork.domain.Task;
import org.manathome.timeatwork.domain.TaskAssignment;
import org.manathome.timeatwork.domain.TeamMember;
import org.manathome.timeatwork.domain.values.Name;
import org.manathome.timeatwork.dto.CTask;
import org.manathome.timeatwork.dto.CTaskAssignment;
import org.manathome.timeatwork.security.AppUserDetailsService;
import org.manathome.timeatwork.security.UserRoleName;
import org.manathome.timeatwork.util.Logger;
import org.manathome.timeatwork.util.NotFoundException;
import org.manathome.timeatwork.util.assertions.Require;

import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

/** http api for task and task assignment administration. */
@RestController
@CrossOrigin
@SecurityRequirement(name = ResourceHelper.AUTHENTICATION_HEADER)
@Tag(name = OpenapiTags.TAG_UI_ADMIN)
public class TaskResource {

  private static final Logger logger = Logger.getLogger(TaskResource.class);

  public static final String PATH_TASK = ResourceHelper.PATH_BASE_REST + "task/"; // NOSONAR
  
  public static final String PARAM_TASK_ID = "{taskId}";

  @Inject
  private TaskRepository taskRepository;

  @Inject
  private TaskAssignmentRepository taskAssignmentRepository;

  @Inject
  private TeamMemberRepository teamMemberRepository;
    
  @Inject
  private AppUserDetailsService userService;  
  
  
  /** rest. */
  @Operation(summary = "retrieving a specific task.")
  @GetMapping(path = PATH_TASK + PARAM_TASK_ID)
  @Secured(UserRoleName.Secured.ROLE_USER)
  public CTask getTask(@PathVariable final Long taskId) {

    logger.trace(PATH_TASK + taskId);
    
    final var currentUser = userService.getCurrentTeamMember();
    
    return taskRepository
        .findById(Require.notNull(taskId, PARAM_TASK_ID))
        .filter(t -> currentUser.hasAdminRole() || t.getProject().hasLead(currentUser))
        .map(CTask::new)
        .orElse(null);
  }

  /** rest. */
  @Operation(summary = "change an existing task.")
  @PutMapping(path = PATH_TASK
      + PARAM_TASK_ID, 
      consumes = ResourceHelper.CONTENT_TYPE_JSON, 
      produces = ResourceHelper.CONTENT_TYPE_JSON)
  @Transactional
  @Secured({UserRoleName.Secured.ROLE_PROJECTLEAD, UserRoleName.Secured.ROLE_ADMIN})
  public Task changeTask(@RequestBody CTask changedTask, @PathVariable Long taskId) {
    logger.trace(PATH_TASK + "{" + taskId + "} (put) for " + changedTask);

    final var currentUser = userService.getCurrentTeamMember();
    final Task oldTask = taskRepository
        .findById(taskId)
        .orElseThrow(() -> new NotFoundException(Task.TASK_NAME, String.valueOf(taskId)));

    Require.isTrue(currentUser.hasAdminRole() || oldTask.getProject().hasLead(currentUser), 
        "task accessible not for user " + currentUser.getUserId());
    
    oldTask.setName(Name.of(changedTask.getName()));

    return taskRepository.save(oldTask);
  }

  /** rest. */
  @Operation(summary = "delete an existing task.")
  @DeleteMapping(path = PATH_TASK + PARAM_TASK_ID, produces = ResourceHelper.CONTENT_TYPE_JSON)
  @Transactional
  @Secured({UserRoleName.Secured.ROLE_PROJECTLEAD, UserRoleName.Secured.ROLE_ADMIN})
  public Long deleteTask(@PathVariable Long taskId) {
    logger.trace(PATH_TASK + "{" + taskId + "} (delete)");

    final var task = taskRepository.findById(taskId)
        .orElseThrow(() -> new NotFoundException(Task.TASK_NAME, String.valueOf(taskId)));

    final var currentUser = userService.getCurrentTeamMember();
    Require.isTrue(currentUser.hasAdminRole() || task.getProject().hasLead(currentUser), 
                   "task accessible not for user " + currentUser.getUserId());
    
    final var assignmentCount = taskAssignmentRepository
        .findByTaskIdOrderById(task.getId()).count();
    
    if (assignmentCount > 0) {
      throw new ResponseStatusException(HttpStatus.CONFLICT,
              Task.TASK_NAME + taskId + " has still " 
              + assignmentCount + " assignments and can not deleted.");
    }

    taskRepository.deleteById(task.getId());
    return taskId;
  }

  /** rest. */
  @Operation(summary = "retrieving team member assignments for a specific task.")
  @GetMapping(path = PATH_TASK + PARAM_TASK_ID + "/assignment", 
              produces = ResourceHelper.CONTENT_TYPE_JSON)
  @Transactional
  @Secured({UserRoleName.Secured.ROLE_USER})
  public List<CTaskAssignment> getTaskAssignments(@PathVariable final Long taskId) {

    logger.trace(PATH_TASK + "{" + taskId + "}/assignment");

    final var currentUser = userService.getCurrentTeamMember();
    
    try (var assignments = taskAssignmentRepository
        .findByTaskIdOrderById(Require.notNull(taskId, PARAM_TASK_ID))) {
      return assignments
          .filter(a -> currentUser.hasAdminRole() 
                    || a.getTask().getProject().hasLead(currentUser))
          .map(CTaskAssignment::new)
          .collect(Collectors.toList());
    }
  }

  /** rest. */
  @Operation(summary = "add a new assignment to an existing task.")
  @PostMapping(path = PATH_TASK + PARAM_TASK_ID
      + "/assignment", 
      consumes = ResourceHelper.CONTENT_TYPE_JSON, 
      produces = ResourceHelper.CONTENT_TYPE_JSON)
  @ResponseStatus(HttpStatus.CREATED)
  @Transactional
  @Secured({UserRoleName.Secured.ROLE_PROJECTLEAD, UserRoleName.Secured.ROLE_ADMIN})
  public CTaskAssignment createAssignment(
      @RequestBody CTaskAssignment newAssignment, 
      @PathVariable Long taskId) {

    logger.trace(PATH_TASK + "{" + taskId + "}/assignment (post) for " + newAssignment);

    Require.notNull(newAssignment, "assignment");
    Require.notNull(newAssignment.getTeamMember(), "teamMember");
    Require.notNullOrZero(taskId, PARAM_TASK_ID);
    Require.notNullOrZero(newAssignment.getTeamMember().getId());

    final TeamMember member = teamMemberRepository.findById(newAssignment.getTeamMember().getId())
        .orElseThrow(() -> new NotFoundException(
                               "member", 
                               String.valueOf(newAssignment.getTeamMember().getId())));
    
    final Task task = taskRepository.findById(taskId)
        .orElseThrow(() -> new NotFoundException(Task.TASK_NAME, String.valueOf(taskId)));

    final var currentUser = userService.getCurrentTeamMember();
    
    Require.isTrue(currentUser.hasAdminRole() || task.getProject().hasLead(currentUser), 
        "assignment accessible not for user " + currentUser.getUserId());
    
    final var assignment = new TaskAssignment(member, task, newAssignment.getFrom());
    return new CTaskAssignment(taskAssignmentRepository.save(assignment));
  }

  /** rest. */
  @Operation(summary = "change from/until of given assignment")
  @PutMapping(path = PATH_TASK + PARAM_TASK_ID
      + "/assignment/{assignmentId}", 
      consumes = ResourceHelper.CONTENT_TYPE_JSON, 
      produces = ResourceHelper.CONTENT_TYPE_JSON)
  @Transactional
  @Secured({UserRoleName.Secured.ROLE_PROJECTLEAD, UserRoleName.Secured.ROLE_ADMIN})
  public CTaskAssignment changeAssignment(
      @NotNull @RequestBody CTaskAssignment changedAssignment,
      @NotNull @PathVariable Long taskId, 
      @NotNull @PathVariable Long assignmentId) {

    logger.trace(PATH_TASK + taskId + "/assignment/" + assignmentId + " (put)");

    Require.notNull(changedAssignment, "changedAssignment");
    Require.notNullOrZero(assignmentId);
    Require.notNullOrZero(taskId, PARAM_TASK_ID);
    Require.sameId(assignmentId, changedAssignment.getId(), "assignment id");

    final var oldAssignment = taskAssignmentRepository
        .findById(assignmentId)
        .orElseThrow(() ->  new NotFoundException("assignment", String.valueOf(assignmentId)));

    Require.sameId(taskId, oldAssignment.getTask().getId(), "taskId");

    final var currentUser = userService.getCurrentTeamMember();
    Require.isTrue(currentUser.hasAdminRole() 
                || oldAssignment.getTask().getProject().hasLead(currentUser), 
        "assignment accessible not for user " + currentUser.getUserId());
    
    
    oldAssignment.setAssignmentRange(changedAssignment.getFrom(), changedAssignment.getUntil());
    return new CTaskAssignment(taskAssignmentRepository.save(oldAssignment));
  }

  /** rest. */
  @Operation(summary = "delete an existing assignment from " + Task.TASK_NAME)
  @DeleteMapping(path = PATH_TASK + PARAM_TASK_ID + "/assignment/{assignmentId}")
  @Transactional
  @Secured({UserRoleName.Secured.ROLE_PROJECTLEAD, UserRoleName.Secured.ROLE_ADMIN})
  public Long deleteAssignment(@PathVariable Long taskId, @PathVariable Long assignmentId) {
    logger.trace(PATH_TASK + "{" + taskId + "}/assignment/" + assignmentId + "(delete)");

    Require.notNullOrZero(taskId, PARAM_TASK_ID);
    Require.notNullOrZero(assignmentId, "assignmentId");

    final var task = taskRepository.findById(taskId)
        .orElseThrow(() -> new NotFoundException(Task.TASK_NAME, String.valueOf(taskId)));
    
    final var currentUser = userService.getCurrentTeamMember();
    Require.isTrue(currentUser.hasAdminRole() 
        || task.getProject().hasLead(currentUser), 
        "task not accessible for user " + currentUser.getUserId());    

    final var assignment = taskAssignmentRepository.findById(assignmentId).orElseThrow(
        () -> new NotFoundException("assignment", String.valueOf(assignmentId)));

    if (assignment.getTask().getId().longValue() != task.getId().longValue()) {
      throw new ResponseStatusException(HttpStatus.CONFLICT,
          Task.TASK_NAME + " " + taskId + " has no " + assignmentId + " assignment to delete");
    }

    taskAssignmentRepository.delete(assignment);
    return assignmentId;
  }

}
