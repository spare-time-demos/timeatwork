package org.manathome.timeatwork.rest;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.inject.Inject;

import org.manathome.timeatwork.db.repository.TeamMemberRepository;
import org.manathome.timeatwork.dto.CTeamMember;
import org.manathome.timeatwork.security.UserRoleName;
import org.manathome.timeatwork.util.Logger;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/** http api for team member access. */
@RestController
@CrossOrigin
@SecurityRequirement(name = ResourceHelper.AUTHENTICATION_HEADER)
@Secured({UserRoleName.Secured.ROLE_ADMIN, UserRoleName.Secured.ROLE_PROJECTLEAD})
public class TeamMemberResource {

  private static final Logger logger = Logger.getLogger(TeamMemberResource.class);

  public static final String PATH_TEAMMEMBER = ResourceHelper.PATH_BASE_REST + "teammember";
  public static final String PATH_TEAMLEAD =   ResourceHelper.PATH_BASE_REST + "teamlead";

  @Inject
  private TeamMemberRepository teamMemberRepository;

  /** rest. */
  @Operation(summary = "retrieving all known users.")
  @GetMapping(path = PATH_TEAMMEMBER, produces = ResourceHelper.CONTENT_TYPE_JSON)
  @Tag(name = OpenapiTags.TAG_UI_USER)
  @Tag(name = OpenapiTags.TAG_UI_ADMIN)
  public List<CTeamMember> getTeamMembers() {

    logger.trace(PATH_TEAMMEMBER);
    
    return StreamSupport
        .stream(teamMemberRepository.findAll().spliterator(), false)
        .sorted((t1, t2) -> t1.getName().toString().compareTo(t2.getName().toString()))
        .map(CTeamMember::new)
        .collect(Collectors.toList());
  }
  
  /** rest. */
  @Operation(summary = "retrieving all known team leads.")
  @GetMapping(path = PATH_TEAMLEAD, produces = ResourceHelper.CONTENT_TYPE_JSON)
  @Tag(name = OpenapiTags.TAG_UI_USER)
  @Tag(name = OpenapiTags.TAG_UI_ADMIN)
  public List<CTeamMember> getTeamLeads() {

    logger.trace(PATH_TEAMLEAD);
    
    return StreamSupport
        .stream(teamMemberRepository.findAll().spliterator(), false)
        .filter(t -> t.hasLeadRole() && t.isActive())
        .sorted((t1, t2) -> t1.getName().toString().compareTo(t2.getName().toString()))
        .map(CTeamMember::new)
        .collect(Collectors.toList());
  }  

}
