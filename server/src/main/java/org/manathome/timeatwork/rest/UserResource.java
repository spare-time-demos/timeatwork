package org.manathome.timeatwork.rest;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.validation.constraints.NotNull;

import org.manathome.timeatwork.db.repository.TeamMemberRepository;
import org.manathome.timeatwork.domain.values.Name;
import org.manathome.timeatwork.dto.CPasswordChangeResult;
import org.manathome.timeatwork.dto.CUser;
import org.manathome.timeatwork.security.AppUserDetailsService;
import org.manathome.timeatwork.security.UserRoleName;
import org.manathome.timeatwork.service.AuditService;
import org.manathome.timeatwork.service.LocalizedValidation;
import org.manathome.timeatwork.util.Logger;
import org.manathome.timeatwork.util.NotFoundException;
import org.manathome.timeatwork.util.assertions.Require;
import org.manathome.timeatwork.util.generated.LocalizedMessages;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/** http api for team member access. */
@RestController
@CrossOrigin
@SecurityRequirement(name = ResourceHelper.AUTHENTICATION_HEADER)
@Secured({UserRoleName.Secured.ROLE_ADMIN})
public class UserResource {

  private static final Logger logger = Logger.getLogger(UserResource.class);

  public static final String PATH_USER =   AdminResource.PATH_ADMIN + "user";
  
  public static final String PARAM_USER_ID = "{id}";

  @Inject
  private TeamMemberRepository teamMemberRepository;
  
  @Inject
  private AuditService auditService;
  
  @Inject
  private AppUserDetailsService appUserService;
  
  @Inject
  private LocalizedValidation validation;

  /** rest. */
  @Operation(summary = "retrieving all known users for administration.")
  @GetMapping(path = PATH_USER, produces = ResourceHelper.CONTENT_TYPE_JSON)
  @Tag(name = OpenapiTags.TAG_UI_ADMIN)
  public List<CUser> getUsers() {

    logger.trace(PATH_USER);
    
    return StreamSupport
        .stream(teamMemberRepository.findAll().spliterator(), false)
        .sorted((t1, t2) -> t1.getName().toString().compareTo(t2.getName().toString()))
        .map(CUser::new)
        .collect(Collectors.toList());
  }
  
  /** rest. */
  @Operation(summary = "retrieving a known user for administration.")
  @GetMapping(path = PATH_USER + "/" + PARAM_USER_ID, produces = ResourceHelper.CONTENT_TYPE_JSON)
  @Tag(name = OpenapiTags.TAG_UI_ADMIN)
  public CUser getUser(
      @NotNull 
      @Parameter(description = "unique user id", example = "0815") 
      @PathVariable 
      Long id) {

    logger.trace(PATH_USER + "/" + id);
    
    final var tm = teamMemberRepository
        .findById(Require.notNullOrZero(id, "id"))
        .orElseThrow(() -> new NotFoundException("user", id.toString()));
    
    return new CUser(tm);
  }
    
  
  /** rest. */
  @Operation(summary = "create a new user.", tags = { "ui-admin" })
  @PostMapping(path = PATH_USER, 
               consumes = ResourceHelper.CONTENT_TYPE_JSON, 
               produces = ResourceHelper.CONTENT_TYPE_JSON)
  @ResponseStatus(HttpStatus.CREATED)
  @CacheEvict(value = AppUserDetailsService.TEAM_MEMBER_CACHE, allEntries = true)
  public @NotNull CUser createUser(final @NotNull @RequestBody CUser newUser) {

    logger.trace(PATH_USER + " (post) for " + newUser);

    Require.notNull(newUser, "newUser");
    Require.isTrue(newUser.getId() == null || newUser.getId().equals(Long.valueOf(0)), 
        "id should be null");
    
    validation.validateNotNullOrEmptyWhitespace(newUser.getName(), 
                                                LocalizedMessages.user_name_required);
    validation.validateNotNullOrEmptyWhitespace(newUser.getUserId(), 
                                                LocalizedMessages.user_id_required);
    
    auditService.logUserEvent("new user " + newUser.getUserId());
    
    final var createdUser = appUserService.saveUser(newUser.asTeamMember());
    
    return new CUser(createdUser);
  }
  
  /** rest. */
  @Operation(summary = "change an existing existing user.")
  @PutMapping(path = PATH_USER + "/" + PARAM_USER_ID, 
      consumes = ResourceHelper.CONTENT_TYPE_JSON, 
      produces = ResourceHelper.CONTENT_TYPE_JSON)
  @Transactional 
  public @NotNull CUser changeUser(
      @NotNull 
      @RequestBody 
      CUser changedUser,
      
      @NotNull 
      @Parameter(description = "unique user id to change", example = "0815") 
      @PathVariable 
      Long id) {

    logger.trace(PATH_USER + "/" + id + " for " + changedUser);

    Require.notNull(changedUser, "changedUser");
    Require.notNull(id, PARAM_USER_ID);
    Require.isTrue(id.equals(changedUser.getId()), "id are equal");

    validation.validateNotNullOrEmptyWhitespace(changedUser.getName(),  
                                                LocalizedMessages.user_name_required);
    validation.validateNotNullOrEmptyWhitespace(changedUser.getUserId(), 
                                                LocalizedMessages.user_id_required);

    return teamMemberRepository.findById(id).map(tm -> {
      
      logger.debug("change " + tm + " to " + changedUser);
      
      tm.setName(Name.of(changedUser.getName()));
      tm.setUserId(Name.of(changedUser.getUserId()));
      
      tm.setAdminRole(changedUser.isAdmin());
      tm.setLeadRole(changedUser.isLead());
      
      tm.setLocked(changedUser.isLocked());
      tm.setActive(changedUser.isActive());
    
      auditService.logUserEvent("changed user " + changedUser.getUserId());
      return new CUser(teamMemberRepository.save(tm));
      
    })
    .orElseGet(() -> new CUser(appUserService.saveUser(changedUser.asTeamMember())));
  }

  /** rest. */
  @Operation(summary = "change password for an existing user.")
  @PutMapping(path = PATH_USER + "/" + PARAM_USER_ID + "/password", 
      consumes = ResourceHelper.CONTENT_TYPE_JSON, 
      produces = ResourceHelper.CONTENT_TYPE_JSON)
  @Transactional 
  public CPasswordChangeResult resetPassword(
      @NotNull 
      @Parameter(description = "unique user id to change", example = "0815") 
      @PathVariable 
      Long id) {

    logger.trace(PATH_USER + "/" + id + "/password");

    Require.notNull(id, PARAM_USER_ID);

    final var user = teamMemberRepository.findById(id).orElseThrow();
        
    logger.debug("reset pw of" + user);
      
    // todo .. make this better..
    final String plainPassword = "my-new-" + (int) (Math.random() * 1000);
    
    final String encryptedPassword = appUserService.encryptPassword(plainPassword);
            
    user.setLocked(false);
    user.setPassword(encryptedPassword);
    
    appUserService.saveUser(user);
      
    auditService.logUserEvent("password reset for user " + user);
    
    return new CPasswordChangeResult(true, "new password is: " + plainPassword);
  }
 
}
