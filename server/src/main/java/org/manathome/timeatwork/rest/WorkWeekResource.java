package org.manathome.timeatwork.rest;

import io.micrometer.core.annotation.Timed;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;

import java.time.LocalDate;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.validation.constraints.NotNull;

import org.manathome.timeatwork.domain.WorkWeek;
import org.manathome.timeatwork.security.UserRoleName;
import org.manathome.timeatwork.service.WorkWeekService;
import org.manathome.timeatwork.util.JsonHelper;
import org.manathome.timeatwork.util.Logger;
import org.manathome.timeatwork.util.assertions.Require;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/** http api for work week entry. */
@RestController
@CrossOrigin
@SecurityRequirement(name = ResourceHelper.AUTHENTICATION_HEADER)
@Tag(name = OpenapiTags.TAG_UI_USER)
@Secured({UserRoleName.Secured.ROLE_USER})
@Timed
public class WorkWeekResource {

  private static final Logger logger = Logger.getLogger(WorkWeekResource.class);

  public static final String PATH_WORKWEEK = ResourceHelper.PATH_BASE_REST + "workweek/";
  public static final String FROM_ISO_DATE_STRING = "from-iso-date-string";

  @Inject
  private WorkWeekService workWeekService;
  
  /** rest. */
  @Operation(summary = "retrieving work week for given week, date format: " 
                     + PATH_WORKWEEK + "YYYY-MM-DD.")
  @GetMapping(path = PATH_WORKWEEK + "{" + FROM_ISO_DATE_STRING + "}", 
              produces = ResourceHelper.CONTENT_TYPE_JSON)
  @Transactional
  public WorkWeek getWorkWeek(
      @Parameter(description = "date, start of week, format YYYY-MM-DD", example = "2019-03-11") 
      @NotNull 
      @PathVariable(name = FROM_ISO_DATE_STRING) 
      final String fromIsoDateString) {

    logger.trace(PATH_WORKWEEK + fromIsoDateString);

    var from = LocalDate.parse(Require.notNull(fromIsoDateString, "from"));

    var workWeek = workWeekService.getWorkWeek(from);
    return workWeek;
  }

  /** rest. */
  @Operation(summary = "update work done for given week")
  @PutMapping(path = PATH_WORKWEEK +  "{" + FROM_ISO_DATE_STRING + "}", 
      consumes = ResourceHelper.CONTENT_TYPE_JSON, 
      produces = ResourceHelper.CONTENT_TYPE_JSON)
  @Transactional
  public Integer updateWorkWeek(
      @Parameter(description = "date, start of week, format YYYY-MM-DD", example = "2019-03-11") 
      @NotNull 
      @PathVariable(name = FROM_ISO_DATE_STRING) 
      final String fromIsoDateString,
      @NotNull 
      @RequestBody 
      final WorkWeek workWeek) {

    logger.trace(PATH_WORKWEEK + fromIsoDateString + " (put) of " 
                + JsonHelper.asJsonString(workWeek));
    
    Require.notNullOrEmptyWhitespace(fromIsoDateString, "fromIsoDateSTring");
    Require.notNull(workWeek, "workWeek");
    Require.notNull(workWeek.getFrom(), "workWeek.from");
    
    Require.isTrue(LocalDate.parse(fromIsoDateString).isEqual(workWeek.getFrom()),
        "from " + fromIsoDateString + " and workWeek.from not equal");

    this.workWeekService.updateWorkWeek(workWeek);

    return Integer.valueOf(workWeek.sumWorkHours().asHours());
  }

}
