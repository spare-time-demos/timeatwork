package org.manathome.timeatwork.security;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.manathome.timeatwork.domain.TeamMember;
import org.manathome.timeatwork.util.assertions.Require;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

/** spring security: user. */
public class AppUser implements UserDetails {

  private static final long serialVersionUID = 1L;
  
  private final TeamMember member;
  
  public AppUser(final TeamMember member) {
    this.member = Require.notNull(member, "member");
  }

  @Override
  public Collection<? extends GrantedAuthority> getAuthorities() {    
    
    final List<GrantedAuthority> roles = new ArrayList<>();

    if (member.hasAdminRole()) {
      roles.add(new SimpleGrantedAuthority(UserRoleName.Secured.ROLE_ADMIN));      
    }
    if (member.hasLeadRole()) {
      roles.add(new SimpleGrantedAuthority(UserRoleName.Secured.ROLE_PROJECTLEAD));      
    }
    roles.add(new SimpleGrantedAuthority(UserRoleName.Secured.ROLE_USER));
    
    return roles;
  }

  @Override
  public String getPassword() {
    return member.getPassword();
  }
  
  public void changePassword(final String newEncodedPassword) {
    member.setPassword(newEncodedPassword);
  }  

  @Override
  public String getUsername() {
    return member.getUserId().toString();
  }

  @Override
  public boolean isAccountNonExpired() {
    return true;
  }

  @Override
  public boolean isAccountNonLocked() {
    return !member.isLocked();
  }

  @Override
  public boolean isCredentialsNonExpired() {
    return true;
  }

  @Override
  public boolean isEnabled() {
    return member.isActive();
  }
  
  
  public TeamMember asTeamMember() {
    return this.member;
  }

}
