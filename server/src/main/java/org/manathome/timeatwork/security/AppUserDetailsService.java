package org.manathome.timeatwork.security;

import io.micrometer.core.instrument.Counter;
import io.micrometer.core.instrument.MeterRegistry;

import javax.validation.constraints.NotNull;

import org.manathome.timeatwork.db.repository.TeamMemberRepository;
import org.manathome.timeatwork.domain.TeamMember;
import org.manathome.timeatwork.domain.values.Name;
import org.manathome.timeatwork.util.Logger;
import org.manathome.timeatwork.util.MetricName;
import org.manathome.timeatwork.util.assertions.Require;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;


/** spring security user retrieval. */
@Service
public class AppUserDetailsService implements UserDetailsService {
  
  /** see entry in ehcache.xml configuration. */
  public static final String TEAM_MEMBER_CACHE = "teamMemberCache";

  private static final Logger logger = Logger.getLogger(AppUserDetailsService.class);

  @Autowired
  private TeamMemberRepository repository;
  
  @Autowired
  private AppUserDetailsService selfReference;
  
  private final PasswordEncoder encoder = PasswordEncoderFactories
      .createDelegatingPasswordEncoder();
  
  private final Counter userLoadCounter;

  private final Counter userPasswordCheckedOkCounter;

  private final Counter userPasswordCheckedInvalidCounter;
  
  /** .ctor */
  public AppUserDetailsService(MeterRegistry registry) {
    
    userLoadCounter = registry.counter(
        MetricName.user_load_count, 
        MetricName.MetricCommonLabel.result, "ok");
    
    userPasswordCheckedOkCounter = registry.counter(
        MetricName.user_password_checked, 
        MetricName.MetricCommonLabel.result, "ok");     
    
    userPasswordCheckedInvalidCounter = registry.counter(
        MetricName.user_password_checked, 
        MetricName.MetricCommonLabel.result, "invalid");     
  }
  
  /** peek into load counter metric. */
  public double getUserLoadCount() {
    return userLoadCounter.count();
  }


  @Override
  public UserDetails loadUserByUsername(final String userId) throws UsernameNotFoundException {

    final var tm = selfReference.findByUserIdCached(userId);
    return new AppUser(tm);
  }
  
  /** cached db retrieval of team member. */
  @Cacheable(TEAM_MEMBER_CACHE)
  public TeamMember findByUserIdCached(final String userId) {

    
    final TeamMember tm = repository
        .findByUserId(Name.of(Require.notNullOrEmptyWhitespace(userId, "userId")))
        .orElseThrow(() -> new UsernameNotFoundException("team member " + userId + " not found."));

    userLoadCounter.increment();
    logger.trace("findByUserIdCached(\"" + userId + "\") is not cached, read from database " 
                 + tm.getId());

    return tm;
  }
  
  /** 
   * current team member logged in. 
   * 
   * @return logged in user
   * @exception CurrentUserNotLoggedInException no user found.
   */  
  public @NotNull TeamMember getCurrentTeamMember() {
    
    Authentication auth = SecurityContextHolder.getContext().getAuthentication();    
    if (auth != null && !(auth instanceof AnonymousAuthenticationToken)) {
      final var userDetails = auth.getPrincipal();
      if (userDetails instanceof UserDetails) {
        
        return selfReference.findByUserIdCached(
            ((UserDetails) userDetails).getUsername());
        
      } else if (userDetails != null) {
        
        final String userName = userDetails.toString();
        logger.debug("current user may be " + userName);
        return repository.findByUserId(Name.of(userName))
            .orElseThrow(() -> new CurrentUserNotLoggedInException(
                "unexpected not found a user " + userName));        
      } else {
        throw new CurrentUserNotLoggedInException("no current user found.");
      }
    }
    
    throw new CurrentUserNotLoggedInException("no current user in session.");
  }  
  
  /** encrypt a new password. */
  public String encryptPassword(final String plainPassword) {

    Require.notNullOrEmptyWhitespace(plainPassword,"non empty password required");
    Require.isTrue(plainPassword.length() >= 4, "minimal password length not met");

    return encoder.encode(plainPassword);
  }
  
  /** validate a password. */
  @CacheEvict(value = AppUserDetailsService.TEAM_MEMBER_CACHE, 
              key = "#user.username", 
              beforeInvocation = true)
  public boolean validatePassword(
      final UserDetails user, 
      final String plainPassword) {

    Require.notNull(user, "user");
    Require.notNull(plainPassword, "pw");
    
    final String encodedPwToMatch = Require.notNullOrEmptyWhitespace(
        user.getPassword(), 
        "known password must be set");
    
    final var ok = encoder.matches(
        plainPassword, 
        encodedPwToMatch);
    
    if (ok)  { 
      userPasswordCheckedOkCounter.increment(); 
    }
    if (!ok) { 
      userPasswordCheckedInvalidCounter.increment(); 
    }
    
    return ok;
  }  

  /** save changes on user. */
  @CacheEvict(value = AppUserDetailsService.TEAM_MEMBER_CACHE, key = "#user.userIdAsString")
  public TeamMember saveUser(final TeamMember user) {

    logger.debug("saveUser(" + user + ")");
    
    return repository.save(Require.notNull(user, "user to save"));
  }

  
  
  /** run on startup, setting passwords for specific (uninitialized) _test_ users. */
  @EventListener
  @CacheEvict(value = AppUserDetailsService.TEAM_MEMBER_CACHE, allEntries = true)
  public void onApplicationEvent(ContextRefreshedEvent event) {
    
    logger.info("updating unitialized test user passwords..");
    
    repository.findByUserId(Name.of(AppUserNames.admin)).ifPresent(tm -> {
      
      if (tm.getPassword().equalsIgnoreCase(AppUserNames.uninitializedTestPasswordMarker)) {
        tm.setPassword(encryptPassword(AppUserNames.testPassword));
        logger.debug("updating " + tm.getUserId() + " user password");
        repository.save(tm);        
      }
    });
    
    repository.findByUserId(Name.of(AppUserNames.user)).ifPresent(tm -> {
                
      if (tm.getPassword().equalsIgnoreCase(AppUserNames.uninitializedTestPasswordMarker)) {
        tm.setPassword(encryptPassword(AppUserNames.testPassword));
        logger.debug("updating " + tm.getUserId() + " user password");
        repository.save(tm);        
      }
    });

    repository.findByUserId(Name.of(AppUserNames.tlead1)).ifPresent(tm -> {
      
      if (tm.getPassword().equalsIgnoreCase(AppUserNames.uninitializedTestPasswordMarker)) {
        tm.setPassword(encryptPassword(AppUserNames.testPassword));
        logger.debug("updating " + tm.getUserId() + " user password");
        repository.save(tm);        
      }
    });

    logger.debug("updating unitialized test user passwords done.");
  }

}
