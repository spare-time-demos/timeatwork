package org.manathome.timeatwork.security;

/** special user names used within tests etc. */
public final class AppUserNames {
  
  public static final String admin = "admin";
  public static final String tlead1  = "tlead1";
  public static final String user = "user";
  
  public static final String testPassword = "password";
  public static final String uninitializedTestPasswordMarker = "TEST-NOTYET-SET";

}
