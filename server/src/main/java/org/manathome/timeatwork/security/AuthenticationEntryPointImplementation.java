package org.manathome.timeatwork.security;

import java.io.IOException;
import java.net.URLEncoder;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.manathome.timeatwork.rest.LoginResource;
import org.manathome.timeatwork.util.Logger;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

/**
 * handle unauthenticated calls. custom logic.
 */
@Component
public class AuthenticationEntryPointImplementation implements AuthenticationEntryPoint {

  private static final Logger logger = 
      Logger.getLogger(AuthenticationEntryPointImplementation.class);

  public static final String HINT_TEXT = "this request needs an authenticated user (login)";

  @Override
  public void commence(final HttpServletRequest request, final HttpServletResponse response,
      final AuthenticationException authException) throws IOException, ServletException {

    logger.trace("unauthorized rest call request to " 
        + request.getRequestURI() + ", " 
        + request.getPathInfo());

    // a rest call is not authenticated, return suitable http result code,
    // angular client should handle this.
    response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
    
    // response body should help browser ui users.
    try (var writer = response.getWriter()) {
      writer.println("HTTP Status - " + HttpServletResponse.SC_UNAUTHORIZED + " \n\n");
      writer.println(HINT_TEXT);
      writer.println("\n");
      writer.println("\nuse...........: " + LoginResource.URL_AUTHENTICATION + " for rest");
      writer.println("\nuse...........: /login for html ui. \n");
      writer.println("\nprotected url.: " + URLEncoder.encode(request.getRequestURI(), "UTF-8"));
      writer.println("\n");
    }
  }
}
