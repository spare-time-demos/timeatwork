package org.manathome.timeatwork.security;

import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;

/** login call. 
 * @category dto. 
 */
@Schema(description = "user provied login data")  
public class AuthenticationRequest implements Serializable {
  
  private static final long serialVersionUID = -3595490471821537471L;
  
  private String userId;
  private String password;

  public AuthenticationRequest() {
  }

  public AuthenticationRequest(String userId, String password) {
    this.userId = userId;
    this.setPassword(password);
  }

  @Schema(description = "user id during as given in login.")  
  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }


  @Schema(description = "password (during login only).")  
  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  @Override
  public String toString() {
    return userId;
  }

}
