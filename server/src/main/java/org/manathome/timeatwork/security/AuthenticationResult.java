package org.manathome.timeatwork.security;

import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;


/** authentication result. 
 * @category dto.
 * */
@Schema(description = "result of login attempt.")  
public class AuthenticationResult implements Serializable {
  
  private static final long serialVersionUID = 1583016722278780207L;
  
  @Schema(description = "true if login was successful.")  
  public final boolean ok;
  
  @Schema(description = "a human readable message describing failed attempts.")  
  public final String message;
  
  @Schema(description = "on success full attempt: a valid session token for future rest calls.")  
  public final String token;
  
  @Schema(description = "user id of currently logged in user.")
  public final String userId;

  @Schema(description = "may work as project lead.")  
  public boolean hasRoleProjectLead;
  
  @Schema(description = "admin account.")  
  public boolean hasRoleAdmin;
  
  /** .ctor. */
  public AuthenticationResult(
      boolean ok, 
      String message, 
      String token,
      String userId,
      boolean hasRoleProjectLead,
      boolean hasRoleAdmin) {
    
    this.ok = ok;
    this.message = message;
    this.token = token;
    this.userId = userId;
    this.hasRoleProjectLead = hasRoleProjectLead;
    this.hasRoleAdmin = hasRoleAdmin;
  }

  @Override
  public String toString() {
    return ok + " " + message;
  }

}
