package org.manathome.timeatwork.security;

import org.manathome.timeatwork.util.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

/** user not logged in, cannot get current team member. */
public class CurrentUserNotLoggedInException extends ResponseStatusException {

  private static final long serialVersionUID = 1L;
  
  private static final Logger logger = Logger.getLogger(CurrentUserNotLoggedInException.class);

  public CurrentUserNotLoggedInException(final String msg) {
    super(HttpStatus.UNAUTHORIZED, msg);
    logger.debug("ctor currentUserNotLoggedInException: " + msg);
  }
  
}
