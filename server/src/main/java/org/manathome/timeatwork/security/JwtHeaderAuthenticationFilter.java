package org.manathome.timeatwork.security;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.manathome.timeatwork.util.assertions.Require;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.OncePerRequestFilter;

/** accept a jwt token as valid credentials. */
public class JwtHeaderAuthenticationFilter extends OncePerRequestFilter {
  
  private final JwtTokenProcessor jwtTokenProcessor;
  
  public JwtHeaderAuthenticationFilter(JwtTokenProcessor jwtTokenProcessor) {
    this.jwtTokenProcessor = Require.notNull(jwtTokenProcessor, "JwtTokenProcessor");
  }

  @Override
  protected void doFilterInternal(
      HttpServletRequest request, 
      HttpServletResponse response, 
      FilterChain filterChain)
      throws IOException, ServletException {

    var authentication = getAuthentication(request);
    
    if (authentication != null) {
      // authenticated with a jwt token extracted within this filter.
      SecurityContextHolder.getContext().setAuthentication(authentication); 
    }
    
    filterChain.doFilter(request, response);      
  }

  /** extract jwt token from http header, verify token and accept valid tokens as authentication. 
   * @return authenticated user or null.
   * */
  private UsernamePasswordAuthenticationToken getAuthentication(HttpServletRequest request) {
    
    var headerValue = request.getHeader(JwtTokenProcessor.TOKEN_HEADER_AUTHORIZATION);
    
    
    if (StringUtils.hasText(headerValue) 
        && headerValue.startsWith(JwtTokenProcessor.TOKEN_PREFIX_AUTHORIZATION)) {

      var rawJwtToken = headerValue.replace(JwtTokenProcessor.TOKEN_PREFIX_AUTHORIZATION, "");
      
      return jwtTokenProcessor.getUserFromToken(rawJwtToken);
    }

    return null;
  }
}
