package org.manathome.timeatwork.security;

import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.UnsupportedJwtException;
import io.jsonwebtoken.security.Keys;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import org.manathome.timeatwork.util.assertions.Require;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

/** jwt token handling. */
@Component
public class JwtTokenProcessor {
  
  private static final Logger logger = LoggerFactory.getLogger(JwtTokenProcessor.class);

  @Value("${timeatwork.security.jwt.token.secret}")
  private String jwtTokenSecretKey; 

  public static final String TOKEN_HEADER_AUTHORIZATION = "Authorization";

  public static final String TOKEN_PREFIX_AUTHORIZATION = "Bearer "; 

  /** build jwt token. */
  public String buildTokenForUser(final UserDetails user) {

    logger.trace("authentication will return jwt token for " + user);
    
    final var signingKey = Require.notNullOrEmptyWhitespace(jwtTokenSecretKey).getBytes();

    final var roles = user.getAuthorities()
        .stream()
        .map(GrantedAuthority::getAuthority)
        .collect(Collectors.toList());

    final var token = Jwts.builder()
        .signWith(Keys.hmacShaKeyFor(signingKey),SignatureAlgorithm.HS512)
        .setHeaderParam("typ", "JWT")
        .setIssuer("time@work-server")
        .setAudience("time@work-clients")
        .setSubject(user.getUsername())
        .setExpiration(new Date(System.currentTimeMillis() + 864000000))
        .claim("rol", roles)
        .compact();
    
    return token;
  }
  
  /** validate jwt token and extract user information from it. */
  public UsernamePasswordAuthenticationToken getUserFromToken(final String rawJwtToken) {
    try {
    
      final var signingKey = Require.notNullOrEmptyWhitespace(jwtTokenSecretKey).getBytes();

      final var parsedToken = Jwts.parserBuilder()
          .setSigningKey(signingKey)
          .build()
          .parseClaimsJws(
              rawJwtToken.replace(JwtTokenProcessor.TOKEN_PREFIX_AUTHORIZATION, ""));
      
      final var username = parsedToken.getBody().getSubject();

      @SuppressWarnings("unchecked")
      final var authorities = ((List<String>) parsedToken.getBody()
          .get("rol")).stream()
          .map(authority -> new SimpleGrantedAuthority((String) authority))
          .collect(Collectors.toList());

      if (StringUtils.hasText(username)) {
        logger.trace("accept jwt header on call for " + username);
        return new UsernamePasswordAuthenticationToken(username, null, authorities);
      }
      
    } catch (ExpiredJwtException exception) {
      logger.warn("Request to parse expired JWT : {} failed : {}", 
          rawJwtToken, exception.getMessage());
    } catch (UnsupportedJwtException exception) {
      logger.warn("Request to parse unsupported JWT : {} failed : {}", 
          rawJwtToken, exception.getMessage());
    } catch (MalformedJwtException exception) {
      logger.warn("Request to parse invalid JWT : {} failed : {}", 
          rawJwtToken, exception.getMessage());
    } catch (IllegalArgumentException exception) {
      logger.warn("Request to parse empty or null JWT : {} failed : {}", 
          rawJwtToken, exception.getMessage());
    }

    return null;    
  }
  
}
