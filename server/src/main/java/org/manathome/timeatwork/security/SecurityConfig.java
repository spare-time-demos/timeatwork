package org.manathome.timeatwork.security;

import org.manathome.timeatwork.rest.AdminResource;
import org.manathome.timeatwork.rest.LoginResource;
import org.manathome.timeatwork.rest.OpenapiTags;
import org.manathome.timeatwork.rest.ResourceHelper;
import org.manathome.timeatwork.ui.HomeController;
import org.manathome.timeatwork.ui.TestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.security.web.util.matcher.OrRequestMatcher;
import org.springframework.security.web.util.matcher.RequestMatcher;

/** spring security configuration. */
@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {
  
  @Autowired
  private AuthenticationEntryPointImplementation authenticationEntryPoint;
  
  @Autowired
  private AppUserDetailsService userDetailsService;
  
  @Autowired
  private JwtTokenProcessor jwtTokenProcessor;
  
  private static final RequestMatcher PROTECTED_REST_URLS = new OrRequestMatcher(
        new AntPathRequestMatcher(ResourceHelper.PATH_BASE_REST + "**"),
        new AntPathRequestMatcher(TestController.URL_TEST_PROTECTED)
      );
  
  /** url protection. */
  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http
        .cors()
        .and()
        .csrf()
          .disable()                // needed for rest calls, no csrf tokens.
        .authorizeRequests()
        // ------------------------------------------------------------
        // OPEN ENDPOINTS FOR EVERYONE.
        // ------------------------------------------------------------
        .antMatchers(HomeController.URL_BASE, 
                     TestController.URL_TEST, 
                     LoginResource.URL_AUTHENTICATION,
                     OpenapiTags.PATH_API)
          .permitAll()                    // base page, test and login url are free for all..  
        // ------------------------------------------------------------
        // ADMIN PAGES
        // ------------------------------------------------------------
        .antMatchers(AdminResource.PATH_ADMIN + "**")
          .hasRole(UserRoleName.ADMIN)  
        // ------------------------------------------------------------
        // AUTHENTICATED UI + REST SERVICES NEED AT LEAST USER ROLE
        // ------------------------------------------------------------
        .requestMatchers(PROTECTED_REST_URLS)
          .hasRole(UserRoleName.USER)
        .and()
          .formLogin()          // keep form based login for special pages..
        .and()
          .exceptionHandling()  // needed to get 401 response code for rest calls.
          .defaultAuthenticationEntryPointFor(authenticationEntryPoint, PROTECTED_REST_URLS) 
        .and()
          .addFilterBefore(new JwtHeaderAuthenticationFilter(jwtTokenProcessor), 
                           UsernamePasswordAuthenticationFilter.class)
      ;  
  }

  /** user configuration. */
  @Autowired
  public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
    
    PasswordEncoder encoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();

    auth.userDetailsService(userDetailsService)
        .passwordEncoder(encoder);
    
      
    /* 
    // use in memory test code alternative..
    auth
      .inMemoryAuthentication()
        .withUser(AppUserNames.admin)
          .password(encoder.encode(AppUserNames.testPassword))
          .roles(UserRoleName.USER, UserRoleName.ADMIN)
        .and()
        .withUser(AppUserNames.user)
          .password(encoder.encode(AppUserNames.testPassword))
          .roles(UserRoleName.USER)
        .and()
        .withUser(AppUserNames.tlead1)
          .password(encoder.encode(AppUserNames.testPassword))
          .roles(UserRoleName.USER, UserRoleName.PROJECTLEAD);
    */
             
  }
  
  
  @Bean
  @Override
  public AuthenticationManager authenticationManagerBean() throws Exception {
    return super.authenticationManagerBean();
  }
  
}
