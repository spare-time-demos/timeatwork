package org.manathome.timeatwork.security;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.manathome.timeatwork.util.HashUtil;
import org.manathome.timeatwork.util.Logger;
import org.manathome.timeatwork.util.LoggerField;
import org.slf4j.MDC;

/** add session hash to log. */
public class SessionLoggingFilter implements Filter {

  private static final Logger logger = Logger.getLogger(SessionLoggingFilter.class);

  @Override
  public void init(final FilterConfig filterConfig) throws ServletException {
    logger.debug("init session logging filter: key " + LoggerField.SESSION);
  }

  @Override
  public void doFilter(
      final ServletRequest request, final ServletResponse response, final FilterChain chain)
      throws IOException, ServletException {

    if (request instanceof HttpServletRequest) {
      var session = ((HttpServletRequest) request).getSession(false);
      if (session != null) {
        MDC.put(LoggerField.SESSION, HashUtil.hashMd5(session.getId()));
      }
    }

    try {
      chain.doFilter(request, response);
    } finally {
      MDC.remove(LoggerField.SESSION);
    }
  }
}
