package org.manathome.timeatwork.security;

import java.io.IOException;
import java.util.Objects;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.manathome.timeatwork.util.Logger;
import org.manathome.timeatwork.util.LoggerField;
import org.slf4j.MDC;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

/** add user info into logging output. */
@Component
public class UserLoggingFilter implements Filter {

  private static final Logger logger = Logger.getLogger(UserLoggingFilter.class);

  @Override
  public void init(final FilterConfig filterConfig) throws ServletException {
    logger.debug("init user logging filter, key=" + LoggerField.USER);
  }

  @Override
  public void doFilter(
      final ServletRequest request, final ServletResponse response, final FilterChain chain)
      throws IOException, ServletException {

    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    if (authentication != null) {
      MDC.put(LoggerField.USER, Objects.toString(authentication.getPrincipal()));
    } else {
      MDC.put(LoggerField.USER, "?");
    }

    try {
      chain.doFilter(request, response);
    } finally {
      MDC.remove(LoggerField.USER);
    }
  }

}
