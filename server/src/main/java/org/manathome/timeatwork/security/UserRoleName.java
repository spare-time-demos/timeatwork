package org.manathome.timeatwork.security;

import org.manathome.timeatwork.util.StringUtil;

/** known user roles of time@work users. */
public final class UserRoleName {

  public static final String USER = "USER";

  public static final String ADMIN = "ADMIN";

  public static final String PROJECTLEAD = "PROJECTLEAD";

  /** to use as arguments on @SECURED() annotation. */
  public static class Secured {
    private static final String ROLE_PRAEFIX = "ROLE_";
    
    public static final String ROLE_USER = ROLE_PRAEFIX + USER;
    public static final String ROLE_ADMIN = ROLE_PRAEFIX + ADMIN;
    public static final String ROLE_PROJECTLEAD = ROLE_PRAEFIX + PROJECTLEAD;
  }
  
  /** compare to roles, ignore ROLE_ praefix.. */
  public static boolean isSameRole(final String role, final String otherRole) {
    
    if (role == null || otherRole == null) {
      return false;
    }
        
    return StringUtil.removePraefix(role, Secured.ROLE_PRAEFIX)
                     .equalsIgnoreCase(StringUtil.removePraefix(otherRole, Secured.ROLE_PRAEFIX));
  }
}
