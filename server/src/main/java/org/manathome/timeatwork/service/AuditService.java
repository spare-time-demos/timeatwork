package org.manathome.timeatwork.service;

import org.manathome.timeatwork.util.Logger;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

/** Nucleus of audit stream. */
@Service
public class AuditService {
  
  private static final Logger loggerUser = Logger.getLogger("org.manathome.timeatwork.AUDIT.user");
  
  /** log a change on a user. */
  public void logUserEvent(final String what) {
    
    final String userName = SecurityContextHolder.getContext().getAuthentication().getName();
    
    loggerUser.info(userName + ": " + what);
  }

}
