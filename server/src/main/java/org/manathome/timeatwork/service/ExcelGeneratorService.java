package org.manathome.timeatwork.service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.manathome.timeatwork.db.repository.result.ProjectTaskResult;
import org.manathome.timeatwork.util.Logger;
import org.springframework.stereotype.Component;

/** prototype of a excel format generator. */
@Component
public class ExcelGeneratorService {

  private static final Logger logger = Logger.getLogger(ExcelGeneratorService.class);

  /** generate an excel format list. */
  public ByteArrayInputStream generateProjectExcelFile(
      final List<ProjectTaskResult> projectTaskResults) {

    try (Workbook workbook = new XSSFWorkbook()) {
      Sheet sheet = workbook.createSheet("Customers");

      Row row = sheet.createRow(0);
      CellStyle headerCellStyle = workbook.createCellStyle();
      headerCellStyle.setFillForegroundColor(IndexedColors.AQUA.getIndex());
      headerCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
      // Creating header
      Cell cell = row.createCell(0);
      cell.setCellValue("Task id");
      cell.setCellStyle(headerCellStyle);

      cell = row.createCell(1);
      cell.setCellValue("Task");
      cell.setCellStyle(headerCellStyle);

      cell = row.createCell(2);
      cell.setCellValue("Task entries count");
      cell.setCellStyle(headerCellStyle);

      cell = row.createCell(3);
      cell.setCellValue("Task hours sum");
      cell.setCellStyle(headerCellStyle);

      int i = 0;
      for (var ptr : projectTaskResults) {
        Row dataRow = sheet.createRow(++i);

        dataRow.createCell(0).setCellValue(ptr.getTaskId());
        dataRow.createCell(1).setCellValue(ptr.getTaskName());
        dataRow.createCell(2).setCellValue(ptr.getWorkHoursCount());
        dataRow.createCell(3).setCellValue(ptr.getWorkHoursSum());
      }

      // Making size of column auto resize to fit with data
      sheet.autoSizeColumn(0);
      sheet.autoSizeColumn(1);
      sheet.autoSizeColumn(2);
      sheet.autoSizeColumn(3);

      try (var outputStream = new ByteArrayOutputStream()) {
        workbook.write(outputStream);
        return new ByteArrayInputStream(outputStream.toByteArray());
      }

    } catch (final IOException ex) {
      logger.error("excel generate error " + ex.getMessage(), ex);
      return null;
    }
  }

}
