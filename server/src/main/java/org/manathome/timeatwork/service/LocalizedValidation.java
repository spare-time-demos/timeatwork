package org.manathome.timeatwork.service;

import java.util.Locale;

import org.manathome.timeatwork.util.assertions.Require;
import org.manathome.timeatwork.util.assertions.ValidationFailedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;

/** checks with human readable localized messages (business logic). */
@Service
public class LocalizedValidation {
      
  final MessageSource messageSource;  
  
  public LocalizedValidation(@Autowired final MessageSource messageSource) {
    this.messageSource = Require.notNull(messageSource, "messageSource");
  }
  
  public Locale getLocale() {
    return LocaleContextHolder.getLocale();
  }

  /** allow neither null nor blank or empty string on "s". 
   * @param s string to test
   * @param messageKeyOnFail text key value for error message. */
  public String validateNotNullOrEmptyWhitespace(final String s, final String messageKeyOnFail) {
    
    if (s == null || s.trim().length() == 0) {
      
      throw new ValidationFailedException(
          messageSource.getMessage(
              messageKeyOnFail, 
              null, 
              getLocale()
              ));
    }
    
    return s;
  }

  /** simple get a localized message.
   * 
   * @param messageKey found in properties file
   * @return localized message
   * @see MessageSource
   */
  public String getMessage(final String messageKey) {
    return messageSource.getMessage(
        messageKey, 
        null, 
        getLocale());
  }

  /** simple get a localized message.
   * 
   * @param messageKey found in properties file
   * @param messageArgs array with message parts to include in message (patterned {0}..)
   * @return localized message
   * @see MessageSource
   */
  public String getMessage(final String messageKey, Object[] messageArgs) {
    return messageSource.getMessage(
        messageKey, 
        messageArgs, 
        getLocale());
  }

  /** check condition to be true.
   * @param conditionToUphold condition
   * @param messageKeyOnFail text key value for error message. */  
  public void validateIsTrue(boolean conditionToUphold, final String messageKeyOnFail) {
    
    if (!conditionToUphold) {
      throw new ValidationFailedException(
          messageSource.getMessage(
              messageKeyOnFail, 
              null, 
              getLocale()
              ));  
    }
  }

}
