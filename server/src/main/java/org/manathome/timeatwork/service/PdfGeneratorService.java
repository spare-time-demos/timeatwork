package org.manathome.timeatwork.service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDType1Font;

import org.manathome.timeatwork.dto.report.MonthlyWorkByProjectReport;
import org.manathome.timeatwork.util.Logger;

import org.springframework.stereotype.Component;

/** prototype of a pdf format generator. 
 * HINT: apache pdfbox has only basic pdf primitives, not suitable for easy layouting reports.
 */
@Component
public class PdfGeneratorService {

  private static final Logger logger = Logger.getLogger(PdfGeneratorService.class);

  /** build pdf. */
  public ByteArrayInputStream generateProjectPdfFile(final MonthlyWorkByProjectReport mwr) 
      throws IOException {

    try (final PDDocument doc = new PDDocument()) {

      PDPage myPage = new PDPage();
      doc.addPage(myPage);

      try (final PDPageContentStream cont = new PDPageContentStream(doc, myPage)) {

        cont.beginText();

        cont.setFont(PDType1Font.TIMES_ROMAN, 12);
        cont.setLeading(14.5f);

        cont.newLineAtOffset(25, 700);
        
        // TODO: put data and layout into pdf.
        
        for (final var pwr : mwr.getProjectWorkReports()) {

          String line = pwr.getProjectName(); 
          cont.showText(line);
          cont.newLine();
          cont.newLine();
          
          for (final var tl : pwr.getTaskWorks()) {
            final String tline = tl.getTaskName() + ": " + tl.getWorkHours() + "h";
            cont.setLeading(20);
            cont.showText(tline);
            cont.newLine();
            
            for (final var dtw : tl.getDailyTaskWorks()) {
              final String dtwline = "      " 
                                   + dtw.getDate().toString() 
                                   + ": " 
                                   + dtw.getWorkHours() + "h";
              cont.showText(dtwline);
              cont.newLine();              
            }
          }
        }

        cont.endText();
      }
            
      logger.debug("pdf built " + doc.getDocumentInformation().getTitle());

      ByteArrayOutputStream out = new ByteArrayOutputStream();
      doc.save(out);
      doc.close();
      ByteArrayInputStream in = new ByteArrayInputStream(out.toByteArray());

      return in;
    }

  }
}
