package org.manathome.timeatwork.service;

import com.google.common.collect.Streams;

import io.micrometer.core.instrument.Metrics;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.validation.constraints.NotNull;

import org.manathome.timeatwork.db.repository.TaskAssignmentRepository;
import org.manathome.timeatwork.db.repository.TaskRepository;
import org.manathome.timeatwork.db.repository.WorkDoneRepository;
import org.manathome.timeatwork.domain.WorkDone;
import org.manathome.timeatwork.domain.WorkWeek;
import org.manathome.timeatwork.domain.values.WorkHours;
import org.manathome.timeatwork.security.AppUserDetailsService;
import org.manathome.timeatwork.util.Logger;
import org.manathome.timeatwork.util.MetricName;
import org.manathome.timeatwork.util.assertions.Require;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class WorkWeekService {

  private static final Logger logger = Logger.getLogger(WorkWeekService.class);

  @Inject
  private TaskAssignmentRepository taskAssignmentRepository;

  @Inject
  private TaskRepository taskRepository;

  @Inject
  private WorkDoneRepository workDoneRepository;

  @Inject
  private AppUserDetailsService userService;
 
  /** read and build work week. */
  public WorkWeek getWorkWeek(@NotNull final LocalDate from) {

    final var until = from.plusDays(6); // 7 days a week

    final var currentUser = userService.getCurrentTeamMember();

    logger.trace("for user " + currentUser.getName());

    final var workDone = workDoneRepository
        .findWorkDoneByMember(currentUser, from, until)
        .collect(Collectors.toList());

    final long sumWorkDone = workDone.stream().mapToLong(wd -> wd.getWorkHours().asMinutes()).sum();
    logger.trace("found: " + workDone.size() + " workdone entries with: " + sumWorkDone + "min");

    final var taskAssignments = taskAssignmentRepository
        .findAssignedTasksForMember(currentUser, from, until)
        .collect(Collectors.toList());

    logger.trace("found: " + taskAssignments.size() + " assignments");

    final var workWeek = new WorkWeek(from, until, taskAssignments, workDone);
    logger.trace("built: " + workWeek);

    Require.isTrue(sumWorkDone == workWeek.sumWorkHours().asMinutes(),
        "Lost Minutes for " + from + ": " + sumWorkDone + " -> " 
        + workWeek.sumWorkHours().asMinutes());

    Metrics.counter(
        MetricName.workWeek, 
        MetricName.MetricCommonLabel.type, 
        "read").increment();
    
    return workWeek;
  }

  /** deconstruct and save work from week. */
  public void updateWorkWeek(@NotNull final WorkWeek workWeek) {

    Require.notNull(workWeek, "workWeek");
    
    final var currentUser = userService.getCurrentTeamMember();
    final var until = workWeek.getFrom().plusDays(6); // 7 days a week

    workWeek.validate();
    
    final var allowedTaskAssignments = taskAssignmentRepository
        .findAssignedTasksForMember(currentUser, workWeek.getFrom(), until)
        .collect(Collectors.toList());

    logger.trace("for user " + currentUser.getName());

    final Map<String, WorkDone> currentWorkDone = workDoneRepository
        .findWorkDoneByMember(currentUser, workWeek.getFrom(), until)
        .collect(Collectors.toMap(WorkDone::getCompareString, workDone -> workDone));

    final var newWorkDone = Arrays.stream(workWeek.getTaskWeeks()).flatMap(tw -> {
      logger.trace("updating new work: " + tw.toDump());
      return Streams.mapWithIndex(Arrays.stream(tw.getWorkHours()),
          (wh, index) -> new WorkDone(currentUser, taskRepository.findById(tw.getTaskId()).get(),
              workWeek.getFrom().plusDays(index), WorkHours.ofHours(wh.getWorkHours())));
    }).collect(Collectors.toMap(WorkDone::getCompareString, workDone -> workDone));

    // delete
    // ------

    final List<WorkDone> removedWork = currentWorkDone.values().stream()
        .filter(cw -> !newWorkDone.containsKey(cw.getCompareString())).collect(Collectors.toList());

    final var deleteCounter = Metrics.counter(
        MetricName.workDone_change, 
        MetricName.MetricCommonLabel.type, 
        "delete");
    final var updateCounter = Metrics.counter(
        MetricName.workDone_change, 
        MetricName.MetricCommonLabel.type, 
        "update");
    final var insertCounter = Metrics.counter(
        MetricName.workDone_change, 
        MetricName.MetricCommonLabel.type, 
        "insert");

    final var workHoursCounter = Metrics.counter(MetricName.workHours_added);

    removedWork.forEach(rw -> {
      logger.debug("removing old work: " + rw.toString());
      workDoneRepository.delete(rw);
      deleteCounter.increment();
    });

    // insert
    // ------

    final List<WorkDone> newWork = newWorkDone.values().stream()
        .filter(nw -> !currentWorkDone.containsKey(nw.getCompareString()))
        .filter(nw -> nw.getWorkHours().asMinutes() != 0).collect(Collectors.toList());

    newWork.stream().sorted(Comparator.comparing(WorkDone::getDate)).forEach(nw -> {
      logger.debug("saving new work: " + nw.toString());
      Require.isTrue(allowedTaskAssignments.stream().anyMatch(ata -> ata.allowesWork(nw)),
          "new work " + nw + " not allowed by current task assignments.");
      Require.isTrue(nw.isValid(), "new work must be valid: " + nw);
      workDoneRepository.save(nw);
      insertCounter.increment();
      workHoursCounter.increment(nw.getWorkHours().asHours());
    });

    // update
    // ------

    final List<WorkDone> changedWork = newWorkDone.values().stream()
        .filter(nw -> currentWorkDone.containsKey(nw.getCompareString()))
        .filter(nw -> currentWorkDone
            .get(nw.getCompareString()).getWorkHours().asMinutes() != nw.getWorkHours().asMinutes())
        .map(nw -> {          
          var toChange = currentWorkDone.get(nw.getCompareString());
          
          if (nw.getWorkHours().asHours() > toChange.getWorkHours().asHours()) {
            workHoursCounter.increment((long) nw.getWorkHours().asHours() 
                                     - (long) toChange.getWorkHours().asHours());            
          }

          toChange.setWorkHours(nw.getWorkHours());
          return toChange;
        }).collect(Collectors.toList());

    changedWork.forEach(cw -> {
      logger.debug("update changed work: " + cw.toString());
      Require.isTrue(allowedTaskAssignments.stream().anyMatch(ata -> ata.allowesWork(cw)),
          "change work + " + cw + " not allowed by current task assignments.");
      Require.isTrue(cw.isValid(), "changed work must be valid: " + cw);
      workDoneRepository.save(cw);
      updateCounter.increment();
    });

  }

}
