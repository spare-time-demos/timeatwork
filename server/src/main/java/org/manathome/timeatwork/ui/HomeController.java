package org.manathome.timeatwork.ui;

import org.manathome.timeatwork.util.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

/** render some server side html pages. */
@Controller
public class HomeController {

  private static final Logger logger = Logger.getLogger(HomeController.class);
  
  public static final String URL_BASE = "/";

  @GetMapping(URL_BASE)
  public String home() {
    logger.info("get page from " + URL_BASE);
    return PageName.home;
  }

}
