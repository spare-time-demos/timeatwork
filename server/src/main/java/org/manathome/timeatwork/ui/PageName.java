package org.manathome.timeatwork.ui;

/** all thymeleaf templates residing in resources/templates/*.html. */
public final class PageName {

  public static final String home = "home";
  public static final String test = "test";
}
