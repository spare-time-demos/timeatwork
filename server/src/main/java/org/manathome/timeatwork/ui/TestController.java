package org.manathome.timeatwork.ui;

import java.security.Principal;

import org.manathome.timeatwork.util.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

/** return some server rendered test html. */
@Controller
public class TestController {

  private static final Logger logger = Logger.getLogger(HomeController.class);

  public static final String URL_TEST = "/test";
  public static final String URL_TEST_PROTECTED = "/test-protected";

  /** unprotected html message. */
  @GetMapping(URL_TEST)
  public String test(final Model model, final Principal principal) {
    logger.info("get unprotected page from " + URL_TEST  
        + " for " + (principal == null ? "no user" : principal.getName()));
    
    model.addAttribute("url", URL_TEST);
    return PageName.test;
  }

  /** protected html message. */
  @GetMapping(URL_TEST_PROTECTED)
  public String testProtected(final Model model, final Principal principal) {
    logger.info("get protected page from: " 
        + URL_TEST_PROTECTED 
        + " for user: " + principal.getName());
    
    model.addAttribute("url", URL_TEST_PROTECTED);
    return PageName.test;
  }
  
}
