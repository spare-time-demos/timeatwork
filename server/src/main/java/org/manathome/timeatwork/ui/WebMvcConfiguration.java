package org.manathome.timeatwork.ui;

import org.manathome.timeatwork.util.Logger;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebMvcConfiguration implements WebMvcConfigurer {

  private static final Logger logger = Logger.getLogger(WebMvcConfiguration.class);

  /**
   * deliver the angular client from the /client/ subfolder.
   * 
   * @implNote angular client files found in resources/static/
   * @implNote angular index.html base href must be set to /client/ also.
   */
  @Override
  public void addResourceHandlers(ResourceHandlerRegistry registry) {

    logger.info("configure resource handlers for static files /static/ (angular client)");

    registry
        .addResourceHandler("/client/**")
        .addResourceLocations("/static/", "classpath:/static/");
  }
}
