package org.manathome.timeatwork.util;

import java.time.LocalDate;

/** future use: fake current date. */
public class DateUtil {

  public static DateUtil instance() {
    return new DateUtil();
  }

  public LocalDate today() {
    return LocalDate.now();
  }
}
