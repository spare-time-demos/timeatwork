package org.manathome.timeatwork.util;

import javax.validation.constraints.NotBlank;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

@Validated
@Configuration
@ConfigurationProperties(prefix = "timeatwork.environment")
public class EnvironmentProperties {

  /**
   * prose readable environment description like: integration test system 1.
   */
  @NotBlank
  private String description = "default";

  /**
   * a short welcome message.
   */
  private String greeting = "welcome to time@work";

  public String getDescription() {
    return description;
  }

  public void setDescription(String environmentDescription) {
    this.description = environmentDescription;
  }

  public String getGreeting() {
    return greeting;
  }

  public void setGreeting(String greeting) {
    this.greeting = greeting;
  }
}
