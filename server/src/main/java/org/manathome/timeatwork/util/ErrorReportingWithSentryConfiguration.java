package org.manathome.timeatwork.util;

import org.springframework.boot.web.servlet.ServletContextInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.HandlerExceptionResolver;

/** activate error reporting to sentry.io, see also sentry.properties. 
 * @see <a href="https://docs.sentry.io/clients/java/integrations/#spring">sentrio.io with spring</a>
 */
@Configuration
public class ErrorReportingWithSentryConfiguration {


  private static final Logger logger = 
      Logger.getLogger(ErrorReportingWithSentryConfiguration.class);
  
  /** add error reporting. */
  @Bean
  public HandlerExceptionResolver sentryExceptionResolver() {
    final var ser =  new io.sentry.spring.SentryExceptionResolver();
    logger.info("adding sentry.io error reporting / " + ser.toString());
    return ser;
  }

  /** get http context info into error reporting. */
  @Bean
  public ServletContextInitializer sentryServletContextInitializer() {
    final var ssci = new io.sentry.spring.SentryServletContextInitializer();
    logger.info("add http context to sentry.io error reporting / " + ssci.toString());
    return ssci;
  }

}
