package org.manathome.timeatwork.util;

import org.springframework.util.DigestUtils;

public class HashUtil {

  /** md5 hash (do not use for cryptography..). */
  public static String hashMd5(String s) {
    if (s == null) {
      return null;
    }
        
    try {
      return DigestUtils.md5DigestAsHex(s.getBytes("UTF-8"));      
    } catch (Exception ex) {
      throw new TimeAtWorkInternalException("could not build digest", ex);
    }
  }
}
