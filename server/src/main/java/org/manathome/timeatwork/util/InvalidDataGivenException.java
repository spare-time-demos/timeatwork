package org.manathome.timeatwork.util;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class InvalidDataGivenException extends ResponseStatusException {

  private static final long serialVersionUID = 1L;

  public InvalidDataGivenException(final String msg) {
    super(HttpStatus.BAD_REQUEST, msg);
  }
  
}
