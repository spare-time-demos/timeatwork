package org.manathome.timeatwork.util;

/** used logging MDC keys. */
public final class LoggerField {

  public static final String USER = "user";
  
  public static final String SESSION = "session";
  
}
