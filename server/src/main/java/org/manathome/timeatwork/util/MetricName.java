package org.manathome.timeatwork.util;

/** time@work prometheus metrics. 
    custom metrics emitted by time@work. 
   
 */
public class MetricName {
  
  /** counter: login attempts into application.
      segregated by label *result* into _ok_ and _failed_. 
   */
  public static final String login = "login";
  
  /** user loaded (without cached access). */
  public static final String user_load_count = "user.load";
  
  /** wrong password. */
  public static final String user_password_checked = "user.password.checked";
  
  
  /** counter: how many workingDone (user per task and day) were changed with new week entries.
      segregated by label *type* into _insert_, _update_, _delete_. 
   */
  public static final String workDone_change = "workDone.change";
  
  /** counter: how many new working hours were logged into the system. */
  public static final String workHours_added = "workHours_added";
  
  /** counter: work week retrieval count. */
  public static final String workWeek = "workWeek";

  public static class MetricCommonLabel {
    public static final String result = "result";
    public static final String type = "type";
  }

}
