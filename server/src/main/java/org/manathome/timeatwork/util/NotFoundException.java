package org.manathome.timeatwork.util;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class NotFoundException extends ResponseStatusException  {
  
  private static final long serialVersionUID = 1L;

  public NotFoundException(final String entity, String id) {
    super(HttpStatus.NOT_FOUND, entity + " " + id + " not found");
  }

}
