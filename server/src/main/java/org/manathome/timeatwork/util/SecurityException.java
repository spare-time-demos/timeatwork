package org.manathome.timeatwork.util;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class SecurityException extends ResponseStatusException  {
  
  private static final long serialVersionUID = 1L;

  public SecurityException(final String msg) {
    super(HttpStatus.UNAUTHORIZED, msg);
  }

}
