package org.manathome.timeatwork.util;

public class StringUtil {

  /** remove a prafix string from given string s if found. */ 
  public static String removePraefix(final String s, final String praefix) {
    
    if (s != null && praefix != null && s.startsWith(praefix)) {
      return s.substring(praefix.length());
    }
    return s;
  }
}
