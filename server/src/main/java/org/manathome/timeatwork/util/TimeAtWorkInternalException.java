package org.manathome.timeatwork.util;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

/** internal coding/unexpected technical error. */
public class TimeAtWorkInternalException extends ResponseStatusException {

  private static final long serialVersionUID = 1L;

  public TimeAtWorkInternalException() {
    super(HttpStatus.INTERNAL_SERVER_ERROR);
  }

  public TimeAtWorkInternalException(final String message) {
    super(HttpStatus.INTERNAL_SERVER_ERROR, message);
  }

  public TimeAtWorkInternalException(final String message, final Throwable cause) {
    super(HttpStatus.INTERNAL_SERVER_ERROR, message, cause);
  }

}
