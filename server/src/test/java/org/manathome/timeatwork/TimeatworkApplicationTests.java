package org.manathome.timeatwork;

import static org.assertj.core.api.Assertions.assertThat;

import javax.inject.Inject;

import org.junit.jupiter.api.Test;
import org.manathome.timeatwork.db.repository.ProjectRepository;
import org.springframework.boot.test.context.SpringBootTest;

/** spring boot ci starting up. */
@SpringBootTest
public class TimeatworkApplicationTests {

  @Inject
  ProjectRepository repository;

  @Test
  public void contextLoads() {
    assertThat(repository).isNotNull();
  }

}
