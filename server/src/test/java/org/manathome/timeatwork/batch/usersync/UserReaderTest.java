package org.manathome.timeatwork.batch.usersync;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

class UserReaderTest {

  @Test
  void testRead() {
    
    UserReader ur = new UserReader();
    var s = ur.read();  // dummy test on dummy implementation..
    assertThat(s).isNotBlank();
  }

}
