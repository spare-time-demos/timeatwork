package org.manathome.timeatwork.db;

import static org.assertj.core.api.Assertions.assertThat;

import javax.sql.DataSource;

import org.junit.jupiter.api.Test;
import org.manathome.timeatwork.util.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

@DataJpaTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
public class DatabaseEnvironmentTest {
  
  private static final Logger logger = Logger.getLogger(DatabaseEnvironmentTest.class);
  
  @Autowired
  DataSource dataSource;

  @Test
  void test_current_Database_Vendor_is_retrievable() throws Exception {
    
    assertThat(dataSource).isNotNull();
    
    try (var con = this.dataSource.getConnection()) {
    
      assertThat(con).as("connection").isNotNull();
      
      final var dbProduct = con.getMetaData().getDatabaseProductName();
      final var dbProductVersion = con.getMetaData()
                                      .getDatabaseProductVersion();
  
      System.out.println("used database: " + dbProduct + " version " + dbProductVersion);
      logger.info("used database during tests: " + dbProduct + " version " + dbProductVersion);
  
      assertThat(dbProduct).as("db product").isNotNull();
    }
  }


  @Test
  void test_current_Database_Driver_info_is_retrievable() throws Exception {
    
    try (var con = this.dataSource.getConnection()) {
      
      final var dbDriver = "used database driver: " 
          + con.getMetaData().getDriverName()
          + con.getMetaData().getDriverVersion();      

      System.out.println(dbDriver);
      logger.info(dbDriver);
      assertThat(dbDriver).isNotNull();
    }
    
  }
  
  

}
