package org.manathome.timeatwork.db.repository;

import static org.assertj.core.api.Assertions.assertThat;

import javax.persistence.EntityManager;

import org.hibernate.envers.AuditReader;

import org.junit.jupiter.api.Test;

import org.manathome.timeatwork.domain.Project;
import org.manathome.timeatwork.domain.TeamMember;
import org.manathome.timeatwork.domain.values.Name;
import org.manathome.timeatwork.util.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.transaction.TestTransaction;
import org.springframework.transaction.annotation.Transactional;

/** test active auditing. 
 * https://www.baeldung.com/spring-test-programmatic-transactions
 * 
 */
@SpringBootTest
@Transactional
@DirtiesContext
public class ProjectAuditingTest {
  
  private static final Logger logger = Logger.getLogger(ProjectAuditingTest.class);

  @Autowired
  ProjectRepository repository;

  @Autowired
  TeamMemberRepository tmRepository;
  
  @Autowired
  AuditReader auditReader;
  
  @Autowired
  EntityManager em;

  @Test
  public void testProjectRetrieveAuditRevision() {

    var newProject = new Project(Name.of("unit-project envers x"));
    var newLead = new TeamMember(Name.of("unit-test-enversx"), Name.of("test lead envers x"));
    newProject.addProjectLead(newLead);
    tmRepository.save(newLead);
    
    // create revision
    // ---------------
    var savedProject = repository.save(newProject);        
    TestTransaction.flagForCommit();
    TestTransaction.end();
    
    final var revisionsNumbers = auditReader.getRevisions(Project.class, savedProject.getId());
    
    boolean foundRevision = false;
    for (Number rev : revisionsNumbers) {
      var auditedProjectRevision = auditReader.find(Project.class, savedProject.getId(), rev);
      var revisionDate = auditReader.getRevisionDate(rev);
      
      logger.trace("hibernate enverse audit revision [" 
                                + auditedProjectRevision 
                                + "] at revision [" 
                                + rev + "] created at " 
                                + revisionDate);
      
      foundRevision = true;
      assertThat(auditedProjectRevision.getId()).isEqualTo(savedProject.getId());
    }
    assertThat(foundRevision).isTrue();
    
  }

  @Test
  public void testProjectSaveWithAuditRevision() {

    var newProject = new Project(Name.of("unit-project envers 1"));
    var newLead = new TeamMember(Name.of("unit-test-envers1"), Name.of("test lead envers 1"));
    newProject.addProjectLead(newLead);
    tmRepository.save(newLead);
    
    // first revision
    // --------------
    var savedProject = repository.save(newProject);    
    var id = savedProject.getId();
    logger.trace("will auditing project " + id);
    
    TestTransaction.flagForCommit();
    TestTransaction.end();
    
    final var revisions1 = auditReader.getRevisions(Project.class, savedProject.getId());
    assertThat(revisions1.size()).isEqualTo(1);
    
    // second revision
    // ---------------
    TestTransaction.start();
    savedProject.setName(Name.of("unit-project envers 2"));
    repository.save(savedProject);
    logger.trace("will auditing project second revision " + id);

    TestTransaction.flagForCommit();
    TestTransaction.end();

    final var revisions2 = auditReader.getRevisions(Project.class, savedProject.getId());    
    assertThat(revisions2.size()).isEqualTo(2);

    TestTransaction.start();
    savedProject.removeProjectLead(newLead);
    repository.delete(savedProject);
    TestTransaction.flagForCommit();
    TestTransaction.end();
    
  }


}
