package org.manathome.timeatwork.db.repository;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.stream.StreamSupport;

import javax.inject.Inject;
import org.junit.jupiter.api.Test;
import org.manathome.timeatwork.domain.Project;
import org.manathome.timeatwork.domain.TeamMember;
import org.manathome.timeatwork.domain.values.Name;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

@DataJpaTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
public class ProjectRepositoryTest {

  @Inject
  ProjectRepository repository;

  @Inject
  TeamMemberRepository tmRepository;

  @Test
  public void testProjectRead() {
    assertThat(repository).isNotNull();

    var projects = repository.findAll();
    assertThat(projects).isNotNull();
  }

  @Test
  public void testProjectSave() {

    var newProject = new Project(Name.of("unit-test-project1"));
    var newLead = new TeamMember(Name.of("unit-test-lead1"), Name.of("test lead 1"));
    newProject.addProjectLead(newLead);

    tmRepository.save(newLead);
    var savedProject = repository.save(newProject);

    assertThat(savedProject.getId()).isNotNull();
    assertThat(savedProject.getProjectLeads().stream().allMatch(tl -> tl.getId() != null)).isTrue();

    var projects = repository.findAll();
    assertThat(projects).isNotNull();
    assertThat(StreamSupport
        .stream(projects.spliterator(), false)
        .filter(p -> p.getId().equals(savedProject.getId()))
        .findFirst().get().getName()).isEqualTo(savedProject.getName());
  }

  @Test
  public void testProjectByLead() {

    var newProject = new Project(Name.of("unit-test-project1"));
    var newLead = new TeamMember(Name.of("unit-test-lead1"), Name.of("test lead 1"));
    newProject.addProjectLead(newLead);

    var leadId = tmRepository.save(newLead).getId();
    var savedProject = repository.save(newProject);

    var projects = repository
        .findByProjectLeads(savedProject.getProjectLeads().stream().findFirst().get());
    assertThat(projects).isNotNull();

    assertThat(StreamSupport.stream(projects.spliterator(), false)
        .allMatch(p -> p.getProjectLeads().stream().anyMatch(l -> l.getId().equals(leadId))))
        .isTrue();
  }

}
