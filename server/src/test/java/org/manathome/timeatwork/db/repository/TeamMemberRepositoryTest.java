package org.manathome.timeatwork.db.repository;

import static org.assertj.core.api.Assertions.assertThat;

import javax.inject.Inject;

import org.junit.jupiter.api.Test;
import org.manathome.timeatwork.domain.TeamMember;
import org.manathome.timeatwork.domain.values.Name;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

@DataJpaTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
public class TeamMemberRepositoryTest {

  @Inject
  TeamMemberRepository repository;

  @Test
  public void storeMember() {
    assertThat(repository).isNotNull();

    var t = new TeamMember(Name.of("t1"), Name.of("test-member 1"));
    var savedT = repository.save(t);
    assertThat(savedT).isNotNull();
    assertThat(savedT.getId()).isGreaterThan(0);
  }

}
