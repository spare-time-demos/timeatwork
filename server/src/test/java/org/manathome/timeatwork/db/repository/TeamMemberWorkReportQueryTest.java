package org.manathome.timeatwork.db.repository;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDate;

import org.junit.jupiter.api.Test;

import org.manathome.timeatwork.security.AppUserNames;
import org.manathome.timeatwork.security.UserRoleName;
import org.manathome.timeatwork.util.JsonHelper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;

@SpringBootTest
@WithMockUser(username = AppUserNames.admin, roles = { UserRoleName.ADMIN })
public class TeamMemberWorkReportQueryTest {
  
  @Autowired
  public TeamMemberWorkReportQuery query;
  
  /** check query. */
  @Test
  public void testReadUsersOrderByWorkDoneAscending() {
    
    LocalDate from = LocalDate.now().minusDays(160);
    LocalDate until = LocalDate.now().plusDays(10);
    
    final var results = query.readUsersOrderByWorkDoneAscending(from, until);
    
    assertThat(results).isNotNull();
    assertThat(results).isNotEmpty();

    final var s = JsonHelper.asJsonString(results);    

    assertThat(results.stream().anyMatch(r -> r.getUserId().equals(AppUserNames.user)))
      .as("has a user row " + s).isTrue();

    assertThat(results.stream().anyMatch(r -> r.getWorkHoursCount() > 0))
    .as("has any count: " + s).isTrue();

    assertThat(results.stream().anyMatch(r -> r.getWorkHoursSum() > 0))
    .as("has any sum: " + s).isTrue();

    assertThat(s).as(s).startsWith("[{\"userId\":\"");
  }

}
