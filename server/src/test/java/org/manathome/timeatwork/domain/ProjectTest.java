package org.manathome.timeatwork.domain;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.manathome.timeatwork.domain.values.Name;

public class ProjectTest {

  /** builder. */
  public static Project buildTestProject(final String projectName, final String teamLeadUserId) {
    final Project project = Project.buildForTest(-9999, projectName);
    assertThat(project).as("project").isNotNull();

    if (teamLeadUserId != null) {
      var lead = new TeamMember(Name.of(teamLeadUserId), Name.of(teamLeadUserId + " team lead"));
      project.addProjectLead(lead);
    }

    return project;
  }
  
  public static Project buildTestProject(final String projectName) {
    return buildTestProject(projectName, null);
  }

  @Test
  @DisplayName("create a new project")
  public void createProject() {
    buildTestProject("project");
  }

  @Test
  @DisplayName("work with project leads")
  public void createProjectWithLeads() {
    var project = buildTestProject("project 2", "u1");
    var lead1 = project.getProjectLeads().stream().findFirst().get();
    final TeamMember lead2 = new TeamMember(Name.of("u2"), Name.of("lead2"));
    project.addProjectLead(lead2);

    assertThat(project.getProjectLeads().stream().count()).as("2 leads").isEqualTo(2);

    project.removeProjectLead(lead1);
    assertThat(project.getProjectLeads().stream().count()).as("1 leads").isEqualTo(1);
    assertThat(project.getProjectLeads().stream()
        .anyMatch(tm -> tm.getName().equals(lead2.getName())))
        .as("lead 2 remainds").isTrue();
  }

  @Test
  public void testProjectWithDuplicateLeads() {
    var project = buildTestProject("project 3", "tl1");
    var tl1 = project.getProjectLeads().stream().findFirst().get();
    project.addProjectLead(tl1); // readd
    assertThat(project.getProjectLeads().stream().count()).as("1 distinct lead").isEqualTo(1);
  }

  @Test
  public void testProjectRemoveUnknownLead() {
    var project = buildTestProject("project 4", "tl1");
    var tl1 = project.getProjectLeads().stream().findFirst().get();
    project.removeProjectLead(tl1);
    project.removeProjectLead(tl1);
    assertThat(project.getProjectLeads().stream().count()).as("0 leads").isEqualTo(0);
  }

}
