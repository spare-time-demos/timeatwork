package org.manathome.timeatwork.domain;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDate;

import org.junit.jupiter.api.Test;

public class TaskAssignmentTest {
  
  @Test
  public void testTaskAssignmentAllowesWork() throws Exception {

    var task = Task.buildForTest(-3, "dummy-task-with-assignment");
    var member = TeamMember.buildForTest(-1, "dummy user", "dummy");
    var assignment = TaskAssignment.buildForTest(member.getId(), task.getId());

    var workDone = new WorkDone(member, task, LocalDate.now());
    assertThat(assignment.allowesWork(workDone)).isTrue();
  }
  
  
  @Test
  public void testTaskAssignmentDoesNotAlloweWork() throws Exception {

    var task = Task.buildForTest(-2, "dummy-task-with-assignment");
    var member = TeamMember.buildForTest(-1, "dummy user", "dummy");
    var assignment = TaskAssignment.buildForTest(member.getId(), task.getId());

    var workDone = new WorkDone(member, task, LocalDate.now().minusDays(15));
    assertThat(assignment.allowesWork(workDone)).isFalse();
  }
  
  @Test
  public void testTaskAssignmentDoesNotAlloweWorkAfterUntil() throws Exception {

    var task = Task.buildForTest(-1, "dummy-task-with-assignment");
    var member = TeamMember.buildForTest(-1, "dummy user", "dummy");
    var assignment = TaskAssignment.buildForTest(member.getId(), task.getId());
    assignment.setAssignmentRange(LocalDate.now(), LocalDate.now().plusDays(2));
    var workDone = new WorkDone(member, task, LocalDate.now().plusDays(3));
    assertThat(assignment.allowesWork(workDone)).isFalse();
  }


}
