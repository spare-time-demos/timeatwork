package org.manathome.timeatwork.domain;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.manathome.timeatwork.domain.values.Name;

public class TaskTest {

  @Test
  @DisplayName("create a new task")
  public void createProject() {
    Task task = new Task(new Project(Name.of("project")), Name.of("task"));
    assertThat(task).as("task").isNotNull();
  }
  
}
