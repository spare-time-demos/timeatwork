package org.manathome.timeatwork.domain.values;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

import org.manathome.timeatwork.domain.values.Name;


public class NameTest {

  @Test
  void testNameCreation() {
    final Name name = Name.of("my name!");
    assertThat(name.toString()).isEqualTo("my name!");
  }
  
  @Test
  void testNameCloneCreation() {
    final Name name = Name.of("my name!");
    final Name clonedName = new Name(name);
    
    assertThat(clonedName).isEqualTo(name);
  }

  @Test
  void testEquals() {
    final Name name1 = Name.of("an equal name!");
    final Name name2 = Name.of("an equal name!");
    final Name name3 = Name.of("not an equal name");
    
    assertThat(name1).isEqualTo(name2);
    assertThat(name1).isNotEqualTo(name3);
  }
  
}
