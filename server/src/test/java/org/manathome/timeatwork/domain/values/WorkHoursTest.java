package org.manathome.timeatwork.domain.values;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

import org.junit.jupiter.api.Test;
import org.manathome.timeatwork.util.assertions.RequirementNotMet;

public class WorkHoursTest {

  @Test
  public void testWorkHoursCreation() {
    final WorkHours wh1 = WorkHours.ofHours(3);
    final WorkHours wh2 = WorkHours.ofHours(4);
    
    assertThat(wh1.asHours()).isEqualTo(3);    
    assertThat(wh2.asDuration().toHours()).isEqualTo(4);
  }

  @Test
  public void testWorkHoursEquality() {
    final WorkHours wh1 = WorkHours.ofHours(3);
    final WorkHours wh2 = WorkHours.ofHours(3);
    final WorkHours wh3 = WorkHours.ofHours(5);
    
    assertThat(wh1).isEqualTo(wh2);    
    assertThat(wh1).isNotEqualTo(wh3);
  }

  @Test
  public void testWorkHoursAdditions() {
    final WorkHours wh1 = WorkHours.ofHours(3).add(WorkHours.ofHours(4));    
    assertThat(wh1.asHours()).isEqualTo(7);    

    final WorkHours wh2 = WorkHours.ofHours(7).add(WorkHours.ofHours(0));    
    assertThat(wh2.asHours()).isEqualTo(7);    
  }

  @Test
  public void testInvalidWorkingHours() {    
    assertThatThrownBy(() -> {
      WorkHours.ofHours(-1); 
    }).isInstanceOf(RequirementNotMet.class);
  }

}
