package org.manathome.timeatwork.rest;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;

import javax.transaction.Transactional;

import org.junit.jupiter.api.Test;
import org.manathome.timeatwork.security.AppUserNames;
import org.manathome.timeatwork.security.UserRoleName;
import org.manathome.timeatwork.test.annotation.Documentation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
@AutoConfigureMockMvc
@Documentation
@Transactional
public class AdminReportIntegrationTest {
  

  @Autowired
  private MockMvc mvc;

  @Test
  @WithMockUser(username = AppUserNames.admin, roles = { UserRoleName.USER, UserRoleName.ADMIN })  
  public void testReportUserByWork() throws Exception {

    var today = LocalDate.now().toString(); // calc todo start of week

    mvc.perform(get(AdminReportResource.PATH_REPORT_USER + today + "/byWork"))
        .andDo(print())
        .andExpect(status().isOk())
        .andDo(document(AdminReportResource.PATH_REPORT_USER 
            + AdminReportResource.FROM_ISO_DATE_STRING
            + "/byWork"
            + "/GET"));
    ;
  }

}
