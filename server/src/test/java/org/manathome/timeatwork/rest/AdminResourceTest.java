package org.manathome.timeatwork.rest;

import static org.hamcrest.CoreMatchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.manathome.timeatwork.security.AuthenticationEntryPointImplementation;
import org.manathome.timeatwork.security.AuthenticationTestHelperBase;
import org.manathome.timeatwork.security.JwtTokenProcessor;
import org.manathome.timeatwork.security.UserRoleName;
import org.manathome.timeatwork.util.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.annotation.DirtiesContext.MethodMode;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import wiremock.org.eclipse.jetty.http.HttpStatus;

/** use this test as authentication test using bearer token. */
@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = ClassMode.BEFORE_EACH_TEST_METHOD)
public class AdminResourceTest extends AuthenticationTestHelperBase {

  private static final Logger logger = Logger.getLogger(LoginResourceIntegrationTest.class);

  @Autowired
  private MockMvc mvc;

  @Test
  public void testTokenAuthenticatedInfoEndpointAsAuthorizedAdmin() throws Exception {
    
    final String bearerToken = authenticatedTokenForTestUser(
        mvc, 
        AuthenticationTestHelperBase.TEST_ADMIN_NAME);

    logger.debug("calling admin url " + AdminResource.PATH_ADMIN_INFO + " as admin with token");

    mvc.perform(get(AdminResource.PATH_ADMIN_INFO)
                .header(JwtTokenProcessor.TOKEN_HEADER_AUTHORIZATION, bearerToken))
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().string(containsString(AdminResource.ADMIN_HEADER)))
        .andExpect(content().string(containsString(AuthenticationTestHelperBase.TEST_ADMIN_NAME)));
  }

  @Test
  @DirtiesContext(methodMode = MethodMode.BEFORE_METHOD)
  public void testTokenAuthenticatedInfoEndpointInsufficientlyAuthorized() throws Exception {
    
    final String bearerToken = authenticatedTokenForTestUser(
        mvc, 
        AuthenticationTestHelperBase.TEST_USER_NAME);

    mvc.perform(get(AdminResource.PATH_ADMIN_INFO)
                .header(JwtTokenProcessor.TOKEN_HEADER_AUTHORIZATION, bearerToken))
        .andDo(print())
        .andExpect(status().is(HttpStatus.FORBIDDEN_403));
  }

  @Test
  public void testTokenAuthenticatedInfoEndpointUnAuthorized() throws Exception {

    mvc.perform(get(AdminResource.PATH_ADMIN_INFO))
        .andExpect(status().is(HttpStatus.UNAUTHORIZED_401))
        .andExpect(content().string(
            containsString(AuthenticationEntryPointImplementation.HINT_TEXT)));
  }

  @WithMockUser(username = "mockadmin", roles = UserRoleName.ADMIN)
  @Test
  public void testAuthenticatedInfoEndpointWithSpringMockAdmin() throws Exception {

    mvc.perform(get(AdminResource.PATH_ADMIN_INFO))
        .andExpect(status().isOk())
        .andExpect(content().string(containsString(AdminResource.ADMIN_HEADER)))
        .andExpect(content().string(containsString("mockadmin")));
  }

  @WithMockUser(username = "mockuser", roles = UserRoleName.USER)
  @Test
  public void testAuthenticatedInfoEndpointWithSpringMockUser() throws Exception {

    mvc.perform(get(AdminResource.PATH_ADMIN_INFO))
        .andExpect(status().is4xxClientError());
  }

  @WithMockUser(username = "mocklead", roles = UserRoleName.PROJECTLEAD)
  @Test
  public void testAuthenticatedInfoEndpointWithSpringMockLead() throws Exception {

    mvc.perform(get(AdminResource.PATH_ADMIN_INFO))
        .andExpect(status().is4xxClientError());
  }
  
}
