package org.manathome.timeatwork.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.manathome.timeatwork.dto.CPasswordChangeRequest;
import org.manathome.timeatwork.security.AppUserNames;
import org.manathome.timeatwork.security.AuthenticationTestHelperBase;
import org.manathome.timeatwork.security.UserRoleName;
import org.manathome.timeatwork.test.annotation.Documentation;
import org.manathome.timeatwork.util.JsonTestUtil;
import org.manathome.timeatwork.util.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.cache.CacheType;
import org.springframework.boot.test.autoconfigure.core.AutoConfigureCache;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.MethodMode;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
@Documentation
@DirtiesContext
@AutoConfigureCache(cacheProvider = CacheType.NONE)
public class LoginResourceChangePasswordIntegrationTest extends AuthenticationTestHelperBase {

  private static final Logger logger = Logger
      .getLogger(LoginResourceChangePasswordIntegrationTest.class);

  @Autowired
  private MockMvc mvc;  
  
  @Test
  @WithMockUser(username = AppUserNames.tlead1, roles = { UserRoleName.USER })    
  @DirtiesContext(methodMode = MethodMode.AFTER_METHOD)
  public void testSuccessfulPasswordChangeOnProjectLead() throws Exception {

    var pwChange = new CPasswordChangeRequest();
    pwChange.setOldPassword(TEST_USER_PASSWORD);
    pwChange.setNewPassword(TEST_USER_PASSWORD);
    
    var pwChangeJson = JsonTestUtil.asJsonString(pwChange);

    logger.debug("test pw change");

    var result = mvc
        .perform(post(LoginResource.URL_CHANGE_PASSWORD)
            .contentType(MediaType.APPLICATION_JSON)
            .content(pwChangeJson))
        .andDo(print())
        .andExpect(status().isOk())
        .andDo(document(LoginResource.URL_CHANGE_PASSWORD + "/POST"))
        .andReturn();
    
    assertThat(result.getResponse().getContentAsString())
      .containsIgnoringCase("password changed successfully");
  }
  
  @Test
  @WithMockUser(username = AppUserNames.user, roles = { UserRoleName.USER })
  @DirtiesContext(methodMode = MethodMode.AFTER_METHOD)
  public void testUnsuccessfulPasswordChangeWrongOldPassword() throws Exception {

    var pwChange = new CPasswordChangeRequest();
    pwChange.setOldPassword(TEST_USER_PASSWORD + "-wrong");
    pwChange.setNewPassword("123456");
    
    var pwChangeJson = JsonTestUtil.asJsonString(pwChange);

    mvc
        .perform(post(LoginResource.URL_CHANGE_PASSWORD)
            .contentType(MediaType.APPLICATION_JSON)
            .content(pwChangeJson))
        .andDo(print())
        .andExpect(status().is4xxClientError());
  }  
  
  @Test
  @WithMockUser(username = AppUserNames.user, roles = { UserRoleName.USER })    
  public void testUnsuccessfulPasswordChangeWeakNewPassword() throws Exception {

    var pwChange = new CPasswordChangeRequest();
    pwChange.setOldPassword(TEST_USER_PASSWORD + "-wrong");
    pwChange.setNewPassword("?");
    
    var pwChangeJson = JsonTestUtil.asJsonString(pwChange);

    mvc
        .perform(post(LoginResource.URL_CHANGE_PASSWORD)
            .contentType(MediaType.APPLICATION_JSON)
            .content(pwChangeJson))
        .andDo(print())
        .andExpect(status().is4xxClientError());
  }    
    

}
