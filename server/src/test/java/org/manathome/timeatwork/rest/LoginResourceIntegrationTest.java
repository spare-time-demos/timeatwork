package org.manathome.timeatwork.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Locale;

import javax.transaction.Transactional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.manathome.timeatwork.security.AuthenticationRequest;
import org.manathome.timeatwork.security.AuthenticationTestHelperBase;
import org.manathome.timeatwork.test.annotation.Documentation;
import org.manathome.timeatwork.util.JsonTestUtil;
import org.manathome.timeatwork.util.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.cache.CacheType;
import org.springframework.boot.test.autoconfigure.core.AutoConfigureCache;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.MethodMode;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
@Documentation
@Transactional
@DirtiesContext
@AutoConfigureCache(cacheProvider = CacheType.NONE)
public class LoginResourceIntegrationTest extends AuthenticationTestHelperBase {

  private static final Logger logger = Logger.getLogger(LoginResourceIntegrationTest.class);

  @Autowired
  private MockMvc mvc;

  @Test
  public void testSuccessfulTokenOnAuthenticationWithTestHelper() throws Exception {

    final String token = authenticatedTokenForTestUser(mvc, TEST_USER_NAME);
    assertThat(token).isNotBlank();
  }

  @Test
  public void testFailedTokenOnAuthenticationWithTestHelper() throws Exception {

    Assertions.assertThrows(AssertionError.class, () -> {
      authenticatedTokenForTestUser(mvc, TEST_USER_NAME + "_UNKNOWN");
    });
  }

  @Test
  public void testSuccessfulLoginWithValidUserCredentials() throws Exception {

    var authenticationRequest = new AuthenticationRequest(TEST_USER_NAME, TEST_USER_PASSWORD);
    var loginJson = JsonTestUtil.asJsonString(authenticationRequest);

    logger.debug("test login post: " + loginJson);

    var result = mvc
        .perform(post(LoginResource.URL_AUTHENTICATION)
            .contentType(MediaType.APPLICATION_JSON)
            .content(loginJson))
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.ok", is(true)))
        .andExpect(jsonPath("$.message", containsString("LOK:")))
        .andExpect(jsonPath("$.token", notNullValue()))
        .andExpect(jsonPath("$.userId", containsString(TEST_USER_NAME)))
        .andExpect(jsonPath("$.hasRoleProjectLead", is(false)))
        .andExpect(jsonPath("$.hasRoleAdmin", is(false)))
        .andDo(document(LoginResource.URL_AUTHENTICATION + "/POST"))
        .andReturn();

    assertThat(result.getResponse().containsHeader(LoginResource.TOKEN_HEADER_AUTHORIZATION))
        .as(LoginResource.TOKEN_HEADER_AUTHORIZATION + " header").isTrue();
  }
  

  @Test
  public void testSuccessfulLoginWithValidAdminCredentials() throws Exception {

    var authenticationRequest = new AuthenticationRequest(
        TEST_ADMIN_NAME, 
        TEST_USER_PASSWORD);
    
    mvc
        .perform(post(LoginResource.URL_AUTHENTICATION)
            .contentType(MediaType.APPLICATION_JSON)
            .content(JsonTestUtil.asJsonString(authenticationRequest)))
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.ok", is(true)))
        .andExpect(jsonPath("$.message", containsString("LOK:")))
        .andExpect(jsonPath("$.hasRoleProjectLead", is(false)))
        .andExpect(jsonPath("$.hasRoleAdmin", is(true)));
  }
  
  @Test
  public void testSuccessfulLoginWithValidProjectLeadCredentials() throws Exception {

    var authenticationRequest = new AuthenticationRequest(
        TEST_PROJECTLEAD_NAME, 
        TEST_USER_PASSWORD);
    
    mvc
        .perform(post(LoginResource.URL_AUTHENTICATION)
            .contentType(MediaType.APPLICATION_JSON)
            .content(JsonTestUtil.asJsonString(authenticationRequest)))
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.ok", is(true)))
        .andExpect(jsonPath("$.message", containsString("LOK:")))
        .andExpect(jsonPath("$.hasRoleProjectLead", is(true)))
        .andExpect(jsonPath("$.hasRoleAdmin", is(false)));
  }  
  

  @Test
  public void testFailedLoginWithUnknownUserAndGermanLocale() throws Exception {

    var authenticationRequest = new AuthenticationRequest(
        TEST_USER_NAME + "_NOT_KNOWN", 
        TEST_USER_PASSWORD);

    var loginJson = JsonTestUtil.asJsonString(authenticationRequest);

    mvc.perform(post(LoginResource.URL_AUTHENTICATION)
        .contentType(MediaType.APPLICATION_JSON)
        .locale(Locale.GERMAN)
        .content(loginJson))
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.ok", is(false)))
        .andExpect(jsonPath("$.message", 
                            containsString("LERR1: Unbekannter Benutzer oder falsches Passwort")))
        .andExpect(jsonPath("$.token", nullValue()));
  }
  
  @Test
  public void testFailedLoginWithUnknownUser() throws Exception {

    var authenticationRequest = new AuthenticationRequest(
        TEST_USER_NAME + "_NOT_KNOWN", 
        TEST_USER_PASSWORD);

    var loginJson = JsonTestUtil.asJsonString(authenticationRequest);

    mvc.perform(post(LoginResource.URL_AUTHENTICATION)
        .contentType(MediaType.APPLICATION_JSON)
        .content(loginJson))
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.ok", is(false)))
        .andExpect(jsonPath("$.message", containsString("LERR1: invalid user or password")))
        .andExpect(jsonPath("$.token", nullValue()));
  }  

  
  @Test
  @DirtiesContext(methodMode = MethodMode.AFTER_METHOD)
  public void testFailedLoginWithWrongPassword() throws Exception {

    var authenticationRequest = new AuthenticationRequest(
        TEST_USER_NAME, 
        TEST_USER_PASSWORD + "_INVALID");

    var loginJson = JsonTestUtil.asJsonString(authenticationRequest);

    var result = mvc
        .perform(post(LoginResource.URL_AUTHENTICATION)
            .contentType(MediaType.APPLICATION_JSON)
            .content(loginJson))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.ok", is(false)))
        .andExpect(jsonPath("$.message", containsString("LERR1: invalid user or password")))
        .andExpect(jsonPath("$.token", nullValue())).andReturn();

    assertThat(result.getResponse().containsHeader(LoginResource.TOKEN_HEADER_AUTHORIZATION))
        .as(LoginResource.TOKEN_HEADER_AUTHORIZATION + " header found").isFalse();
  }

  @Test
  public void testFailedLoginWithoutPassword() throws Exception {

    var authenticationRequest = new AuthenticationRequest(TEST_USER_NAME, null);

    var loginJson = JsonTestUtil.asJsonString(authenticationRequest);

    mvc.perform(post(LoginResource.URL_AUTHENTICATION)
          .contentType(MediaType.APPLICATION_JSON)
          .content(loginJson))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.ok", is(false)))
        .andExpect(jsonPath("$.message", containsString("LERR1: invalid user or password")))
        .andExpect(jsonPath("$.token", nullValue()));
  }

  @Test
  public void testFailedLoginWithoutUser() throws Exception {

    var authenticationRequest = new AuthenticationRequest(null, "irrelevant-pw");

    var loginJson = JsonTestUtil.asJsonString(authenticationRequest);

    mvc.perform(post(LoginResource.URL_AUTHENTICATION)
        .contentType(MediaType.APPLICATION_JSON)
        .content(loginJson))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.ok", is(false)))
        .andExpect(jsonPath("$.message", containsString("LERR1: invalid user or password")))
        .andExpect(jsonPath("$.token", nullValue()));
  }

  @Test
  public void testFailedLoginWithoutAnyData() throws Exception {

    mvc.perform(post(LoginResource.URL_AUTHENTICATION)).andExpect(status().isUnauthorized());
  }

  @Test
  public void testFailedLoginWithCallViaGet() throws Exception {

    mvc.perform(get(LoginResource.URL_AUTHENTICATION)).andExpect(status().is4xxClientError());
  }

}
