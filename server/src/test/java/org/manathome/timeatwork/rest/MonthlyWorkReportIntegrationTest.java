package org.manathome.timeatwork.rest;

import static org.hamcrest.Matchers.containsString;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;

import org.junit.jupiter.api.Test;

import org.manathome.timeatwork.security.AppUserNames;
import org.manathome.timeatwork.security.UserRoleName;
import org.manathome.timeatwork.test.annotation.Documentation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
@AutoConfigureMockMvc
@Documentation
@WithMockUser(username = AppUserNames.user, roles = { UserRoleName.USER })  
public class MonthlyWorkReportIntegrationTest {
  

  @Autowired
  private MockMvc mvc;

  @Test
  public void testReportMonthlyByDay() throws Exception {

    var today = LocalDate.now().toString(); // calc todo start of week

    mvc.perform(get(MonthlyReportResource.PATH_REPORT_MONTHLY + today + "/byDay"))
        .andDo(print())
        .andExpect(status().isOk())
        .andDo(document(MonthlyReportResource.PATH_REPORT_MONTHLY 
            + MonthlyReportResource.FROM_ISO_DATE_STRING
            + "/byDay"
            + "/GET"));
    ;
  }

  @Test
  public void testReportMonthlyByProject() throws Exception {

    var today = LocalDate.now().toString(); // calc todo start of week

    mvc.perform(get(MonthlyReportResource.PATH_REPORT_MONTHLY + today + "/byProject"))
        .andDo(print())
        .andExpect(status().isOk())
        .andDo(document(MonthlyReportResource.PATH_REPORT_MONTHLY 
            + MonthlyReportResource.FROM_ISO_DATE_STRING
            + "/byProject"
            + "/GET"));
    ;
  }
  
  @Test
  public void testReportMonthlyByProjectAsPdf() throws Exception {

    var today = LocalDate.now().toString(); 

    mvc.perform(get(MonthlyReportResource.PATH_REPORT_MONTHLY + today + "/byProject/pdf"))
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().contentType(ResourceHelper.CONTENT_TYPE_PDF))
        .andExpect(content().string(containsString("PDF-1.4")))
        .andDo(document(MonthlyReportResource.PATH_REPORT_MONTHLY 
            + MonthlyReportResource.FROM_ISO_DATE_STRING
            + "/byProject/pdf"
            + "/GET"));
    ;
  }  
}
