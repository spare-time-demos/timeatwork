package org.manathome.timeatwork.rest;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDate;
import javax.transaction.Transactional;

import org.junit.jupiter.api.Test;
import org.manathome.timeatwork.security.AppUserNames;
import org.manathome.timeatwork.security.UserRoleName;
import org.manathome.timeatwork.test.annotation.Documentation;
import org.manathome.timeatwork.util.JsonHelper;
import org.manathome.timeatwork.util.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.cache.CacheType;
import org.springframework.boot.test.autoconfigure.core.AutoConfigureCache;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;

@SpringBootTest
@AutoConfigureMockMvc
@Documentation
@Transactional
@AutoConfigureCache(cacheProvider = CacheType.NONE)
public class MonthlyWorkReportResourceTest {


  private static final Logger logger = Logger.getLogger(MonthlyWorkReportResourceTest.class);
  
  private static final int WorkedHoursInMonthByUserMax = 22;
  
  
  @Autowired
  private MonthlyReportResource reportResource;
  

  private static final LocalDate today = LocalDate.now();

  @Test
  @WithMockUser(username = AppUserNames.user, 
                roles = { UserRoleName.USER })    
  public void testMonthlyReportByDay() throws Exception {


    final var reportRows = reportResource.reportMonthlyByDay(today.toString());
    
    logger.debug("report: " + JsonHelper.asJsonString(reportRows));

    assertThat(reportRows)
      .isNotNull();
    assertThat(reportRows.getMonthlyHours())
      .as("monthly worked hours from month " + today + " for user " + AppUserNames.user)
      .isLessThanOrEqualTo(WorkedHoursInMonthByUserMax);
    
    assertThat(reportRows.getMonthlyHours())
      .as("monthly worked hours from month " + today + " for user " + AppUserNames.user)
      .isGreaterThan(0);
    
    assertThat(reportRows.getDailyWorkReports().length)
      .as("worked days count from month " + today + " for user " + AppUserNames.user)
      .isLessThanOrEqualTo(4);
  }
  
  @Test
  @WithMockUser(username = AppUserNames.user, 
                roles = { UserRoleName.USER })    
  public void testMonthlyReportByProject() throws Exception {


    var today = LocalDate.now().toString();    
    final var reportRows = reportResource.reportMonthlyByProject(today);

    logger.debug("report: " + JsonHelper.asJsonString(reportRows));
    
    assertThat(reportRows)
      .isNotNull();

    assertThat(reportRows.getMonthlyHours())
      .as("monthly worked hours from month " + today + " for user " + AppUserNames.user)
      .isLessThanOrEqualTo(WorkedHoursInMonthByUserMax);
    
    assertThat(reportRows.getMonthlyHours())
      .as("monthly worked hours from month " + today + " for user " + AppUserNames.user)
      .isGreaterThan(0);
    
    assertThat(reportRows.getProjectWorkReports().length)
      .as("project count from month " + today + " for user " + AppUserNames.user)
      .isEqualTo(1);
  }
  
}
