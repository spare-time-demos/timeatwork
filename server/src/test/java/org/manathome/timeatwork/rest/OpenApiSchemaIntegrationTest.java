package org.manathome.timeatwork.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.FileWriter;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.manathome.timeatwork.test.annotation.Documentation;
import org.manathome.timeatwork.test.annotation.IntegrationTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

/**
 * download openapi schema files time@work.openapi.json and
 * time@work.openapi.yaml.
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
@IntegrationTest
@Documentation
public class OpenApiSchemaIntegrationTest {

  @Autowired
  private MockMvc mvc;

  @Test
  public void testGetApiDocsOpenApiAsJson() throws Exception {

    
    var result = mvc.perform(get(OpenapiTags.PATH_API + "api-docs"))
        .andExpect(status().isOk())
        .andReturn();

    var openApiContent = result.getResponse().getContentAsString();
    assertThat(openApiContent).contains("internal rest api, used by time@work angular client");

    try (FileWriter fileWriter = new FileWriter("time@work.openapi.json")) {
      fileWriter.write(openApiContent);
      fileWriter.close();
    }
  }

  @Test
  public void testGetApiDocsOpenApiAsYaml() throws Exception {

    var result = mvc.perform(get(OpenapiTags.PATH_API + "api-docs.yaml"))
        .andExpect(status().isOk())
        .andReturn();

    var openApiContent = result.getResponse().getContentAsString();
    assertThat(openApiContent).contains("internal rest api, used by time@work angular client");

    try (FileWriter fileWriter = new FileWriter("time@work.openapi.yaml")) {
      fileWriter.write(openApiContent);
      fileWriter.close();
    }
  }

}
