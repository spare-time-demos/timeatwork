package org.manathome.timeatwork.rest;

import static org.hamcrest.Matchers.greaterThanOrEqualTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.lessThanOrEqualTo;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;

import javax.transaction.Transactional;

import org.junit.jupiter.api.Test;
import org.manathome.timeatwork.security.AppUserNames;
import org.manathome.timeatwork.security.UserRoleName;
import org.manathome.timeatwork.test.annotation.Documentation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
@AutoConfigureMockMvc
@Documentation
@Transactional
@DirtiesContext // needed w/ "cache is closed"
public class ProjectReportIntegrationTest {

  static final int expectedTaskCountInReport = 4;
  static final int projectId = -1;
  static final String today = LocalDate.now().toString(); 

  @Autowired
  private MockMvc mvc;

  
  @Test
  @WithMockUser(username = AppUserNames.admin, 
                   roles = { UserRoleName.USER, UserRoleName.ADMIN })  
  public void testReportUserByWorkForAdmin() throws Exception {

    mvc.perform(get(ProjectReportResource.PATH_REPORT_PROJECT 
                  + projectId  + "/" 
                  + today 
                  + "/byTask"))
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.length()", is(lessThanOrEqualTo(expectedTaskCountInReport))))
        .andExpect(jsonPath("$.length()", is(greaterThanOrEqualTo(1))))
        .andDo(document(ProjectReportResource.PATH_REPORT_PROJECT
            + ProjectReportResource.PROJECT_ID + "/"
            + ProjectReportResource.FROM_ISO_DATE_STRING
            + "/byTask"
            + "/GET"));
  }
  

  @Test
  @WithMockUser(username = AppUserNames.tlead1, 
                   roles = { UserRoleName.USER, UserRoleName.PROJECTLEAD })  
  public void testReportUserByWorkForLead() throws Exception {

    mvc.perform(get(ProjectReportResource.PATH_REPORT_PROJECT 
                  + projectId  + "/" 
                  + today 
                  + "/byTask"))
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.length()", is(lessThanOrEqualTo(expectedTaskCountInReport))))
        .andExpect(jsonPath("$.length()", is(greaterThanOrEqualTo(1))));
  }  


  @Test
  @WithMockUser(username = AppUserNames.tlead1, 
                   roles = { UserRoleName.USER, UserRoleName.PROJECTLEAD })  
  public void testReportUserByWorkForLeadAsExcel() throws Exception {

    mvc.perform(get(ProjectReportResource.PATH_REPORT_PROJECT 
                  + projectId  + "/" 
                  + today 
                  + "/byTask/excel"))
        .andDo(print())
        .andExpect(content().contentType(ResourceHelper.CONTENT_TYPE_EXCEL))
        .andExpect(status().isOk());
  }  
  
  @Test
  @WithMockUser(username = AppUserNames.user, 
                   roles = { UserRoleName.USER })  
  public void testReportUserByWorkForUserNotAllowed() throws Exception {

    mvc.perform(get(ProjectReportResource.PATH_REPORT_PROJECT 
                  + projectId  + "/" 
                  + today 
                  + "/byTask"))
        .andExpect(status().is4xxClientError());
  }  

}
