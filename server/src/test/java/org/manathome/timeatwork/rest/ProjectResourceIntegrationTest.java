package org.manathome.timeatwork.rest;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import javax.persistence.EntityManager;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.manathome.timeatwork.db.repository.ProjectRepository;
import org.manathome.timeatwork.db.repository.TeamMemberRepository;
import org.manathome.timeatwork.domain.Project;
import org.manathome.timeatwork.domain.values.Name;
import org.manathome.timeatwork.security.AppUserNames;
import org.manathome.timeatwork.security.UserRoleName;
import org.manathome.timeatwork.test.annotation.Documentation;
import org.manathome.timeatwork.util.JsonTestUtil;
import org.manathome.timeatwork.util.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
@Documentation
@DirtiesContext
public class ProjectResourceIntegrationTest {

  private static final Logger logger = Logger.getLogger(ProjectResourceIntegrationTest.class);

  @Autowired
  private MockMvc mvc;

  @Autowired
  private TeamMemberRepository teamMemberRepository;

  @Autowired
  private ProjectRepository projectRepository;

  @Autowired
  private EntityManager em;

  
  @Test
  @WithMockUser(username = AppUserNames.user, roles = UserRoleName.USER)  
  public void testGetProjectsWithInsufficientUser() throws Exception {

    mvc.perform(get(ProjectResource.PATH_PROJECT))
       .andExpect(status().is4xxClientError());
  }

  
  @Test
  @WithMockUser(username = AppUserNames.tlead1, 
                roles = { UserRoleName.USER, UserRoleName.PROJECTLEAD })  
  public void testGetProjects() throws Exception {

    mvc.perform(get(ProjectResource.PATH_PROJECT)).andDo(print()).andExpect(status().isOk())
        .andExpect(jsonPath("$[0].id", org.hamcrest.Matchers.is(-3)))
        .andExpect(jsonPath("$[0].name", org.hamcrest.Matchers.is("a third inactive project")))
        .andDo(document("rest/project/GET"));
  }

  
  @Test
  @WithMockUser(username = AppUserNames.tlead1, 
                roles = { UserRoleName.PROJECTLEAD, UserRoleName.USER })  
  public void testGetProject() throws Exception {

    mvc.perform(get(ProjectResource.PATH_PROJECT + "-1"))
      .andDo(print())
      .andExpect(status().isOk())
      .andExpect(jsonPath("$.id", org.hamcrest.Matchers.is(-1)))
      .andExpect(jsonPath("$.name", org.hamcrest.Matchers.is("a first active project")))
      .andExpect(jsonPath("$.projectLeads.length()", org.hamcrest.Matchers.is(2)))
      .andDo(document("rest/project/id/GET"));   
  }

  
  @Test
  @Transactional
  @WithMockUser(username = AppUserNames.tlead1, 
                roles = { UserRoleName.PROJECTLEAD, UserRoleName.USER })  
  public void testChangeProject() throws Exception {

    var projectLead = teamMemberRepository
                        .findByUserId(Name.of(AppUserNames.tlead1))
                        .orElseThrow();
    
    var project = Project.buildForTest(0, "a project to be changed");
    project.setActive(true);
    project.addProjectLead(projectLead);

    project = projectRepository.save(project);
    em.flush();

    project.setName(Name.of("a project after change"));
    project.setActive(false);

    var projectJson = JsonTestUtil.asJsonString(project);

    logger.debug("put data: " + projectJson);

    mvc.perform(put(ProjectResource.PATH_PROJECT + project.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(projectJson))
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.id", org.hamcrest.Matchers.is(project.getId().intValue())))
        .andExpect(jsonPath("$.name", org.hamcrest.Matchers.is(project.getName().toString())))
        .andExpect(jsonPath("$.projectLeads[0].id", 
                   org.hamcrest.Matchers.is(projectLead.getId().intValue())))
        .andDo(document("rest/project/id/PUT"));
  }

  @Test
  @Transactional
  @WithMockUser(username = "mockuser", roles = UserRoleName.USER)  
  public void testChangeProjectWithoutProjectLead() throws Exception {

    var projectLead = teamMemberRepository.findById(Long.valueOf(-1)).orElseThrow();
    var project = Project.buildForTest(0, "a project to be changed");
    project.setActive(true);
    project.addProjectLead(projectLead);

    project = projectRepository.save(project);
    em.flush();

    project.setName(Name.of("a project after change"));
    project.setActive(false);

    var projectJson = JsonTestUtil.asJsonString(project);

    logger.debug("put data: " + projectJson);

    mvc.perform(put(ProjectResource.PATH_PROJECT + project.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(projectJson))
              .andExpect(status().is4xxClientError());
  }
  
  
  @Test
  @Transactional
  @WithMockUser(username = AppUserNames.tlead1, 
                roles = { UserRoleName.PROJECTLEAD, UserRoleName.USER })  
  public void testCreateProject() throws Exception {

    var projectLead = teamMemberRepository
                        .findByUserId(Name.of(AppUserNames.tlead1))
                        .orElseThrow();
        
    var project = Project.buildForTest(0, "a new project");
    project.setActive(true);
    project.addProjectLead(projectLead);

    var projectJson = JsonTestUtil.asJsonString(project);

    logger.debug("posted data: " + projectJson);

    mvc.perform(post(ProjectResource.PATH_PROJECT)
                .contentType(MediaType.APPLICATION_JSON)
                .content(projectJson))
        .andDo(print())
        .andExpect(status().isCreated())
        .andExpect(jsonPath("$.id", org.hamcrest.Matchers.is(notNullValue())))
        .andExpect(jsonPath("$.name", org.hamcrest.Matchers.is(project.getName().toString())))
        .andExpect(jsonPath("$.projectLeads[0].id", 
             org.hamcrest.Matchers.is(projectLead.getId().intValue())))
        .andDo(document(ProjectResource.PATH_PROJECT + "id/POST"));
  }
  
  @Test
  @Transactional
  @WithMockUser(username = AppUserNames.tlead1, 
                roles = { UserRoleName.PROJECTLEAD, UserRoleName.USER })  
  public void testCreateProjectWithCorruptDataNok() throws Exception {

  
    var projectJson = "{\"content\": \"totally corrupt project json\"";

    logger.debug("posted data: " + projectJson);

    mvc.perform(post(ProjectResource.PATH_PROJECT)
                .contentType(MediaType.APPLICATION_JSON)
                .content(projectJson))
        .andDo(print())
        .andExpect(status().is4xxClientError());
  }  

  
  @Test
  @WithMockUser(username = AppUserNames.admin, 
                roles = { UserRoleName.USER, UserRoleName.ADMIN })  
  public void testGetProjectTasks() throws Exception {

    Long projectId = Long.valueOf(-1);
    mvc.perform(get(ProjectResource.PATH_PROJECT + projectId + "/task"))
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(jsonPath("$[0].id", org.hamcrest.Matchers.is(-4)))
        .andExpect(jsonPath("$[0].name", org.hamcrest.Matchers.is("a done task")))
        .andDo(document(ProjectResource.PATH_PROJECT + "id/task/GET"));
  }
  
  
  @Test
  @WithMockUser(username = AppUserNames.user, 
                roles = { UserRoleName.USER })  
  public void testGetProjectTasksWithInsufficientAccessNok() throws Exception {

    Long projectId = Long.valueOf(-1);
    mvc.perform(get(ProjectResource.PATH_PROJECT + projectId + "/task"))
        .andExpect(status().is4xxClientError());
  }
}
