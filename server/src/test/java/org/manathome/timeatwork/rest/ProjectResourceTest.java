package org.manathome.timeatwork.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Arrays;
import java.util.Optional;

import javax.transaction.Transactional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import org.manathome.timeatwork.db.repository.ProjectRepository;
import org.manathome.timeatwork.db.repository.TeamMemberRepository;
import org.manathome.timeatwork.domain.ProjectTest;
import org.manathome.timeatwork.domain.values.Name;
import org.manathome.timeatwork.security.AppUserNames;
import org.manathome.timeatwork.security.UserRoleName;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
@Transactional
public class ProjectResourceTest {

  @Autowired
  private MockMvc mvc;

  @Autowired
  private TeamMemberRepository teamMemberRepository;

  @MockBean
  private ProjectRepository projectRepository;

  @Test
  @WithMockUser(username = AppUserNames.tlead1, 
                roles = { UserRoleName.USER, UserRoleName.PROJECTLEAD })    
  public void testProjects() throws Exception {

    final var projectLead = teamMemberRepository.findByUserId(Name.of(AppUserNames.tlead1)).get();
    
    final var project = ProjectTest.buildTestProject("test project 1", null);
    project.addProjectLead(projectLead);

    given(this.projectRepository.findAll()).willReturn(Arrays.asList(project));

    mvc.perform(get(ProjectResource.PATH_PROJECT))
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(jsonPath("$[0].name", org.hamcrest.Matchers.is("test project 1")))
        .andExpect(jsonPath("$[0].projectLeads[0].userId", 
                   org.hamcrest.Matchers.is(AppUserNames.tlead1)));
  }

  
  @Test
  @WithMockUser(username = AppUserNames.admin, 
                roles = { UserRoleName.ADMIN, UserRoleName.USER })      
  public void testProjectById() throws Exception {

    var project = ProjectTest.buildTestProject("test project 3",  AppUserNames.admin);
    assertThat(project).isNotNull();

    Long newId = Long.valueOf(project.getId());
    assertThat(newId).as("newId").isNotNull();

    given(this.projectRepository.findById(eq(newId))).willReturn(Optional.of(project));

    mvc.perform(get(ProjectResource.PATH_PROJECT + project.getId()))
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.name", org.hamcrest.Matchers.is("test project 3")))
        .andExpect(jsonPath("$.projectLeads[0].userId", 
                   org.hamcrest.Matchers.is(AppUserNames.admin)));
  }

}
