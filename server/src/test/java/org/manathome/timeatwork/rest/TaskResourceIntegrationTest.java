package org.manathome.timeatwork.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.CoreMatchers.is;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.time.LocalDate;

import javax.persistence.EntityManager;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.manathome.timeatwork.db.repository.TaskAssignmentRepository;
import org.manathome.timeatwork.db.repository.TaskRepository;
import org.manathome.timeatwork.domain.Task;
import org.manathome.timeatwork.domain.TaskAssignment;
import org.manathome.timeatwork.domain.TaskState;
import org.manathome.timeatwork.domain.values.Name;
import org.manathome.timeatwork.security.AppUserNames;
import org.manathome.timeatwork.security.UserRoleName;
import org.manathome.timeatwork.test.annotation.Documentation;
import org.manathome.timeatwork.util.JsonHelper;
import org.manathome.timeatwork.util.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;


@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
@Documentation
@WithMockUser(username = AppUserNames.admin, 
              roles = { UserRoleName.ADMIN, UserRoleName.USER }) 
public class TaskResourceIntegrationTest {

  private static final Logger logger = Logger.getLogger(TaskResourceIntegrationTest.class);

  @Autowired
  private MockMvc mvc;

  @Autowired
  private TaskRepository taskRepository;

  @Autowired
  private TaskAssignmentRepository taskAssignmentRepository;

  @Autowired
  private EntityManager em;

  @Test
  public void testGetTask() throws Exception {

    Long taskId = Long.valueOf(-1);
    mvc.perform(get(TaskResource.PATH_TASK + taskId)).andDo(print()).andExpect(status().isOk())
        .andExpect(jsonPath("$.id", org.hamcrest.Matchers.is(-1)))
        .andExpect(jsonPath("$.name", org.hamcrest.Matchers.is("a first task")))
        .andDo(document(TaskResource.PATH_TASK + TaskResource.PARAM_TASK_ID + "/GET"));
  }

  @Test
  @Transactional
  public void testDeleteTask() throws Exception {

    var task = Task.buildForTest("dummy-task");
    task = taskRepository.save(task);
    em.flush();

    var newTask = taskRepository.findById(task.getId());
    assertThat(newTask.isPresent()).as("new task").isTrue();

    logger.debug("deleting task " + task.getId());

    Long taskId = task.getId();
    mvc.perform(delete(TaskResource.PATH_TASK + taskId)).andDo(print()).andExpect(status().isOk())
        .andDo(document(TaskResource.PATH_TASK + TaskResource.PARAM_TASK_ID + "DELETE"));

    var removedTask = taskRepository.findById(task.getId());
    assertThat(removedTask.isPresent()).as("deleted task").isFalse();
  }

  @Test
  @Transactional
  public void testChangeTask() throws Exception {

    var task = Task.buildForTest("dummy-task2");
    task.setState(TaskState.ACTIVE);
    task = taskRepository.save(task);
    em.flush();

    task.setState(TaskState.DONE);
    task.setName(Name.of("another changed name"));
    // not persisted..

    mvc.perform(put(TaskResource.PATH_TASK + task.getId()).contentType(MediaType.APPLICATION_JSON)
        .content(JsonHelper.asJsonString(task))).andDo(print()).andExpect(status().isOk())
        .andDo(document(TaskResource.PATH_TASK + TaskResource.PARAM_TASK_ID + "/PUT"));
    ;

    var changedTask = taskRepository.findById(task.getId());
    assertThat(changedTask.isPresent()).as("task").isTrue();
    assertThat(changedTask.get().getName()).isEqualTo(task.getName());
    assertThat(changedTask.get().getState()).isEqualTo(TaskState.DONE);
  }

  @Test
  @Transactional
  @WithMockUser(username = AppUserNames.admin, 
                roles = { UserRoleName.ADMIN, UserRoleName.USER })   
  public void testCreateTask() throws Exception {

    var task = Task.buildForTest("dummy-task3");
    task.setState(TaskState.ACTIVE);

    var result = mvc
        .perform(post(ProjectResource.PATH_PROJECT + task.getProject().getId() + "/task")
            .contentType(MediaType.APPLICATION_JSON).content(JsonHelper.asJsonString(task)))
        .andDo(print())
        .andExpect(status().isCreated())
        .andExpect(jsonPath("$.name", is(task.getName().toString())))
        .andDo(document(ProjectResource.PATH_PROJECT + "id/task/POST")).andReturn();

    var json = result.getResponse().getContentAsString();
    var createdTask = new ObjectMapper().readValue(json, Task.class);

    assertThat(createdTask.getId()).isGreaterThan(0);
    assertThat(taskRepository.findById(createdTask.getId()).isPresent()).isTrue();
  }

  @Test
  public void testGetTaskAssignments() throws Exception {

    String testYearString = LocalDate.now().getYear() + "-";  // may break on newyear :-)
    
    logger.debug(testYearString);
    
    Long taskId = Long.valueOf(-1);
    mvc.perform(get(TaskResource.PATH_TASK + taskId + "/assignment"))
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(jsonPath("$[0].id", org.hamcrest.Matchers.is(-3)))
        .andExpect(jsonPath("$[0].teamMember.userId", org.hamcrest.Matchers.is(AppUserNames.user)))
        .andExpect(jsonPath("$[0].from", 
            org.hamcrest.Matchers.containsString(testYearString))) 
        .andDo(document("rest/task/id/assignment/GET"));
  }

  @Test
  @Transactional
  public void testDeleteTaskAssignment() throws Exception {

    var task = taskRepository.save(Task.buildForTest("dummy-task-with-assignment"));
    var assignment = taskAssignmentRepository.save(TaskAssignment.buildForTest(-1, task.getId()));
    
    em.flush();
    em.detach(assignment);

    var assignmentToDelete = taskAssignmentRepository.findById(assignment.getId());
    assertThat(assignmentToDelete.isPresent()).as("assignment stored").isTrue();

    mvc.perform(delete(TaskResource.PATH_TASK + task.getId() 
                + "/assignment/" + assignment.getId()))
        .andDo(print())
        .andExpect(status().isOk())
        .andDo(document(TaskResource.PATH_TASK + "id/assignment/id/DELETE"));

    assignmentToDelete = taskAssignmentRepository.findById(assignment.getId());
    assertThat(assignmentToDelete.isEmpty()).as("assignment deleted").isTrue();
  }

  @Test
  @Transactional
  public void testChangeTaskAssignment() throws Exception {

    LocalDate baseDate = LocalDate.now().minusDays(33);

    var task = taskRepository.save(Task.buildForTest("dummy-task-with-assignment"));
    var assignment = taskAssignmentRepository.save(TaskAssignment.buildForTest(-1, task.getId()));
    em.flush();

    em.detach(assignment);

    assignment.setAssignmentRange(baseDate, baseDate.plusDays(66));

    mvc.perform(put(TaskResource.PATH_TASK + task.getId() + "/assignment/" + assignment.getId())
        .contentType(MediaType.APPLICATION_JSON)
        .content(JsonHelper.asJsonString(assignment)))
        .andDo(print())
        .andExpect(status().isOk())
        .andDo(document(TaskResource.PATH_TASK + "id/assignment/id/PUT"));

    var changedAssignment = taskAssignmentRepository.findById(assignment.getId());
    assertThat(changedAssignment.isPresent()).as("assignment").isTrue();
    assertThat(changedAssignment.get().getFrom()).isEqualTo(baseDate);
    assertThat(changedAssignment.get().getUntil()).isEqualTo(baseDate.plusDays(66));
  }

  @Test
  @Transactional
  public void testCreateTaskAssignment() throws Exception {

    LocalDate baseDate = LocalDate.now().minusDays(33);

    var task = taskRepository.save(Task.buildForTest("dummy-task-with-new-assignment"));
    em.flush();

    var assignment = TaskAssignment.buildForTest(-1, task.getId());
    assignment.setAssignmentRange(baseDate, baseDate.plusDays(66));

    var result = mvc
        .perform(post(TaskResource.PATH_TASK + task.getId() + "/assignment/")
        .contentType(MediaType.APPLICATION_JSON)
        .content(JsonHelper.asJsonString(assignment)))
        .andDo(print())
        .andExpect(status().isCreated())
        .andDo(document(TaskResource.PATH_TASK + "id/assignment/id/POST"))
        .andReturn();

    assertThat(result.getResponse().isCommitted()).as("done").isTrue();
  }

}
