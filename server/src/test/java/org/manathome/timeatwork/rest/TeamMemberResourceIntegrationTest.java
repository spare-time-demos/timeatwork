package org.manathome.timeatwork.rest;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.manathome.timeatwork.security.UserRoleName;
import org.manathome.timeatwork.test.annotation.Documentation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
@Documentation
@WithMockUser(username = "mocklead", roles = { UserRoleName.PROJECTLEAD, UserRoleName.USER })  
public class TeamMemberResourceIntegrationTest {

  @Autowired
  private MockMvc mvc;

  @Test
  public void testGetTeamMembers() throws Exception {

    mvc.perform(get(TeamMemberResource.PATH_TEAMMEMBER))
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(jsonPath("$[0].id", org.hamcrest.Matchers.is(-1)))
        .andExpect(jsonPath("$[0].name", org.hamcrest.Matchers.is("a first test lead and user")))
        .andDo(document(TeamMemberResource.PATH_TEAMMEMBER + "/GET"));
    ;
  }

}
