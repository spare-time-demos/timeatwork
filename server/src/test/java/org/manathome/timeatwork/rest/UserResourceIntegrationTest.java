package org.manathome.timeatwork.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import javax.transaction.Transactional;

import org.junit.jupiter.api.Test;
import org.manathome.timeatwork.db.repository.TeamMemberRepository;
import org.manathome.timeatwork.domain.values.Name;
import org.manathome.timeatwork.dto.CUser;
import org.manathome.timeatwork.security.AppUser;
import org.manathome.timeatwork.security.AppUserDetailsService;
import org.manathome.timeatwork.security.AuthenticationTestHelperBase;
import org.manathome.timeatwork.security.UserRoleName;
import org.manathome.timeatwork.test.annotation.Documentation;
import org.manathome.timeatwork.util.JsonHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;
import org.springframework.test.annotation.DirtiesContext.MethodMode;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
@AutoConfigureMockMvc
@Documentation
@WithMockUser(username = "mockadmin", roles = { UserRoleName.ADMIN, UserRoleName.USER })  
@Transactional
@DirtiesContext(classMode = ClassMode.AFTER_CLASS)
public class UserResourceIntegrationTest extends AuthenticationTestHelperBase {

  @Autowired
  private MockMvc mvc;
  
  @Autowired
  private TeamMemberRepository teamMemberRepository;

  @Autowired
  private AppUserDetailsService userDetailsService;

  @Test
  public void testGetUsers() throws Exception {

    mvc.perform(get(UserResource.PATH_USER))
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(jsonPath("$[0].id", org.hamcrest.Matchers.is(-1)))
        .andExpect(jsonPath("$[0].name", org.hamcrest.Matchers.is("a first test lead and user")))
        .andDo(document(UserResource.PATH_USER + "/GET"));
    ;
  }

  @Test
  public void testGetUserById() throws Exception {

    mvc.perform(get(UserResource.PATH_USER + "/-1"))
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.id", org.hamcrest.Matchers.is(-1)))
        .andExpect(jsonPath("$.name", org.hamcrest.Matchers.is("a first test lead and user")))
        .andDo(document(UserResource.PATH_USER + "/id/GET"));
    ;
  }

  @Test
  public void testCreateUser() throws Exception {
    final String new_user_id = "u999";
    final var newUser = CUser.buildTestuser(new_user_id, "newUser");

    mvc
        .perform(post(UserResource.PATH_USER)
            .contentType(MediaType.APPLICATION_JSON).content(JsonHelper.asJsonString(newUser)))    
        .andDo(print())
        .andExpect(status().isCreated())
        .andExpect(jsonPath("$.name", org.hamcrest.Matchers.is("newUser")))
        .andExpect(jsonPath("$.userId", org.hamcrest.Matchers.is(new_user_id)))
        .andExpect(jsonPath("$.active", org.hamcrest.Matchers.is(true)))
        .andDo(document(UserResource.PATH_USER + "/POST"));    
    
    final var tm = teamMemberRepository.findByUserId(Name.of(new_user_id)).orElseThrow();
    assertThat(tm.getName()).isEqualTo(Name.of("newUser"));
  }

  @Test
  @DirtiesContext(methodMode = MethodMode.AFTER_METHOD)
  public void testChangeUser() throws Exception {

    // todo: change a property and test for it..
    
    final var tm = teamMemberRepository.findByUserId(Name.of(TEST_USER_NAME)).orElseThrow();    
    final var changedUser = new CUser(tm);
    
    final String newName = changedUser.getName().endsWith("-CHNGX-") 
        ? changedUser.getName().replace("-CHNGX-", "-CHNGY-") 
        : changedUser.getName() + "-CHNGX-";
        
    changedUser.setName(newName);

    mvc
        .perform(put(UserResource.PATH_USER + "/" + changedUser.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .content(JsonHelper.asJsonString(changedUser)))    
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.name", org.hamcrest.Matchers.is(newName)))
        .andDo(document(UserResource.PATH_USER + "/PUT"));    
  }
  
  @Test
  @Transactional
  @DirtiesContext(methodMode = MethodMode.AFTER_METHOD)
  public void testResetUserPassword() throws Exception {

    final var tm = teamMemberRepository.findByUserId(Name.of(TEST_USER_NAME)).orElseThrow();    
    assertThat(userDetailsService.validatePassword(new AppUser(tm), TEST_USER_PASSWORD))
      .isTrue();

    mvc
        .perform(put(UserResource.PATH_USER + "/" + tm.getId() + "/password")
            .contentType(MediaType.APPLICATION_JSON)
            .content(""))    
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.message", org.hamcrest.Matchers.containsString("new password is:")))
        .andDo(document(UserResource.PATH_USER + "/password/POST"));
    
    
    final var changedTm = teamMemberRepository.findByUserId(Name.of(TEST_USER_NAME)).orElseThrow();
    assertThat(userDetailsService.validatePassword(new AppUser(changedTm), TEST_USER_PASSWORD))
      .isFalse();
  }  

}
