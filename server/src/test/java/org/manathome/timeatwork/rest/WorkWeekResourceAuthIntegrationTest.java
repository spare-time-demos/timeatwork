package org.manathome.timeatwork.rest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.manathome.timeatwork.security.AuthenticationTestHelperBase;
import org.manathome.timeatwork.security.JwtTokenProcessor;
import org.manathome.timeatwork.test.annotation.IntegrationTest;
import org.manathome.timeatwork.util.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
@AutoConfigureMockMvc
@IntegrationTest
public class WorkWeekResourceAuthIntegrationTest extends AuthenticationTestHelperBase {

  private static final Logger logger = Logger.getLogger(WorkWeekResourceAuthIntegrationTest.class);

  @Autowired
  private MockMvc mvc;


  /** NOT WORKING AT THE MOMENT. */
  @Test
  @Disabled
  public void testGetWorkWeekAsUser() throws Exception {

    var startOfWeekDate = LocalDate.now().plusDays(-5).toString(); // calc todo start of week

    final String bearerToken = authenticatedTokenForTestUser(
        mvc, 
        AuthenticationTestHelperBase.TEST_USER_NAME);    
    
    logger.debug("call " 
        + WorkWeekResource.PATH_WORKWEEK  
        + " as " + AuthenticationTestHelperBase.TEST_USER_NAME);
    
    mvc.perform(
        get(WorkWeekResource.PATH_WORKWEEK + startOfWeekDate)
       .header(JwtTokenProcessor.TOKEN_HEADER_AUTHORIZATION, bearerToken)
        )
        .andDo(print())
        .andExpect(status().isOk());
  }
}
