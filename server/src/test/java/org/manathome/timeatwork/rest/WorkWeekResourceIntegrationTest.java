package org.manathome.timeatwork.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.time.LocalDate;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;

import org.junit.jupiter.api.Test;
import org.manathome.timeatwork.db.repository.TeamMemberRepository;
import org.manathome.timeatwork.db.repository.WorkDoneRepository;
import org.manathome.timeatwork.domain.values.Name;
import org.manathome.timeatwork.security.AppUserNames;
import org.manathome.timeatwork.security.UserRoleName;
import org.manathome.timeatwork.service.WorkWeekService;
import org.manathome.timeatwork.test.annotation.Documentation;
import org.manathome.timeatwork.util.JsonHelper;
import org.manathome.timeatwork.util.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

@SpringBootTest
@AutoConfigureMockMvc
@Documentation
public class WorkWeekResourceIntegrationTest {

  private static final Logger logger = Logger.getLogger(WorkWeekResourceIntegrationTest.class);

  @Autowired
  private MockMvc mvc;

  @Autowired
  private WorkWeekService workWeekService;
  
  @Autowired
  private WorkDoneRepository workDoneRepository;
  
  @Autowired
  private TeamMemberRepository teamMemberRepository;

  @Autowired
  private EntityManager em;

  @Test
  @WithMockUser(username = AppUserNames.user, roles = { UserRoleName.USER })  
  public void testGetWorkWeek() throws Exception {

    var startOfWeekDate = LocalDate.now().plusDays(-5).toString(); // calc todo start of week

    mvc.perform(get(WorkWeekResource.PATH_WORKWEEK + startOfWeekDate))
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.from", org.hamcrest.Matchers.is(startOfWeekDate)))
        .andExpect(jsonPath("$.until", org.hamcrest.Matchers.containsString("-")))
        .andDo(document(WorkWeekResource.PATH_WORKWEEK 
            + WorkWeekResource.FROM_ISO_DATE_STRING + "/GET"));
    ;
  }

  @Test
  @Transactional
  @WithMockUser(username = AppUserNames.user, roles = { UserRoleName.USER })  
  public void testChangeWorkWeek() throws Exception {

    var startOfWeekDate = LocalDate.now().plusDays(-5); // calc todo start of week

    final var ww = workWeekService.getWorkWeek(startOfWeekDate);
    assertThat(ww.getTaskWeeks().length).isGreaterThan(1);

    final var newWorkHoursAssinged00       = ww.getTaskWeeks()[0]
                                               .getWorkHours()[0]
                                               .addTo(2); // first task, first day
    
    ww.getTaskWeeks()[0].getWorkHours()[0] = newWorkHoursAssinged00;
    
    final var newWorkHoursAssinged03       = ww.getTaskWeeks()[0]
                                               .getWorkHours()[3]
                                               .addTo(1); // first task, fourth day
    
    ww.getTaskWeeks()[0].getWorkHours()[3] = newWorkHoursAssinged03; 

    var firstDayWorkHours = ww.getTaskWeeks()[0].getWorkHours()[0].getWorkHours();
    assertThat(firstDayWorkHours).isGreaterThan(1);

    var workWeekJson = JsonHelper.asJsonString(ww);

    logger.debug("post data: " + workWeekJson);

    mvc.perform(put(WorkWeekResource.PATH_WORKWEEK + startOfWeekDate)
        .contentType(MediaType.APPLICATION_JSON)
        .content(workWeekJson))
        .andDo(print())
        .andExpect(status().isOk())
        .andDo(document(WorkWeekResource.PATH_WORKWEEK 
            + WorkWeekResource.FROM_ISO_DATE_STRING + "/PUT"));
    ;

    em.flush();
    
    // read changed values back from database:    
    
    final var tm = teamMemberRepository.findByUserId(Name.of(AppUserNames.user)).get();
    final var workDone = workDoneRepository
        .findWorkDoneByMember(
          tm, 
          startOfWeekDate, 
          startOfWeekDate.plusDays(7))
        .collect(Collectors.toList());
    
    
    assertThat(workDone
        .stream()
        .filter(wd -> wd.getTaskId().equals(ww.getTaskWeeks()[0].getTaskId()) 
                   && wd.getDate().isEqual(startOfWeekDate))
        .findFirst()
        .get()
        .getWorkHours()
        .asHours())
        .as("work 0 day hour of " + ww.getTaskWeeks()[0].getTaskName())
        .isEqualTo(newWorkHoursAssinged00.getWorkHours());
   
    assertThat(workDone
        .stream()
        .filter(wd -> wd.getTaskId().equals(ww.getTaskWeeks()[0].getTaskId()) 
                   && wd.getDate().isEqual(startOfWeekDate.plusDays(3)))
        .findFirst()
        .get()
        .getWorkHours()
        .asHours())
        .as("work 3 day hour of " + ww.getTaskWeeks()[0].getTaskName())
        .isEqualTo(newWorkHoursAssinged03.getWorkHours());    
  }

}
