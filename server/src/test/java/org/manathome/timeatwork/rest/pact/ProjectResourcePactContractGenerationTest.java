package org.manathome.timeatwork.rest.pact;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.manathome.timeatwork.security.AppUserNames;
import org.manathome.timeatwork.security.UserRoleName;
import org.manathome.timeatwork.test.annotation.Documentation;
import org.manathome.timeatwork.util.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.wiremock.restdocs.SpringCloudContractRestDocs;
import org.springframework.cloud.contract.wiremock.restdocs.WireMockRestDocs;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
@AutoConfigureMockMvc
@Documentation
@WithMockUser(username = AppUserNames.tlead1, 
              roles = { UserRoleName.USER, UserRoleName.PROJECTLEAD })  
@Tag("pact")
public class ProjectResourcePactContractGenerationTest {

  private static final Logger logger = Logger
      .getLogger(ProjectResourcePactContractGenerationTest.class);

  @Autowired
  private MockMvc mvc;

  @SuppressWarnings("deprecation")
  @Test
  public void generateContractsAndWiremockForRestGetProjects() throws Exception {

    logger.debug("create pac and mock");

    mvc.perform(get("/rest/project/"))
       .andDo(print())
       .andExpect(status().isOk())
       .andExpect(jsonPath("$[0].id", org.hamcrest.Matchers.is(-3)))
       .andDo(WireMockRestDocs.verify().stub("mock") // generate wiremock
        )
       .andDo(document("rest/project/GET", SpringCloudContractRestDocs.dslContract()));
  }

}
