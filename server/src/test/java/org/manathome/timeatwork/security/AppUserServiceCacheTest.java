package org.manathome.timeatwork.security;

import static org.assertj.core.api.Assertions.assertThat;

import javax.cache.CacheManager;
import javax.transaction.Transactional;

import org.junit.jupiter.api.Test;
import org.manathome.timeatwork.util.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.ClassMode;

@SpringBootTest
@DirtiesContext(classMode = ClassMode.BEFORE_CLASS)
public class AppUserServiceCacheTest {
  
  private static final Logger logger = Logger.getLogger(AppUserServiceCacheTest.class);

  @Autowired
  AppUserDetailsService userDetailsService;
  
  @Autowired
  CacheManager cacheManager;

  @Test
  public void testCacheManagerExistence() {

    assertThat(this.cacheManager).isNotNull();
    assertThat(this.cacheManager.getCache(AppUserDetailsService.TEAM_MEMBER_CACHE))
      .as("cache config")
      .isNotNull();              
  }    
  
  @Test
  public void testAdminUserDetailsFirstReadPutsIntoCache() {

    // reset cache
    this.cacheManager.getCache(AppUserDetailsService.TEAM_MEMBER_CACHE)
                     .clear();

    assertThat(this.cacheManager.getCache(AppUserDetailsService.TEAM_MEMBER_CACHE)
        .containsKey(AppUserNames.admin))
        .as("empty cache after before first call")
        .isFalse();
    
    // use db and fill cache
    // ---------------------
    double currentLoadCount = userDetailsService.getUserLoadCount();
    
    final var admin = userDetailsService.findByUserIdCached(AppUserNames.admin);
    
    assertThat(userDetailsService.getUserLoadCount())
       .as("load metric should increase")
       .isGreaterThan(currentLoadCount);

    assertThat(admin.getUserId().toString()).isEqualToIgnoringCase(AppUserNames.admin);
    
    assertThat(this.cacheManager.getCache(AppUserDetailsService.TEAM_MEMBER_CACHE)
                                .containsKey(AppUserNames.admin))
      .as("value in cache after first call")
      .isTrue();
    
    userDetailsService.findByUserIdCached(AppUserNames.admin);
  }

  @Test
  public void testLoadAdminUserDetailsUsingCached() {

    userDetailsService.findByUserIdCached(AppUserNames.admin); // put into cache.
    
    double currentLoadCount = userDetailsService.getUserLoadCount();
    logger.trace("current load count " + currentLoadCount);
    
    // should use cached admin tm
    // --------------------------
    logger.debug("now access to cached value of admin user..");
    userDetailsService.findByUserIdCached(AppUserNames.admin);
    userDetailsService.findByUserIdCached(AppUserNames.admin);
    userDetailsService.findByUserIdCached(AppUserNames.admin);
    userDetailsService.findByUserIdCached(AppUserNames.admin);
    
    assertThat(userDetailsService.getUserLoadCount())
        .as("no db loads happend on cached user access.")
        .isEqualTo(currentLoadCount);
  }
  
    
  @Test  
  @WithMockUser(username = AppUserNames.user, roles = { UserRoleName.USER })
  public void testCurrentTeamMemberUseFromCache() {
    
    final var user = userDetailsService.getCurrentTeamMember(); // ensure cache filled.
    assertThat(user).isNotNull();
    
    final var countBefore = userDetailsService.getUserLoadCount();
    
    userDetailsService.getCurrentTeamMember();
    userDetailsService.getCurrentTeamMember();
    userDetailsService.getCurrentTeamMember();
    
    assertThat(userDetailsService.getUserLoadCount())
       .as("no db access for multiply call to cached team member")
       .isLessThanOrEqualTo(countBefore);
  }
  
  
  @Test  
  @WithMockUser(username = AppUserNames.user, roles = { UserRoleName.USER })
  @Transactional
  public void testChangedUserGetsEvictedFromCache() {
    
    final var user = userDetailsService.getCurrentTeamMember(); // ensure cache filled.    
    user.setActive(true);
    userDetailsService.saveUser(user); // should evict cache.
    
    final var countBefore = userDetailsService.getUserLoadCount();

    final var rereadUser = userDetailsService.getCurrentTeamMember(); // should hit db.
    assertThat(rereadUser).isNotNull();

    assertThat(userDetailsService.getUserLoadCount())
       .as("no db access for multiply call to cached team member")
       .isGreaterThan(countBefore);
  }  
  

}
