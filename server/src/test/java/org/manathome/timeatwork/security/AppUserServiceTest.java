package org.manathome.timeatwork.security;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;
import org.manathome.timeatwork.domain.values.Name;
import org.manathome.timeatwork.util.assertions.Require;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.cache.CacheType;
import org.springframework.boot.test.autoconfigure.core.AutoConfigureCache;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.test.context.support.WithMockUser;

@SpringBootTest
@AutoConfigureCache(cacheProvider = CacheType.NONE)
public class AppUserServiceTest {

  @Autowired
  AppUserDetailsService userDetailsService;
  
  @Test
  public void testLoadAdminUserDetails() {
    var admin = Require.notNull(userDetailsService, "userDetailsService")
        .loadUserByUsername(AppUserNames.admin);
    
    assertThat(admin).isNotNull();
    assertThat(admin.getUsername()).isEqualToIgnoringCase("admin");
    assertThat(admin.isCredentialsNonExpired()).as("non expired").isTrue();
    assertThat(admin.isAccountNonLocked()).as("non locked").isTrue();
  }
    
  
  @Test
  public void testLoadAdminUserDetailsWithRole() {
    var admin = Require.notNull(userDetailsService, "userDetailsService")
        .loadUserByUsername(AppUserNames.admin);
    
    assertThat(admin
        .getAuthorities().stream()
        .anyMatch(ga -> UserRoleName.isSameRole(UserRoleName.ADMIN, ga.getAuthority()))
        ).isTrue();
  }
    

  @Test
  public void testLoadUnknownUserDetails() {
    assertThrows(UsernameNotFoundException.class, () -> {
      userDetailsService.loadUserByUsername("not-a-known-user");
    });
  }
  
  
  @Test
  public void testPasswordEncryptionOk() {
    
    final String pw = "blaBLA3-_//?!=+*,1234567 890";
    
    final var admin = ((AppUser) 
        userDetailsService.loadUserByUsername(AppUserNames.admin));
    
    final var encPw = userDetailsService.encryptPassword(pw);
    admin.changePassword(encPw);
    
    assertThat(userDetailsService.validatePassword(admin, pw)).isTrue();
  }
  

  @Test
  public void testPasswordEncryptionWithWrongPasswordEntry() {
    
    final String pw = "blaBLA3-_//?!=+*,1234567 890";
    
    final var admin = ((AppUser) 
        Require.notNull(userDetailsService, "userDetailsService")
        .loadUserByUsername(AppUserNames.admin));
    
    final var encPw = userDetailsService.encryptPassword(pw);
    admin.changePassword(encPw);
    
    assertThat(userDetailsService.validatePassword(admin, "wrong Pw")).isFalse();
  }

  
  @Test
  public void testPasswordEncryptionWithWrongStoredPassword() {
    
    final String pw = "blabla3//?!=+*,1234567 890";
    
    final var admin = ((AppUser) 
        Require.notNull(userDetailsService, "userDetailsService")
        .loadUserByUsername(AppUserNames.admin));
    
    final var encPw = userDetailsService.encryptPassword(pw);
    admin.changePassword(encPw + "wrong");
    
    assertThat(userDetailsService.validatePassword(admin, pw)).isFalse();
  }
  
  
  @Test  
  @WithMockUser(username = AppUserNames.user, roles = { UserRoleName.USER })
  public void testCurrentTeamMemberUser() {
    final var user = userDetailsService.getCurrentTeamMember();
    assertThat(user.getUserId()).isEqualTo(Name.of(AppUserNames.user));
  }

  
  @Test  
  @WithMockUser(username = AppUserNames.tlead1, 
                roles = { UserRoleName.USER, UserRoleName.PROJECTLEAD })
  public void testCurrentTeamMemberLead() {
    final var user = userDetailsService.getCurrentTeamMember();
    assertThat(user.getUserId()).isEqualTo(Name.of(AppUserNames.tlead1));
  }

  
  @Test  
  public void testCurrentTeamMemberNotLoggedIn_Nok() {

    assertThatThrownBy(() -> {
      userDetailsService.getCurrentTeamMember();
    });
    
  }

  
}
