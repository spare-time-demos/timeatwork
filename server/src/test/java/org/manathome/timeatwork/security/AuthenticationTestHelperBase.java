package org.manathome.timeatwork.security;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.manathome.timeatwork.rest.LoginResource;
import org.manathome.timeatwork.util.JsonTestUtil;
import org.manathome.timeatwork.util.Logger;
import org.manathome.timeatwork.util.TimeAtWorkInternalTestException;
import org.manathome.timeatwork.util.assertions.Require;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

/** authenticate an retrieve jwt token within spring tests. */
public abstract class AuthenticationTestHelperBase {

  private static final Logger logger = Logger.getLogger(AuthenticationTestHelperBase.class);

  public static final String TEST_USER_NAME = AppUserNames.user;
  public static final String TEST_ADMIN_NAME = AppUserNames.admin;
  public static final String TEST_PROJECTLEAD_NAME = AppUserNames.tlead1;
  
  public static final String TEST_USER_PASSWORD = AppUserNames.testPassword;
  
  protected String authenticatedTokenForTestUser(final MockMvc mvc, final String userName) {
    
    logger.trace("login mock mvc with user " + userName);
    
    try {
      
      var authenticationRequest = new AuthenticationRequest(userName, TEST_USER_PASSWORD);
      var loginJson = JsonTestUtil.asJsonString(authenticationRequest);
      
      final var result = mvc
          .perform(post(LoginResource.URL_AUTHENTICATION)
            .contentType(MediaType.APPLICATION_JSON)
            .content(loginJson))
          // .andDo(print())          
          .andExpect(status().isOk())
          .andExpect(jsonPath("$.ok", is(true)))          
          .andReturn();
      
      final String bearerToken = result
          .getResponse()
          .getHeader(JwtTokenProcessor.TOKEN_HEADER_AUTHORIZATION);
      
      Require.notNullOrEmptyWhitespace(bearerToken, "returned bearerToken");
      Require.isTrue(
            bearerToken.startsWith(JwtTokenProcessor.TOKEN_PREFIX_AUTHORIZATION), 
            "bearer token should begin with " 
            + JwtTokenProcessor.TOKEN_PREFIX_AUTHORIZATION);
      
      return bearerToken;

    } catch (Exception e) {
      throw new TimeAtWorkInternalTestException(
          "test authentication as " + userName + " failed with: " + e.getMessage(), 
          e);
    }
    
  }

}
