package org.manathome.timeatwork.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.api.Assertions.fail;

import java.util.Locale;

import org.junit.jupiter.api.Test;
import org.manathome.timeatwork.security.AppUserNames;
import org.manathome.timeatwork.security.UserRoleName;
import org.manathome.timeatwork.util.assertions.ValidationFailedException;

import org.mockito.Mockito;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;

@SpringBootTest
@WithMockUser(username = AppUserNames.user, roles = { UserRoleName.USER })
public class LocalizedValidationTest {

  @Autowired
  public LocalizedValidation validation;

  @Test
  public void testGetMessage() {

    final String msg = validation.getMessage("user.name.required");
    assertThat(msg).isNotBlank();
    assertThat(msg).contains("USRN1: user name is required.");

  }

  @Test
  public void testGetMessageWithGermanLocale() {

    LocalizedValidation lv = Mockito.spy(validation);
    Mockito.doReturn(Locale.GERMAN).when(lv).getLocale();

    final String msg = lv.getMessage("user.name.required");
    assertThat(msg).isNotBlank();
    assertThat(msg).contains("USRN1: Ein Benutzername ist notwendig.");

  }

  @Test
  public void testGetMessageWithExplicitEnglishLocale() {

    LocalizedValidation lv = Mockito.spy(validation);
    Mockito.doReturn(Locale.ENGLISH).when(lv).getLocale();

    final String msg = lv.getMessage("user.name.required");
    assertThat(msg).isNotBlank();
    assertThat(msg).contains("USRN1: user name is required.");

  }

  @Test
  public void testValidateNotNullOrEmptyWhitespaceWithGermanLocale() {

    LocalizedValidation lv = Mockito.spy(validation);
    Mockito.doReturn(Locale.GERMAN).when(lv).getLocale();

    final String s = " ";
    assertThatExceptionOfType(ValidationFailedException.class).isThrownBy(() -> {
      lv.validateNotNullOrEmptyWhitespace(s, "task.name.required");
    });

  }
  
  @Test
  public void testValidateTrueWithGermanLocale() {

    LocalizedValidation lv = Mockito.spy(validation);
    Mockito.doReturn(Locale.GERMAN).when(lv).getLocale();

    try {
      lv.validateIsTrue(false, "project.lead.not.you");
      fail("validation vailed");
    } catch (ValidationFailedException vfe) {
      assertThat(vfe.getMessage()).contains("PROJ3: Projekt nicht im Zugriff.");
    }

  }  

}
