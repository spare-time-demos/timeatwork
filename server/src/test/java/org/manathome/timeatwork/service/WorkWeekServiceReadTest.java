package org.manathome.timeatwork.service;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDate;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.junit.jupiter.api.Test;

import org.manathome.timeatwork.db.repository.WorkDoneRepository;
import org.manathome.timeatwork.domain.Task;
import org.manathome.timeatwork.domain.WorkDone;
import org.manathome.timeatwork.domain.values.WorkHours;
import org.manathome.timeatwork.security.AppUserDetailsService;
import org.manathome.timeatwork.security.AppUserNames;
import org.manathome.timeatwork.security.UserRoleName;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;

@SpringBootTest
@WithMockUser(username = AppUserNames.user, roles = { UserRoleName.USER })
public class WorkWeekServiceReadTest {

  /** for user and current week, will change if liquibase test data is added.. */
  private final long testFixtureMinutesForUser = 900L;    

  @Autowired
  WorkWeekService workWeekService;

  @Autowired
  WorkDoneRepository workDoneRepository;
  
  @Autowired
  AppUserDetailsService userService;

  @Autowired
  EntityManager em;

  
  @Test
  public void testGetWorkWeek() {
    var startOfWeekDate = LocalDate.now().plusDays(-5); // calc todo start of week

    var workWeek = workWeekService.getWorkWeek(startOfWeekDate);
    assertThat(workWeek).isNotNull();
    assertThat(workWeek.getFrom()).as("from").isEqualTo(startOfWeekDate);
    assertThat(workWeek.getTaskWeeks().length).as("task count").isEqualTo(3);
    assertThat(workWeek.sumWorkHours().asMinutes())
          .as("sum minutes for " + AppUserNames.user + " in week " + startOfWeekDate)
          .isEqualTo(testFixtureMinutesForUser);
  }

  
  @Test
  public void testGetEmptyWorkWeek() {

    final int testFixtureAssignedTasks = 2;

    var startOfWeekDate = LocalDate.now().plusDays(20); // no work entry yet.

    var workWeek = workWeekService.getWorkWeek(startOfWeekDate);
    assertThat(workWeek).isNotNull();
    assertThat(workWeek.getFrom()).as("from").isEqualTo(startOfWeekDate);
    assertThat(workWeek.getTaskWeeks().length)
          .as("task count")
          .isEqualTo(testFixtureAssignedTasks);
    assertThat(workWeek.sumWorkHours().asMinutes()).as("sum minutes").isEqualTo(0L);
  }

  
  @Test
  @Transactional
  public void testGetWorkWeekWithAdditionalWork() {

    final Long additionalMinutes = 60L * 3;

    var startOfWeekDate = LocalDate.now().plusDays(-5); // calc todo start of week
    
    var teamMember = userService.getCurrentTeamMember();

    var newWork = new WorkDone(
        teamMember, 
        Task.buildForTest(-4, "x"), 
        startOfWeekDate);
    
    newWork.setWorkHours(WorkHours.ofHours(3));

    workDoneRepository.save(newWork);
    em.flush();

    var workWeek = workWeekService.getWorkWeek(startOfWeekDate);
    assertThat(workWeek).isNotNull();
    assertThat(workWeek.sumWorkHours().asMinutes()).as("sum minutes")
        .isEqualTo(testFixtureMinutesForUser + additionalMinutes.longValue());
  }

  
  @Test
  public void testGetOldWorkWeek() {
    var startOfWeekDate = LocalDate.now().plusDays(-11);

    var workWeek = workWeekService.getWorkWeek(startOfWeekDate);
    assertThat(workWeek).isNotNull();
    assertThat(workWeek.getFrom()).as("from").isEqualTo(startOfWeekDate);
    assertThat(workWeek.getTaskWeeks().length).as("task count").isEqualTo(3);
  }

}
