package org.manathome.timeatwork.service;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDate;

import javax.persistence.EntityManager;

import org.junit.jupiter.api.Test;
import org.manathome.timeatwork.db.repository.WorkDoneRepository;
import org.manathome.timeatwork.security.AppUserNames;
import org.manathome.timeatwork.security.UserRoleName;
import org.manathome.timeatwork.util.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;

@SpringBootTest
@WithMockUser(username = AppUserNames.user, roles = { UserRoleName.USER })
public class WorkWeekServiceUpdateTest {

  private static final Logger logger = Logger.getLogger(WorkWeekServiceUpdateTest.class);

  @Autowired
  WorkWeekService workWeekService;

  @Autowired
  WorkDoneRepository workDoneRepository;

  @Autowired
  EntityManager em;

  @Test
  public void testUpdateMoreHoursToWorkWeek() {

    var startOfWeekDate = LocalDate.now().plusDays(10);

    var workWeek = workWeekService.getWorkWeek(startOfWeekDate);
    assertThat(workWeek).isNotNull();

    final var oldSum = workWeek.sumWorkHours().asHours();

    var firstTask = workWeek.getTaskWeeks()[0];

    logger.debug("before: " + firstTask.toDump());

    firstTask.getWorkHours()[0] = firstTask.getWorkHours()[0].addTo(2);
    firstTask.getWorkHours()[3] = firstTask.getWorkHours()[3].addTo(1);
    firstTask.getWorkHours()[5] = firstTask.getWorkHours()[5].addTo(3);

    logger.debug("change via service to: " + firstTask.toDump());

    workWeekService.updateWorkWeek(workWeek);

    var changedWorkWeek = workWeekService.getWorkWeek(startOfWeekDate);
    assertThat(changedWorkWeek).isNotNull();

    logger.debug("change done by service: " + changedWorkWeek.getTaskWeeks()[0].toDump());

    assertThat(changedWorkWeek.sumWorkHours().asHours())
      .as("6 hours added workweek")
      .isEqualTo(oldSum + 2 + 1 + 3);
  }

}
