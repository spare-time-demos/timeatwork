package org.manathome.timeatwork.test.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import org.junit.jupiter.api.Tag;
import org.manathome.timeatwork.test.DocumentationConstants;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;

/** annotate tests that contribute to living documentation. */
@Target({ ElementType.TYPE, ElementType.METHOD })
@Retention(RetentionPolicy.RUNTIME)
@Tag("documentation")
@AutoConfigureRestDocs(uriHost = DocumentationConstants.SAMPLEHOST_TIMEATWORK)
@IntegrationTest
public @interface Documentation {
}