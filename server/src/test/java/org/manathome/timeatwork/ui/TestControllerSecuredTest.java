package org.manathome.timeatwork.ui;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.manathome.timeatwork.security.UserRoleName;
import org.manathome.timeatwork.test.annotation.IntegrationTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

/** test protected resources. 
 *  protected with spring boot security 
 *  
 *  @see org.manathome.timeatwork.security.SecurityConfig
 */
@ExtendWith(SpringExtension.class)
@IntegrationTest
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class TestControllerSecuredTest {

  @LocalServerPort
  private int port;

  @Autowired
  private WebApplicationContext context;

  private MockMvc mvc;

  /** enable spring security in this test. 
   * https://docs.spring.io/spring-security/site/docs/current/reference/html/test.html
   * */
  @BeforeEach
  public void setup() {
    mvc = MockMvcBuilders
        .webAppContextSetup(context)
        .apply(SecurityMockMvcConfigurers.springSecurity())
        .build();
  }

  @Test
  public void testHtmlUnprotectedTestPageWithoutUser() throws Exception {
    mvc.perform(get(TestController.URL_TEST))
       .andExpect(status().isOk());  
  }

  @Test
  public void testHtmlProtectedTestPageWithoutRequiredUser() throws Exception {
    mvc.perform(get(TestController.URL_TEST_PROTECTED))
       .andExpect(status().is4xxClientError());  
  }
  
  @Test
  @WithMockUser(roles = UserRoleName.USER)
  public void testHtmlProtectedTestPageWithAuthenticatedUser() throws Exception {
    mvc.perform(get(TestController.URL_TEST_PROTECTED))
       .andExpect(status().isOk());  
  }
  
}
