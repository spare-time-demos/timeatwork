package org.manathome.timeatwork.ui;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.manathome.timeatwork.test.annotation.IntegrationTest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.context.junit.jupiter.SpringExtension;

/** test server html response. */
@ExtendWith(SpringExtension.class)
@IntegrationTest
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class TestControllerTest {

  @LocalServerPort
  private int port;

  @Autowired
  private TestRestTemplate restTemplate;
  
  @Test
  public void testHtmlUnprotectedTestPage() {
    assertThat(
        this.restTemplate
        .getForObject("http://localhost:" + port + TestController.URL_TEST, String.class))
        .contains("hello, this is the time@work backend")
        .contains(TestController.URL_TEST);
  }

}
