package org.manathome.timeatwork.util;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDate;

import org.junit.jupiter.api.Test;

public class DateUtilTest {

  @Test
  public void testInstance() {
    assertThat(DateUtil.instance()).isNotNull();
  }

  @Test
  public void testToday() {
    assertThat(DateUtil.instance().today()).isNotNull();
    assertThat(DateUtil.instance().today()).isAfter(LocalDate.now().minusDays(1));
    assertThat(DateUtil.instance().today()).isBefore(LocalDate.now().plusDays(1));
  }

}
