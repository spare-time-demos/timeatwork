package org.manathome.timeatwork.util;

import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigInteger;
import java.time.LocalDate;

import org.junit.jupiter.api.Test;
import org.manathome.timeatwork.domain.Task;
import org.manathome.timeatwork.domain.TaskAssignment;

public class JsonHelperTest {

  private static final Logger logger = Logger.getLogger(JsonHelperTest.class);

  @Test
  public void testJsonSerializationOfAssignment() throws Exception {

    var dt = LocalDate.now().minusDays(1);
    var ta = TaskAssignment.buildForTest(-1, -1);
    ta.setAssignmentRange(dt, null);
    var json = JsonHelper.asJsonString(ta);

    logger.debug(json);
    assertThat(json).contains(dt.getYear() + "-");
  }

  @Test
  public void testJsonSerializationOfTask() throws Exception {

    var t = Task.buildForTest("dummy-test");
    var json = JsonHelper.asJsonString(t);

    logger.debug(json);
    assertThat(json).contains("\"dummy-test\"");
  }

  @Test
  public void testJsonSerializationOfLocalDate() throws Exception {

    var ld = LocalDate.of(2019, 11, 1);
    var json = JsonHelper.asJsonString(ld);
    assertThat(json).as(ld.toString()).contains("2019-11-01");

    ld = LocalDate.of(2021, 1, 1);
    json = JsonHelper.asJsonString(ld);
    assertThat(json).as(ld.toString()).contains("2021-01-01");

    ld = LocalDate.of(2021, 12, 31);
    json = JsonHelper.asJsonString(ld);
    assertThat(json).as(ld.toString()).contains("2021-12-31");

  }

  @Test
  public void testJsonSerializationNullToEmptyString() throws Exception {
    Object ld = null;
    String json = JsonHelper.asJsonString(ld);
    assertThat(json).as("nul to empty string").isEqualToIgnoringCase("");
  }
  
  @Test
  public void testJsonSerializationEmptyStringToEmptyString() throws Exception {
    String ld = "a-B-c";
    String json = JsonHelper.asJsonString(ld);
    assertThat(json).as("empty string").isEqualToIgnoringCase("\"a-B-c\"");
  }
  
  @Test
  public void testJsonSerializationOfNumberNumberToJsonValue() throws Exception {
    BigInteger bi = BigInteger.TWO;
    String json = JsonHelper.asJsonString(bi);
    assertThat(json).as("convert number to json").isEqualToIgnoringCase("2");
  }
  

}
