package org.manathome.timeatwork.util;

public final class JsonTestUtil {

  public static String asJsonString(final Object obj) {
    return JsonHelper.asJsonString(obj);
  }

}
