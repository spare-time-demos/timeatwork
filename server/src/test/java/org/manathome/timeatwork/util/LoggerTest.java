package org.manathome.timeatwork.util;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

public class LoggerTest {

  
  @Test
  public void testLoggerCreation() {
    
    final Logger logger = Logger.getLogger("test-logger");
    assertThat(logger).isNotNull();
  }

  @Test
  public void testLogWrite() {
    
    final Logger logger = Logger.getLogger("test-write-logger");
    assertThat(logger).isNotNull();
    logger.trace("test trace log write");
    logger.info("test info log write");
    logger.fatal("test fatal log write", new Exception("exc test"));
  }

  @Test
  public void testLogErrorWrite() {
    
    final Logger logger = Logger.getLogger("test-error-write-logger");
    assertThat(logger).isNotNull();
    logger.error("test error log write");
    logger.error("test error log write", 
        new TimeAtWorkInternalTestException());
    logger.error("test error log write nested", 
        new TimeAtWorkInternalTestException("msg", new Exception("dummy inner exc")));
  }


  @Test
  public void testLogDebugWrite() {
    
    final Logger logger = Logger.getLogger("test-write-logger");
    assertThat(logger).isNotNull();
    logger.debug("test debug log write");
    logger.debug("test debug log write param {} {} {}", 5, 4, 3);
  }

}
