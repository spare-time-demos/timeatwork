package org.manathome.timeatwork.util;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;

public class NotFoundExceptionTest {

  @Test
  public void testNotFoundExceptionCreation() {
    var ex = new NotFoundException("aba", "blublu");
    assertThat(ex.getMessage()).contains("aba");
    assertThat(ex.getMessage()).contains("blublu");    
    
    assertThat(ex.getStatus()).isEqualTo(HttpStatus.NOT_FOUND);
  }

}
