package org.manathome.timeatwork.util;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

import org.manathome.timeatwork.util.assertions.Require;
import org.manathome.timeatwork.util.assertions.RequirementNotMet;

class RequireTest {

  @Test
  void testNotNullFailed() {
    assertThrows(RequirementNotMet.class, () -> {
      Require.notNullOrZero(null, "should expected this text");
    });
  }

  @Test
  void testIsTrueFailed() {
    assertThrows(RequirementNotMet.class, () -> {
      Require.isTrue(false, "should expected this text (false)");
    });
  }

  @Test
  void testIsNotWithespace() {
    assertThrows(RequirementNotMet.class, () -> {
      Require.notNullOrEmptyWhitespace("      ", "should be thrown on whitespace.");
    });
  }

  @Test
  void testIsNotZeroWithZero() {
    assertThrows(RequirementNotMet.class, () -> {
      Require.notNullOrZero(Long.valueOf(0), "should be thrown on 0.");
    });
  }

  @Test
  void testIsSameId() {
    assertThrows(RequirementNotMet.class, () -> {
      Require.sameId(Long.valueOf(55), Long.valueOf(56), "dummy 55 vs 56");
    });
  }

  @Test
  void testIsNotIsNotSameId() {
    assertThrows(RequirementNotMet.class, () -> {
      Require.sameId(null, Long.valueOf(56), "null vs 56");
    });
  }

  @Test
  void testIsSameIdOk() {
    final Long r = Require.sameId(
        Long.valueOf(5555433), 
        Long.valueOf(5555433), 
        "expect large identical numbers should not fail.");
    
    assertThat(r.longValue()).isEqualTo(Long.valueOf(5555433));
  }

}
