package org.manathome.timeatwork.util;

/** internal testing error. */
public class TimeAtWorkInternalTestException extends RuntimeException {

  private static final long serialVersionUID = 1L;

  public TimeAtWorkInternalTestException() {
    super();
  }

  public TimeAtWorkInternalTestException(final String message) {
    super(message);
  }

  public TimeAtWorkInternalTestException(final String message, final Throwable cause) {
    super(message, cause);
  }

}
