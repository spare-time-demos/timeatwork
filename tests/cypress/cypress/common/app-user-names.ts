/** special user names used within tests etc. */
export class AppUserNames {
  
    static readonly admin: string = 'admin';
    static readonly tlead1: string = 'tlead1';
    static readonly user: string = 'user';

    static readonly testPassword: string = 'password';
} 
  