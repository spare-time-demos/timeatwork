
import { AppUserNames } from '../common/app-user-names';

describe('time@work web application access', () => {

  beforeEach(function() {
    
    cy.visit(
     Cypress.env('urlStartPage'),
      {
        onBeforeLoad: (win) => {
          // reset old login..
          win.sessionStorage.clear()
        }}
    )

    cy.get('.navbar-brand').contains('time@work').should('exist')
  })

  after(function() {
    cy.screenshot('user-ui/end.of.test', {'capture': 'fullPage'})
  })


  specify('login page opens on startup', () => {

    cy.get('h1').contains('Welcome to time@work').should('exist')
    cy.get('.btn').contains('login').should('exist')
    cy.get('input[name="userId"]').should('exist')
    cy.get('input[name="password"]').should('exist')

    cy.screenshot('user-ui/login', {'capture': 'fullPage'})
  })

  specify('login with wrong password displays error', () => {

    cy.get('.btn[type="submit"]').contains('login').should('exist')
    cy.get('input[name="userId"]').type('wrong-user')
    cy.get('input[name="password"]').type('wrong-password')
    cy.get('.btn[type="submit"]').click()
    
    cy.get('[data-test="errorMessage"]').contains('invalid user or password')
    cy.screenshot('user-ui/login-error', {'capture': 'fullPage'})
  })

  specify('login as user with correct password possible', () => {
    cy.login(AppUserNames.user, AppUserNames.testPassword)
  })
  
  specify('login as admin user is possible', () => {
    cy.login(AppUserNames.admin, AppUserNames.testPassword)
  })

});


