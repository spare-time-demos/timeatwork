import { AppUserNames } from '../common/app-user-names';

describe('admin-ui: manage projects', () => {

  beforeEach(function() {
    cy.visit(
      Cypress.env('urlStartPage'),
      {
        onBeforeLoad: (win) => {
          win.sessionStorage.clear()
        }}
    )
    cy.login(AppUserNames.admin, AppUserNames.testPassword)
  })
 
  it('project-view page opens', () => {
    
    cy.get('.badge').should('exist')
    cy.get('.nav-link').contains('project').should('exist')
    cy.get('.nav-link').contains('project').click()

    cy.get('.btn').contains('new project..').should('exist')
    cy.get('[data-test="project-link"]').should('exist')
    
    cy.screenshot('admin-ui/project-view')
  })

  it('project-view page navigates to project edit page', () => {

    cy.openMenu('project')

    cy.get('[data-test="project-link"]').should('exist')
    cy.get('a').contains('a first active project').click()

    cy.get('input[name="project-name"]').should('have.value', 'a first active project')
    cy.screenshot('admin-ui/project-edit', {'capture': 'fullPage', 'scale': true})
  })

  it('project-view page navigates to new project page', () => {

    cy.openMenu('project')

    cy.get('.btn')
      .contains('new project..')
      .click()

    cy.get('input[name="project-name"]').should('have.value', '')
    cy.get('.btn-primary').should('have.text', 'Save')

    cy.get('[data-cy=project-id]').should(($badge) => {
      expect($badge.get(0).innerText).be.equal('0')
    })
    cy.screenshot('admin-ui/project-new')

  })

});


