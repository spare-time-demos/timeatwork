import { AppUserNames } from '../common/app-user-names';

describe('admin-ui: manage tasks in project', () => {

  beforeEach(function() {
    cy.visit(
      Cypress.env('urlStartPage'),
      {
        onBeforeLoad: (win) => {
          // reset old login..
          win.sessionStorage.clear()
        }}
    )
    cy.login(AppUserNames.admin, AppUserNames.testPassword)
  })

  after(function() {
    cy.screenshot('admin-ui/end.of.test', {'capture': 'runner'})
  })

  it('navigate to new task page', () => {

    cy.openMenu('project')

    cy.get('.list-group > :nth-child(1) > a').click()
    cy.get('[name="project-name"]').should('exist')  

    cy.get('.btn').contains('new task..').click()

    cy.get('#task-name').should('exist')
    cy.get('h3').contains('task:').should('exist')
    cy.get('h3').contains('task assignments').should('exist')
    cy.get('.badge').contains('0').should('exist')
    cy.screenshot('admin-ui/task-new', {'capture': 'fullPage', 'scale': true})
  })


  it('project: create a new task', () => {

    const newTaskName = 'cy-test-' + (new Date()).toISOString();
    
    cy.openMenu('project')

    cy.get('.list-group > :nth-child(1) > a').click()

    cy.get('[name="project-name"]').should('exist')  
    cy.get('h3').contains('project tasks:').should('exist')

    cy.get('.btn').contains('new task..').click()

    cy.log('task name entry')
    cy.get('#task-name').should('exist')
    cy.log('fill in task data..')
    cy.get('#task-name').type(newTaskName)

    cy.log('save new task')
    cy.server();
    cy.route("POST", "rest/**/task").as("postTask");
    cy.get('.btn-primary').contains('Save').click()
    cy.wait('@postTask')

    cy.log('check successful creation of ' + newTaskName)
    cy.get('.badge').should(($badge) => {
      let newTaskId = Number($badge.get(0).innerText)
      return expect(newTaskId).be.at.least(1)
    })

    cy.log('navigate back to project page with new task ' + newTaskName)
    cy.get('.btn-secondary').contains('back..').click()

    cy.get('[name="project-name"]').should('exist')  
    cy.get('[data-test="task-link"]').should('exist')
    cy.log('check new task in project')
    cy.get('.btn-outline-danger').contains('delete').should('be.enabled')
    cy.get('a').contains(newTaskName).should('exist')    
  })

  it('project: navigate to existing task page with assignments', () => {

    cy.openMenu('project')
    
    cy.get('[data-test="project-link"]').should('exist')
    cy.get('a').contains('a first active project').click()

    cy.log('check project page')
    cy.get('[name="project-name"]').should('have.value', 'a first active project')  
    cy.get('.btn-outline-danger').contains('delete').should('exist')

    cy.log('navigate to task')
    cy.get('[data-test="task-link"]').should('exist')

    cy.get('a').contains('a third task').scrollIntoView()
    cy.get('a').contains('a third task').click()

    cy.log('check shown task')
    cy.get('#task-name').should('have.value', 'a third task')  

    cy.log('check task assignments')
    cy.get('h3').contains('task assignments:').should('exist')
    cy.get('.btn-outline-danger').contains('delete').should('exist')
    cy.get('.badge').contains('tlead').should('exist')

    cy.screenshot('admin-ui/task-edit', {'capture': 'fullPage', 'scale': true})
  })

  it('project: task page, add new assignment row', () => {

    cy.openMenu('project')
    cy.log('open project, then task page')
    
    cy.get('[data-test="project-link"]').should('exist')
    cy.get('a').contains('a first active project').click()

    cy.log('navigate to task')
    cy.get('[data-test="task-link"]').should('exist')

    cy.get('a').contains('a third task')
               .scrollIntoView()
               .click() 

    cy.get('input[name="name"').should('exist')
    cy.get('h3').contains('task assignments:').should('exist')

    cy.log('add task assignment member') 

    cy.get('button').contains('add')
                    .should('exist')
                    .should('be.disabled')

    cy.get('select[name="new-assignment-member"]')
      .should('exist')
      .select('1: Object', {force: true}) 
    
    cy.get('div[data-test="assignment"]').its('length').then((assignmentRowCount) => {

      cy.log('add another assignment to current ' + assignmentRowCount + " rows.")

      cy.get('button').contains('add')
        .should('exist')
        .should('be.enabled')
        .click()

      cy.get('div[data-test="infoMessage"]').contains('created new assignment')

      cy.get('button').contains('add')
      .should('exist')
      .should('be.disabled')

      cy.get('div[data-test="assignment"]').its('length').should('eq', assignmentRowCount +1)
    });

    cy.screenshot('admin-ui/task-edit-new-assignment', {'capture': 'fullPage', 'scale': true})
  })

});


