import { AppUserNames } from '../common/app-user-names';

describe('admin-ui: manage user', () => {

  beforeEach(function() {
    cy.visit(
      Cypress.env('urlStartPage'),
      {
        onBeforeLoad: (win) => {
          win.sessionStorage.clear()
        }}
    )
    cy.login(AppUserNames.admin, AppUserNames.testPassword)
  })

  it('user-view page opens with list of user shown', () => {
    
    cy.get('.badge').should('exist')
    cy.get('.nav-link').contains('users').should('exist')
    cy.get('.nav-link').contains('users').click()

    cy.get('.btn').contains('new user..').should('exist')

    cy.log('data (user) is loaded')
    cy.get('span[data-test="user-id-badge"]').should('exist')
    
    cy.screenshot('admin-ui/user-view')
  })

  it('user-view opens edit fields for selected user', () => {

    cy.openMenu('users')

    cy.get('[data-test="user-id-badge"]').should('exist')
    cy.get('strong').contains(AppUserNames.tlead1).click()

    cy.get('input[name="user-name"]').should('exist')
    cy.get('input[name="user-name"]').should('have.value', 'a first test lead and user')
    cy.get('button').contains('Save').scrollIntoView()

    cy.screenshot('admin-ui/user-edit', {'capture': 'fullPage', 'scale': true})
  })


  it('user-view opens edit fields for new user', () => {

    cy.openMenu('users')

    cy.get('[data-test="user-id-badge"]').should('exist')

    cy.get('.btn').contains('new user..').should('exist')
    cy.get('.btn').contains('new user..').click()

    cy.get('input[name="user-name"]').should('exist')
    cy.get('input[name="user-name"]').should('have.value', '')
    cy.get('input[name="user-id"]').should('have.value', '')

    cy.screenshot('admin-ui/user-new', {'capture': 'fullPage', 'scale': true})
  })

});


