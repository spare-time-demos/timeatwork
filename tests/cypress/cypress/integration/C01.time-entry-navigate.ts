import { AppUserNames } from '../common/app-user-names';

describe('user-ui: time entry navigate and view', () => {

  beforeEach(function() {
    cy.viewport(1000, 1024)

    cy.visit(
      Cypress.env('urlStartPage'),
      {
        onBeforeLoad: (win) => {
          // reset old login..
          win.sessionStorage.clear()
        }}
    )
    cy.login(AppUserNames.user, AppUserNames.testPassword)
  })

  after(function() {
    cy.screenshot('user-ui/time-entry.after.test', {'capture': 'runner', 'scale': true})
  })

  it('time entry view opens', () => {
 
    cy.log('load week')

    cy.log('check work week page')
    cy.get('h3').contains('work:').should('exist')
    cy.get('[type=submit]').contains('save').should('exist').should('be.visible')
    cy.get('.badge').should('exist')
    cy.get('button').contains('save').should('exist').should('be.visible')

    cy.screenshot('user-ui/time-entry', {'capture': 'fullPage', 'scale': true})

    cy.get('button').contains('save').scrollIntoView()
    cy.screenshot('user-ui/time-entry.large', {'capture': 'fullPage', 'scale': false})
  })

  
  it('time entry change week navigation', () => {
 
    cy.log('load week')

    cy.get('span[data-test="inWeek"]').then( (span: any) => {
      
      let currentWeek = span.text()
      cy.log('change next week and back from ' + currentWeek)

      cy.get('button[data-test="next7"').click()

      cy.get('span[data-test="inWeek"]').should('not.contain.text', currentWeek)
      cy.get('button[data-test="prev7"').click()
      cy.get('span[data-test="inWeek"]').should('contain.text', currentWeek)
    })

  })

});


