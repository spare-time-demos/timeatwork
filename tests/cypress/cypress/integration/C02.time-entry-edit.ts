import { AppUserNames } from '../common/app-user-names';

describe('user-ui: time entry edit', () => {

  beforeEach(function() {
    cy.visit(
      Cypress.env('urlStartPage'),
      {
        onBeforeLoad: (win) => {
          // reset old login..
          win.sessionStorage.clear()
        }}
    )
    cy.login(AppUserNames.user, AppUserNames.testPassword)
  })


  it('time entry view opens', () => {
 
    cy.log('check work week page')
    cy.get('h3').contains('work:').should('exist')
    cy.get('button').contains('save').should('exist').should('be.visible')

    cy.log('input data rows there (input fields)')
    cy.get('input[type="number"]').should('exist')
    cy.get('input[type="number"]')
      .eq(0)
      .focus()
      .type("{selectall}")
      .type("1")

    cy.get('input[type="number"]')
      .eq(1)
      .type("{selectall}")
      .type("2")

    cy.get('input[type="number"]')
      .eq(2)
      .type("{selectall}")
      .type('3')

    cy.log('save week')
    cy.get('button').contains('save').click()

    cy.get('div[data-test="infoMessage"]').should('exist').contains('saved week')
  })
  
});


