import { AppUserNames } from '../common/app-user-names';

describe('user-ui: password change', () => {

  beforeEach(function() {
    cy.visit(
      Cypress.env('urlStartPage'),
      {
        onBeforeLoad: (win) => {
          win.sessionStorage.clear()
        }}
    )
    cy.login(AppUserNames.user, AppUserNames.testPassword)
  })

  it('show change password ui', () => {
 
    cy.log('load password page')
    cy.openMenu('password')

    cy.log('check fields')
    cy.get('h3')
      .contains('change password:')
      .should('exist') // correct header/page.

    cy.get('input[name="password-old"')
      .should('exist')

    cy.get('input[name="password-old"')
      .should('have.value', '')

    cy.get('button')
      .contains('Change Password')
      .should('exist')

    cy.screenshot('user-ui/password')
  })

});


