import { AppUserNames } from '../common/app-user-names';

describe('report-ui: monthly report by project', () => {

  beforeEach(function() {
    cy.visit(
      Cypress.env('urlStartPage'),
      {
        onBeforeLoad: (win) => {
          // reset old login..
          win.sessionStorage.clear()
        }}
    )
    cy.login(AppUserNames.user, AppUserNames.testPassword)
  })

  it('show report', () => {
 
    cy.openReportSubMenu('report by project')

    cy.log('check report display')
    cy.get('h3').contains(' monthly report grouped by project').should('exist') // correct header/page.
    
    cy.get('div .row-day').should('exist')  // data found, rows displayed
    cy.screenshot('report-ui/report-monthly-byproject')
  })

});


