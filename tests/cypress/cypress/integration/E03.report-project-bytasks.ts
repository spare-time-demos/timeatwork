import { AppUserNames } from '../common/app-user-names';

describe('report-ui: monthly report project by task', () => {

  beforeEach(function() {
    cy.visit(
      Cypress.env('urlStartPage'),
      {
        onBeforeLoad: (win) => {
          // reset old login..
          win.sessionStorage.clear()
        }}
    )
    cy.login(AppUserNames.admin, AppUserNames.testPassword)
  })

  it('show project report by task', () => {
 
    cy.openReportSubMenu('project report')

    cy.log('check report display')
    cy.get('h3').contains('project report:').should('exist') // correct header/page.
    
    cy.get('em').contains('choose a project to report').should('exist')  // init state of page.
    cy.screenshot('report-ui/report-project-bytask.init')

    cy.log('choose project first')
    cy.get('button[data-toggle="dropdown"]').click()
    cy.get('.dropdown-menu').contains('a first active project').click()
    cy.screenshot('report-ui/report-project-bytask.choose-project')

    cy.log('wait for data to be display for first project')
    cy.get('div[data-test="report-data-row"]').should('exist')

    cy.log('excel download button')
    cy.get('button[data-test="button-excel-download"]').should('exist')
    cy.get('button[data-test="button-excel-download"]').click()
    cy.screenshot('report-ui/report-project-bytask')

  })

});


