import { AppUserNames } from '../common/app-user-names';

describe('report-ui: user participation', () => {

  beforeEach(function() {
    cy.visit(
      Cypress.env('urlStartPage'),
      {
        onBeforeLoad: (win) => {
          win.sessionStorage.clear()
        }}
    )
    cy.login(AppUserNames.admin, AppUserNames.testPassword)
  })

  it('show user report by participation', () => {
 
    cy.log('load report page')
    cy.openReportSubMenu('report user participation')

    cy.log('check report display')
    cy.get('h3').contains('monthly user work').should('exist') // correct header/page.
    
    cy.get('div[data-test="report-data-row"]').should('exist')
    cy.screenshot('report-ui/report-user-participation')
  })

});


