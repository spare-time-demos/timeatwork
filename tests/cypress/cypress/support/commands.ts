/// <reference types="cypress" />

declare global {
  namespace Cypress {
  
  interface Chainable<Subject> {
    login: typeof login;
    openMenu: typeof openMenu;
    openReportSubMenu: typeof openReportSubMenu;
  }
  
}}

/** login int application with given user/password. check if menu adapts to logged in state. */
export function login(userId: string, password: string): void {
  cy.log('login with user "' + userId + '"..')

  cy.get('button[type="submit"]').contains('login').should('exist')
  cy.get('input[name="userId"]').type(userId)
  cy.get('input[name="password"]').type(password)
  cy.get('button[type="submit"]').contains('login').click()  

  cy.log('login checking success..' + userId + '"')

  cy.get('.badge').should('exist')  
  cy.get('.navbar-text').contains(userId).should('exist')
  cy.get('.nav-link[data-test="logout"]').should('exist')

  cy.log('login done as ' + userId + '"')
}

/** change active menu (navigate top level menu). */
export function openMenu(menu: string): void {
  cy.log('changing to menu: "' + menu + '"..')

  cy.get('.badge').should('exist')
  cy.get('.nav-link').contains(menu).should('exist')
  cy.get('.nav-link').contains(menu).click()
}

/** change active menu (report dropdown). */
export function openReportSubMenu(menu: string): void {
  cy.log('changing to report sub menu: "' + menu + '"..')

  cy.get('#navbarReportDropdown')
    .contains('reporting')
    .click()

  cy.get('.dropdown-item')
    .contains(menu)
    .click()
}

Cypress.Commands.add('login', login)
Cypress.Commands.add('openMenu', openMenu)
Cypress.Commands.add('openReportSubMenu', openReportSubMenu)
