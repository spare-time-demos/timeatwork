package org.manathome.timeatwork.loadtests

import io.gatling.core.Predef._ 
import io.gatling.http.Predef._
import scala.concurrent.duration._

class LoginCallSimulation extends Simulation { 

  val httpProtocol = http 
    .baseUrl("https://time-at-work.herokuapp.com/authentication") 
    .acceptHeader("application/json, text/plain, */*") 
    .acceptEncodingHeader("gzip, deflate")
    .userAgentHeader("gatling-tests")

  val scn = scenario("LoginSimulation") 
    .exec(http("authentication") 
      .post("/")
      .body(StringBody("""{"userId":"user","password":"password"}""")).asJson
      ) 
    .pause(5) 

  setUp( 
    scn.inject(atOnceUsers(5)) 
  ).protocols(httpProtocol) 
}