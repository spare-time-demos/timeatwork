package org.manathome.timeatwork.loadtests

import io.gatling.core.Predef._ 
import io.gatling.http.Predef._
import scala.concurrent.duration._

class LoginFailedCallSimulation extends Simulation { 

  val httpProtocol = http 
    .baseUrl("https://time-at-work.herokuapp.com/authentication") 
    .acceptHeader("application/json, text/plain, */*") 
    .acceptEncodingHeader("gzip, deflate")
    .userAgentHeader("gatling-tests")

  val scn = scenario("LoginFailedSimulation") 
    .exec(http("authentication") 
      .post("/")
      .body(StringBody("""{"userId":"unknownUserId","password":"irrelevantPassword"}""")).asJson
      ) 
    .pause(1) 

  setUp( 
    scn.inject(atOnceUsers(3)) 
  ).protocols(httpProtocol) 
}