package org.manathome.timeatwork.loadtests

import io.gatling.core.Predef._ 
import io.gatling.http.Predef._
import scala.concurrent.duration._

class WorkWeekGetCallSimulation extends Simulation { 


  val sessionHeaders = Map("Authorization" -> "Bearer ${authToken}",
                           "Content-Type" -> "application/json")

  val httpProtocol = http 
    .baseUrl("https://time-at-work.herokuapp.com") 
    .acceptHeader("application/json, text/plain, */*") 
    .acceptEncodingHeader("gzip, deflate")
    .userAgentHeader("gatling-tests")

  val scn = scenario("WorkWeekSimulation") 
    .exec(http("authentication") 
      .post("/authentication")
      .body(StringBody("""{"userId":"user","password":"password"}""")).asJson
      .check(jsonPath("$.token").exists.saveAs("authToken"))      
      ) 
    .exec(http("rest_workweek")
      .get("/rest/workweek/2020-02-03")
      .headers(sessionHeaders)
      )

  setUp( 
    scn.inject(atOnceUsers(15)) 
  ).protocols(httpProtocol) 
}


